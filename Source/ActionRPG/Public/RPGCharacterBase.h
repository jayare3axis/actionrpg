// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "RPGTypes.h"
#include "GameFramework/Character.h"
#include "RPGCombatInterface.h"
#include "RPGPlayerAppearanceInterface.h"
#include "RPGCharacterEquipmentInterface.h"
#include "AbilitySystemInterface.h"
#include "Abilities/RPGAbilitySystemComponent.h"
#include "Abilities/RPGAttributeSet.h"
#include "Items/RPGActiveEffectAtSlot.h"
#include "RPGInteractableInterface.h"
#include "RPGHighlightableInterface.h"
#include "RPGTopDownCameraActorInterface.h"
#include "RPGSlottedAbilitiesInterface.h"
#include "RPGGameplayEffect.h"
#include "RPGCharacterBase.generated.h"

// Forward
class ARPGPlayerControllerBase;
class URPGGameplayAbility;
class UGameplayEffect;
class URPGTopDownCameraComponent;
class URPGChasingManagerComponent;
class URPGSaveGame;

DECLARE_MULTICAST_DELEGATE(FTurnCompletedDelegate);

/**
* Base class for Character, should to be blueprinted
*/
UCLASS()
class ACTIONRPG_API ARPGCharacterBase : public ACharacter, public IRPGCombatInterface, public IRPGPlayerAppearanceInterface, public IRPGCharacterEquipmentInterface, public IAbilitySystemInterface, public IRPGInteractableInterface, public IRPGHighlightableInterface, public IRPGTopDownCameraActorInterface
{
	GENERATED_BODY()

public:
	// Constructor and overrides
	ARPGCharacterBase();
	virtual void BeginPlay() override;
	virtual void Destroyed() override;
	virtual void Tick(float DeltaTime) override;
	virtual void PossessedBy(AController* NewController) override;
	virtual void UnPossessed() override;
	virtual void OnRep_Controller() override;
	virtual URPGTopDownCameraComponent* GetTopDownCamera() override;
	virtual URPGCombatComponent* GetCombatComponent() override;
	virtual URPGPlayerAppearanceComponent* GetPlayerAppearanceComponent() override;
	virtual URPGCharacterEquipmentComponent* GetCharacterEquipmentComponent() override;
	
	// IInteractable
	virtual void Interact(ARPGPlayerControllerBase* PC) override;
	virtual void InteractClient(ARPGPlayerControllerBase* PC) override;
	virtual void StopInteraction(ARPGPlayerControllerBase* PC) override;
	virtual void StopInteractionClient(ARPGPlayerControllerBase* PC) override;
	// IHighlightable
	virtual void Highlighted(bool Value) override;

#pragma region Sockets
	/** Creates actor and ataches to a socket, if class is empty destroys actor attached to target socket. */
	UFUNCTION(BlueprintCallable, Category = "Items")
	void SetSocketedActor(TSubclassOf<AActor> ActorClass, FRotator Rotation, FName SocketName);

	/** Returns actor attached to socket, if there is any. */
	UFUNCTION(BlueprintCallable, Category = "Items")
	AActor* GetActorAtSocket(FName SocketName);
#pragma endregion
	
#pragma region Turning
	/** Starts turning animation so the character faces target actor */
	UFUNCTION(BLueprintCallable, Category = "Animation", meta = (DisplayName = "TurnToActor"))
	void BlueprintTurnToActor(AActor* TargetActor);

	/** Starts the turning animation and notifies the clients */
	UFUNCTION()
	void StartTurningAnimation();

	UFUNCTION(NetMulticast, Reliable)
	void MulticastAnimateTurnToYaw(float Yaw, bool bIsTurning);

	UFUNCTION(BlueprintImplementableEvent, Category = "Animation", meta = (DisplayName = "AnimateTurnToYaw"))
	void BlueprintAnimateTurnToYaw(float Yaw, bool bIsTurning);

	UFUNCTION(BlueprintImplementableEvent, Category = "Camera")
	URPGTopDownCameraComponent* BlueprintGetTopDownCamera();

	UFUNCTION(BlueprintImplementableEvent, Category = "Abilities")
	URPGCombatComponent* BlueprintGetCombatComponent();

	UFUNCTION(BlueprintImplementableEvent, Category = "Player Appearance")
	URPGPlayerAppearanceComponent* BlueprintGetPlayerAppearanceComponent();

	UFUNCTION(BlueprintImplementableEvent, Category = "Equipment")
	URPGCharacterEquipmentComponent* BlueprintGetCharacterEquipmentComponent();

	/** Character is currently turning */
	bool IsTurning;
#pragma endregion

#pragma region Chasing
	/** Component used to choose chasing target sub-actor for aggros */
	UPROPERTY(BlueprintReadonly, Category = "Abilities")
	URPGChasingManagerComponent* ChasingManagerComponent;

#pragma endregion

#pragma region Ability System
	// IAbilitySystemInterface
	UAbilitySystemComponent* GetAbilitySystemComponent() const override;
#pragma endregion

#pragma region Interaction
	UFUNCTION(BlueprintImplementableEvent, Category = "Interactable", meta = (DisplayName = "Interact"))
	void BlueprintInteract(ARPGPlayerControllerBase* PC);

	UFUNCTION(BlueprintImplementableEvent, Category = "Interactable", meta = (DisplayName = "Interact Client"))
	void BlueprintInteractClient(ARPGPlayerControllerBase* PC);

	UFUNCTION(BlueprintImplementableEvent, Category = "Interactable", meta = (DisplayName = "Stop Interaction"))
	void BlueprintStopInteraction(ARPGPlayerControllerBase* PC);

	UFUNCTION(BlueprintImplementableEvent, Category = "Interactable", meta = (DisplayName = "Stop Interaction Client"))
	void BlueprintStopInteractionClient(ARPGPlayerControllerBase* PC);
	
	UFUNCTION(BlueprintImplementableEvent, Category = "Interactable", meta = (DisplayName = "Highlighted"))
	void BlueprintHighlighted(bool Value);
#pragma endregion

#pragma region Move To Target
	/** Used by AI to set movement destination so the move ability knows where to go */
	UFUNCTION(BlueprintCallable, Category = "AI")
	void SetMoveTargetByLocation(FVector Target, float AcceptanceRadius);

	/** Used by AI to set movement destination so the move ability knows where to go */
	UFUNCTION(BlueprintCallable, Category = "AI")
	void SetMoveTargetByActor(AActor* Target, float AcceptanceRadius);

	/** Used by AI to set action destination, for example for turn ability */
	UFUNCTION(BlueprintCallable, Category = "AI")
	void SetActionTargetByLocation(FVector Target);

	/** Used by AI to set action destination for example for turn ability */
	UFUNCTION(BlueprintCallable, Category = "AI")
	void SetActionTargetByActor(AActor* Target);

	UFUNCTION()
	void SetMoveTarget(FMoveTarget Value);

	FORCEINLINE
	FMoveTarget GetMoveTarget() const { return MoveTarget; }

	FORCEINLINE
	bool IsHostile() const { return isHostile; }
#pragma endregion

#pragma region Turn To Target
	UFUNCTION()
	void TurnToTarget();

	FTurnCompletedDelegate OnTurnEnd;
#pragma endregion

#pragma region Action Target
	UFUNCTION()
	void SetActionTarget(FActionTarget Value);

	FORCEINLINE
	FActionTarget GetActionTarget() const { return ActionTarget; }
#pragma endregion
	
	/** Blueprint implements this to handle appearance option (unfortunatelly I'm not able to move this to component, since event broadcast here runs on server even that it's invoked on client) */
	UFUNCTION(BlueprintImplementableEvent, Category = "Combat")
	void HandleAppearanceOption(FName Key, int32 Value);
	
	URPGSaveGame* CurrentSaveGame;

protected:
#pragma region Abilties and Attributes
	/** The component used to handle ability system interactions */
	UPROPERTY(BlueprintReadonly, Category = "Abilities")
	URPGAbilitySystemComponent* AbilitySystemComponent;

	/** List of attributes modified by the ability system */
	UPROPERTY()
	URPGAttributeSet* AttributeSet;

	// Friended
	friend ARPGPlayerControllerBase;
	friend URPGCombatComponent;
#pragma endregion

	/** Destination for move ability */
	UPROPERTY(BlueprintReadWrite, Category = "Movement")
	FMoveTarget MoveTarget;

	/** Destination for turn ability */
	UPROPERTY(BlueprintReadWrite, Category = "Abilities")
	FActionTarget ActionTarget;

	/** Tells if character is hostile towars player */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HostilityRanking")
	bool isHostile;

private:
	/** Stores final rotation for turning interpolation */
	UPROPERTY()
	FRotator FinalRotation;

	/** Holds generated actors attached to a socket */
	UPROPERTY()
	TMap<FName, AActor*> SocketedActors;
};
