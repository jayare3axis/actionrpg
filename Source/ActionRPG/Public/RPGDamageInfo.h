// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "RPGDamageInfo.generated.h"

// Forward
class ARPGCharacterBase;

/** Wraps information about caused damage */
USTRUCT(BlueprintType)
struct ACTIONRPG_API FRPGDamageInfo
{
	GENERATED_BODY()

	/** Constructor */
	FRPGDamageInfo() : DamageAmount(0.f), HitInfo(FHitResult()), DamageTags(FGameplayTagContainer())
	{}

	FRPGDamageInfo(float _DamageAmount, const FHitResult& _HitInfo, const struct FGameplayTagContainer& _DamageTags, ARPGCharacterBase* _InstigatorCharacter, AActor* _DamageCauser)
		: DamageAmount(_DamageAmount), HitInfo(_HitInfo), DamageTags(_DamageTags), InstigatorCharacter(_InstigatorCharacter), DamageCauser(_DamageCauser)
	{}

	/** Amount of damage done */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Data)
	float DamageAmount;

	/** Holds information about the collision */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Data)
	FHitResult HitInfo;

	/** Holds damage tags */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Data)
	struct FGameplayTagContainer DamageTags;

	/** Character that caused the damage */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Data)
	ARPGCharacterBase* InstigatorCharacter;

	/** Actual actor that caused the damage (for example projectile) */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Data)
	AActor* DamageCauser;

};
