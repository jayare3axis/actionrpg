// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "RPGInitializationInterface.h"
#include "RPGInitManagerSubsystem.generated.h"

/**
 * Helps with synchronization of initialization processes.
 */
UCLASS()
class ACTIONRPG_API URPGInitManagerSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()

public:
	// Overrides
	virtual void Initialize(FSubsystemCollectionBase& Collection) override;

	/**
	* Binds target to listen to initialization events, callbacks allways run, even if registered late.
	*/
	UFUNCTION()
	void Bind(TScriptInterface<IRPGInitializationInterface> Target);

	/**
	* Unbinds target to listen to initialization events.
	*/
	UFUNCTION()
	void Unbind(TScriptInterface<IRPGInitializationInterface> Target);

	/**
	* Triggers listeners and sets next phase.
	*/
	UFUNCTION()
	void NextStep();

private:
	UPROPERTY()
	TEnumAsByte<EInitializationPhase> InitializationPhaze;

	UPROPERTY()
	TArray<TScriptInterface<IRPGInitializationInterface>> Listeners;
	
};
