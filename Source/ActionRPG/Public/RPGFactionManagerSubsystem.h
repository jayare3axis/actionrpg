// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "Subsystems/LocalPlayerSubsystem.h"
#include "RPGFactionManagerSubsystem.generated.h"

/**
 * Determines relations between actors.
 */
UCLASS()
class ACTIONRPG_API URPGFactionManagerSubsystem : public ULocalPlayerSubsystem
{
	GENERATED_BODY()
	
public:
	/** Returns hostility ranking of target actor towards the player */
	UFUNCTION(BlueprintCallable, Category = "HostilityRanking")
	EHostilityRanking GetHostilityRankingForLocalPlayer(AActor* Actor);

	/** Returns hostility ranking of target actor towards another */
	UFUNCTION(BlueprintCallable, Category = "HostilityRanking")
	EHostilityRanking GetHostilityRankingForActor(AActor* TargetActor, AActor* Actor);
};
