// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "Camera/PlayerCameraManager.h"
#include "RPGPlayerCameraManagerBase.generated.h"

/**
 * Base class for Player Camera Manager, should be blueprinted
 */
UCLASS()
class ACTIONRPG_API ARPGPlayerCameraManagerBase : public APlayerCameraManager
{
	GENERATED_BODY()

public:
	// Constructor
	ARPGPlayerCameraManagerBase();
	
};
