// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "Subsystems/LocalPlayerSubsystem.h"
#include "GameFramework/Actor.h"
#include "Templates/Tuple.h"
#include "RPGLootTileManagerSubsystem.generated.h"

/**
 * Handles the manage of which world position can spawn loot and which not, mean to be blueprinted
 */
UCLASS()
class ACTIONRPG_API URPGLootTileManagerSubsystem : public ULocalPlayerSubsystem
{
	GENERATED_BODY()

public:
	// Constructor
	URPGLootTileManagerSubsystem();

	/** Tries to get position for loot to spawn based on target position in a world */
	UFUNCTION(BlueprintCallable, Category = "Loot")
	bool TryGetLootPosition(FVector BasePosition, int32 MaxDistance, FVector& Result);

private:
	/** Converts world possition to tile */
	void WorldPositionToTile(FVector WorldPosition, TTuple<int32, int32>& Result);

	/** Converts tile to world possition */
	void TileToWorldPosition(const TTuple<int32, int32>& Tile, FVector& Result);

	UFUNCTION()
	bool CanWorldPositionSpawnLoot(const FVector& WorldPossition, float WorldZ, float& SpawnZ);

	/** Determines tile size */
	UPROPERTY(EditDefaultsOnly, Category = "Loot")
	int32 TileSize;

	/** Determines maximum elevation at which we can span the loot */
	UPROPERTY(EditDefaultsOnly, Category = "Loot")
	int32 MaxElevation;

	/** Holds currently occupied tiles */
	TMap<TTuple<int32, int32>, bool> OccupiedTiles;
	
};
