// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "RPGPlayerAppearanceComponent.h"
#include "RPGPlayerAppearanceInterface.generated.h"

/*
* Interface used to access Player Appearance Component.
*/
UINTERFACE(Blueprintable)
class URPGPlayerAppearanceInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class ACTIONRPG_API IRPGPlayerAppearanceInterface
{
	GENERATED_BODY()

public:
	/** Returns the player appearance component to use for this actor. */
	virtual URPGPlayerAppearanceComponent* GetPlayerAppearanceComponent() = 0;
};
