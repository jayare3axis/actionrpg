// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "GameFramework/SpringArmComponent.h"
#include "RPGTopDownCameraComponent.generated.h"

/**
 * Spring arm with attached camera component meant to be attached to player character to enable top down view.
 */
UCLASS(Blueprintable, ClassGroup = Camera, meta = (BlueprintSpawnableComponent))
class ACTIONRPG_API URPGTopDownCameraComponent : public USpringArmComponent
{
	GENERATED_BODY()

public:
	// Constructor
	URPGTopDownCameraComponent();

	/** Zooms or un-zooms camera by given amount while watching the boundary values */
	UFUNCTION()
	void SetCameraZoomAmount(float Amount);

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;
	
};
