// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "Engine/AssetManager.h"
#include "RPGAssetManager.generated.h"

// Forward
class URPGItem;
class URPGRaceDataAsset;
class URPGClassDataAsset;
class URPGSkillSetDataAsset;
class URPGSkillTierDataAsset;

/**
* Game implementation of asset manager, overrides functionality and stores game-specific types
* This is used by setting AssetManagerClassName in DefaultEngine.ini
*/
UCLASS()
class ACTIONRPG_API URPGAssetManager : public UAssetManager
{
	GENERATED_BODY()

public:
	// Constructor and overrides
	URPGAssetManager() {}

	/** Static types for items */
	static const FPrimaryAssetType ConsumableItemType;
	static const FPrimaryAssetType SkillItemType;
	static const FPrimaryAssetType EquipmentItemType;
	static const FPrimaryAssetType TokenItemType;
	static const FPrimaryAssetType WeaponItemType;
	static const FPrimaryAssetType ArmorItemType;
	static const FPrimaryAssetType AccessoryItemType;

	/** Returns the current AssetManager object */
	static URPGAssetManager& Get();

	/**
	* Synchronously loads an RPGItem subclass, this can hitch but is useful when you cannot wait for an async load
	* This does not maintain a reference to the item so it will garbage collect if not loaded some other way
	*
	* @param PrimaryAssetId The asset identifier to load
	* @param bDisplayWarning If true, this will log a warning if the item failed to load
	*/
	URPGItem* ForceLoadItem(const FPrimaryAssetId& PrimaryAssetId, bool bLogWarning = true);

	/**
	* Synchronously loads an URPGRaceDataAsset subclass, this can hitch but is useful when you cannot wait for an async load
	* This does not maintain a reference to the item so it will garbage collect if not loaded some other way
	*
	* @param PrimaryAssetId The asset identifier to load
	* @param bDisplayWarning If true, this will log a warning if the item failed to load
	*/
	URPGRaceDataAsset* ForceLoadRace(const FPrimaryAssetId& PrimaryAssetId, bool bLogWarning = true);

	/**
	* Synchronously loads an URPGClassDataAsset subclass, this can hitch but is useful when you cannot wait for an async load
	* This does not maintain a reference to the item so it will garbage collect if not loaded some other way
	*
	* @param PrimaryAssetId The asset identifier to load
	* @param bDisplayWarning If true, this will log a warning if the item failed to load
	*/
	URPGClassDataAsset* ForceLoadClass(const FPrimaryAssetId& PrimaryAssetId, bool bLogWarning = true);

	/**
	* Synchronously loads an URPGClassDataAsset subclass, this can hitch but is useful when you cannot wait for an async load
	* This does not maintain a reference to the item so it will garbage collect if not loaded some other way
	*
	* @param PrimaryAssetId The asset identifier to load
	* @param bDisplayWarning If true, this will log a warning if the item failed to load
	*/
	URPGSkillSetDataAsset* ForceLoadSkillSet(const FPrimaryAssetId& PrimaryAssetId, bool bLogWarning = true);

	/**
	* Synchronously loads an URPGClassDataAsset subclass, this can hitch but is useful when you cannot wait for an async load
	* This does not maintain a reference to the item so it will garbage collect if not loaded some other way
	*
	* @param PrimaryAssetId The asset identifier to load
	* @param bDisplayWarning If true, this will log a warning if the item failed to load
	*/
	URPGSkillTierDataAsset* ForceLoadSkillTier(const FPrimaryAssetId& PrimaryAssetId, bool bLogWarning = true);
	
};
