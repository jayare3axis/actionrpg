// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "Components/ActorComponent.h"
#include "RPGPlayerAppearanceComponent.generated.h"

/**
* Handles appearance settings for player.
*/
UCLASS( Blueprintable, Abstract, ClassGroup = Player, meta = (BlueprintSpawnableComponent) )
class ACTIONRPG_API URPGPlayerAppearanceComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Constructor
	URPGPlayerAppearanceComponent();

	/** Blueprint implements this to load appearance option keys for Player Character */
	UFUNCTION(BlueprintImplementableEvent, Category = "Player")
	void InitAppearanceOptions(FName Race, const TArray<int32>& AppearancceOptions);

	/** Blueprint calls this to handle skin for Player Character */
	UFUNCTION(BlueprintCallable, Category = "Player", meta = (DisplayName = "HandleAppearanceOption"))
	void BlueprintHandleAppearanceOption(FName Key, int32 Value);

	/** Raises Handle Appearance Option event */
	UFUNCTION()
	void HandleAppearanceOption(FName Key, int32 Value);

protected:
	UFUNCTION()
	void OnRep_AppearanceOptions();

	/** Appearance options data */
	UPROPERTY(BlueprintReadWrite, ReplicatedUsing = OnRep_AppearanceOptions, Category = "Player")
	TArray<FRPGNamedAppearanceOption> AppearanceOptions;

};
