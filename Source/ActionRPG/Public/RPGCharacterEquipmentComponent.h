// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "Items/RPGItemTypes.h"
#include "Items/RPGItemAtSlot.h"
#include "Components/ActorComponent.h"
#include "RPGCharacterEquipmentComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FItemEquippedSignature, FRPGItemSlot, Slot, FRPGItemInstance, ItemInstance);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FItemUnequippedSignature, FRPGItemSlot, Slot);

UCLASS( Blueprintable, ClassGroup=(Character), meta=(BlueprintSpawnableComponent) )
class ACTIONRPG_API URPGCharacterEquipmentComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Constructor
	URPGCharacterEquipmentComponent();

	/** Equips an item. */
	UFUNCTION(BlueprintCallable, Category = "Character")
	void EquipItem(const FRPGItemSlot& Slot, const FRPGItemInstance& Item);

	/** Equips an item. */
	UFUNCTION(BlueprintCallable, Category = "Character")
	FRPGItemInstance UnequipItem(const FRPGItemSlot& Slot);

	UFUNCTION(BlueprintImplementableEvent, Category = "Character")
	void EquipItemImplementation(const FRPGItemSlot& Slot, URPGItem* Item);

	/** Equips an item. */
	UFUNCTION(BlueprintImplementableEvent, Category = "Character")
	void UnequipItemImplementation(const FRPGItemSlot& Slot, URPGItem* Item);

	/** Used by player character to handle visuals determined by equipment */
	UPROPERTY(BlueprintReadWrite, ReplicatedUsing = OnRep_Equipment, Category = "Character")
	TArray<FRPGItemAtSlot> Equipment;

	/** Holds previous equipment state */
	UPROPERTY()
	TArray<FRPGItemAtSlot> PrevEquipment;

	UFUNCTION()
	void OnRep_Equipment();

protected:
	/** Used by UI to bind item equiped event */
	UPROPERTY(BlueprintAssignable, meta = (DisplayName = "ItemEquiped"))
	FItemEquippedSignature ReceiveItemEquipped;

	/** Used by UI to bind item unequiped event */
	UPROPERTY(BlueprintAssignable, meta = (DisplayName = "ItemUnequiped"))
	FItemUnequippedSignature ReceiveItemUnequipped;

	UPROPERTY(BlueprintReadonly , Category = "Character")
	TMap<FRPGItemSlot, FRPGItemInstance> Items;

};
