// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "UObject/Interface.h"
#include "RPGDynamicTransparencyInterface.generated.h"

UINTERFACE(MinimalAPI)
class URPGDynamicTransparencyInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * Interface of all actors that should be semi transparent if they overlap player character
 */
class ACTIONRPG_API IRPGDynamicTransparencyInterface
{
	GENERATED_BODY()

public:
	/**
	* Called to turn semi transparency on or off
	*/
	UFUNCTION()
	virtual void MakeSemiTransparent(bool val);
	
};
