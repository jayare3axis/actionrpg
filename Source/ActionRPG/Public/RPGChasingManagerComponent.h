// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "Components/ActorComponent.h"
#include "RPGCharacterChasingTarget.h"
#include "RPGChasingManagerComponent.generated.h"

// Forward
class ARPGCharacterBase;

/**
 * Aggro component can use this to get the best chasing target actor to chase the character where this component is used.
*/
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ACTIONRPG_API URPGChasingManagerComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Constructor
	URPGChasingManagerComponent();

	/** Assigns aggro characters to registered chasing targets. */
	UFUNCTION()
	void Process();

	/** Called when actor is received by aggro. */
	UFUNCTION()
	void ReceivedByAggro(ARPGCharacterBase* Aggro);

	/** Called when actor is not received by aggro anymore. */
	UFUNCTION()
	void LostByAggro(ARPGCharacterBase* Aggro);

	/** Registers chasing target to use. */
	UFUNCTION(BlueprintCallable, Category = "AI")
	void RegisterChasingTarget(ARPGCharacterChasingTarget* ChasingTarget);

	/** Gets chasing target for aggro. */
	UFUNCTION(BlueprintCallable, Category = "AI")
	AActor* GetChasingTargetForAggro(ARPGCharacterBase* Aggro);
			
private:
	UFUNCTION()
	void ProcessAggro(ARPGCharacterBase* Aggro);

	UPROPERTY()
	TArray<ARPGCharacterChasingTarget*> Targets;

	UPROPERTY()
	TSet<ARPGCharacterBase*> Aggros;

	UPROPERTY()
	TMap<ARPGCharacterBase*, AActor*> Results;

	UPROPERTY()
	float ChaseCharInsteadTreshold;

};
