// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "Components/ActorComponent.h"
#include "Abilities/RPGAbilitySystemComponent.h"
#include "Abilities/RPGAttributeSet.h"
#include "Items/RPGActiveEffectAtSlot.h"
#include "RPGSlottedAbilitiesInterface.h"
#include "RPGGameplayEffect.h"
#include "RPGDamageInfo.h"
#include "RPGCombatComponent.generated.h"

// Forward
class ARPGCharacterBase;
class ARPGPlayerControllerBase;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FAttributeChangedSignature, EAttributeName, AttributeName, float, Value);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnDamagedSignature, FRPGDamageInfo, DamageInfo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDeathSignature);

/**
* This component handles a bit higher level management of the gameplay ability system.
*/
UCLASS( Blueprintable, Abstract, ClassGroup = Combat, meta=(BlueprintSpawnableComponent) )
class ACTIONRPG_API URPGCombatComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Constructor
	URPGCombatComponent();

	UFUNCTION()
	ARPGCharacterBase* GetOwnerCharacter() const;

	UFUNCTION()
	void GrantAbilities();

	UFUNCTION()
	void ResetSlottedAbilitiesSource();

	/** Uses target controller as Slotted Abilities Source for the owner of this component */
	UFUNCTION()
	void InitSlottedAbilitiesSourceAsController(AController* NewController);

	/** Allows setting abilities, makes sense only when called before character is possesed by a controller */
	UFUNCTION()
	void InitAbilities(const TArray<FAbilityAndLevel>& GameplayAbilities, const TMap<FRPGItemSlot, FAbilityAndLevel>& DefaultSlottedAbilities, const TArray<FEffectAndLevel>& PassiveGameplayEffects);

	/** Cancels attack if character is performing any (melee attack or skill) */
	UFUNCTION(BlueprintCallable, Category = "Attack")
	void CancelAnyAttack();

	/** Called when owner character acquires new skill */
	UFUNCTION(BlueprintNativeEvent, Category = "Abilities")
	void OnNewSkillAcquired();

	/** Levels up owner character, only for debuging purposes */
	UFUNCTION()
	void ForceLevelUp();

	/** Called whenever atribute changes to propagate changes into view */
	UFUNCTION(BlueprintCallable, Category = "Abilities")
	void NotifyAttributesChanged();

#pragma region Abilities activation
	/** Tries to activate ability based on slot */
	UFUNCTION(BlueprintCallable, Category = "Combat")
	bool TryAbilityByItemSlot(FRPGItemSlot Slot);
#pragma endregion

#pragma region Combo
	/** Called by blueprint when combo animation montage started */
	UFUNCTION(BlueprintCallable, Category = "Combat")
	void StartCombo();

	/** Cancels current combo move **/
	UFUNCTION(BlueprintCallable, Category = "Combat")
	void CancelCombo();

	/** Gets current combo section name **/
	UFUNCTION(BlueprintCallable, Category = "Combat")
	FName GetCurrentComboSectionName() const;
#pragma endregion

#pragma region Aggro
	/** Called by blueprint when character targeted this as aggro */
	UFUNCTION(BlueprintCallable, Category = "Combat")
	void RecievedByAggro(ARPGCharacterBase* AggroChar);

	/** Called by blueprint when character targeted this as aggro */
	UFUNCTION(BlueprintCallable, Category = "Combat")
	void LostByAggro(ARPGCharacterBase* AggroChar);
#pragma endregion

	/** Returns true if health > 0 */
	UFUNCTION(BlueprintCallable, Category = "Stats")
	bool IsAlive();

	/** Returns true if character is using melee */
	UFUNCTION(BlueprintCallable, Category = "Abilities")
	bool IsUsingMelee();

	/** Returns true if character is using skill */
	UFUNCTION(BlueprintCallable, Category = "Abilities")
	bool IsUsingSkill();

	/** Called by save game to initialize attributes which are stored by save game */
	UFUNCTION()
	void InitAttributesBySaveGameData(float Level, float Experience, float AvailableSkillPoints, float Health, float Mana);

	// Friended
	friend URPGAttributeSet;
	friend ARPGPlayerControllerBase;

protected:
#pragma region Abilities management
	/** Apply the startup gameplay abilities and effects */
	void AddStartupGameplayAbilities();

	/** Attempts to remove any startup gameplay abilities */
	void RemoveStartupGameplayAbilities();

	/** Called by AddStartupGameplayAbilities to apply start up passives */
	void AddStartupGameplayEffects();

	/** Adds slotted item abilities if needed */
	void AddSlottedGameplayAbilities();

	/** Adds slotted item effects if needed */
	void AddSlottedGameplayEffects();

	/** Removes all gameplay effects */
	void RemoveAllGameplayEffects();

	/** Fills in with ability specs, based on defaults and slotted abilties */
	void FillSlottedAbilitySpecs(TMap<FRPGItemSlot, FGameplayAbilitySpec>& SlottedAbilitySpecs);

	/** Called when slotted items change, bound to delegate on interface */
	void OnItemSlotChanged(FRPGItemSlot ItemSlot, URPGItem* Item);

	/** Called to update slotted gameplay abilities */
	void RefreshSlottedGameplayAbilities();

	/** Called by RefreshSlottedGameplayAbilities to apply changes to passives aswell */
	void RefreshSlottedGameplayEffects();

	/** Remove slotted gameplay abilities, if force is false it only removes invalid ones */
	void RemoveSlottedGameplayAbilities(bool bRemoveAll);

	/** Removes slotted gameplay effects */
	void RemoveSlottedGameplayEffects();
#pragma endregion

#pragma region Abilities activation internal
	/**
	* Attempts to activate any ability in the specified item slot. Will return false if no activatable ability found or activation fails
	* Returns true if it thinks it activated, but it may return false positives due to failure later in activation.
	* If bAllowRemoteActivation is true, it will remotely activate local/server abilities, if false it will only try to locally activate the ability
	*/
	UFUNCTION(BlueprintCallable, Category = "Abilities")
	bool ActivateAbilitiesWithItemSlot(FRPGItemSlot ItemSlot, bool bAllowRemoteActivation = true);

	/** Returns a list of active abilities bound to the item slot. This only returns if the ability is currently running */
	UFUNCTION(BlueprintCallable, Category = "Abilities")
	void GetActiveAbilitiesWithItemSlot(FRPGItemSlot ItemSlot, TArray<URPGGameplayAbility*>& ActiveAbilities);

	/**
	* Attempts to activate all abilities that match the specified tags
	* Returns true if it thinks it activated, but it may return false positives due to failure later in activation.
	* If bAllowRemoteActivation is true, it will remotely activate local/server abilities, if false it will only try to locally activate the ability
	*/
	UFUNCTION(BlueprintCallable, Category = "Abilities")
	bool ActivateAbilitiesWithTags(FGameplayTagContainer AbilityTags, bool bAllowRemoteActivation = true);

	/** Returns a list of active abilities matching the specified tags. */
	UFUNCTION(BlueprintCallable, Category = "Abilities")
	void GetActiveAbilitiesWithTags(FGameplayTagContainer AbilityTags, TArray<URPGGameplayAbility*>& ActiveAbilities);
#pragma endregion

#pragma region Attributes
	/** Returns current attribute value */
	UFUNCTION(BlueprintCallable)
	virtual float GetAttributeValue(EAttributeName AttributeName) const;

	/** Called when attribute is changed */
	UFUNCTION(BlueprintImplementableEvent)
	void OnAttributeChanged(EAttributeName AttributeName, float DeltaValue, const struct FGameplayTagContainer& EventTags);

	/** Multicast broadcast for attribute changed signature */
	UFUNCTION(NetMulticast, Reliable)
	void MulticastBroadcastAttributeChanged(EAttributeName AttributeName, float Value);

	/** Multicast wrapper for OnDamage function */
	UFUNCTION(NetMulticast, Reliable)
	void MulticastBroadcastOnDamaged(float DamageAmount, const FHitResult& HitInfo, const struct FGameplayTagContainer& DamageTags, ARPGCharacterBase* InstigatorCharacter, AActor* DamageCauser);

	/** Multicast wrapper for on death function */
	UFUNCTION(NetMulticast, Reliable)
	void MulticastBroadcastOnDeath();

	virtual void HandleDamage(float DamageAmount, const FHitResult& HitInfo, const struct FGameplayTagContainer& DamageTags, ARPGCharacterBase* InstigatorCharacter, AActor* DamageCauser);
	virtual void HandleAttributeChanged(EAttributeName AttributeName, float DeltaValue, const struct FGameplayTagContainer& EventTags);
#pragma endregion

	/** Don't allow interrupting skills with another ability, or while dead. Blueprints can override this */
	UFUNCTION(BlueprintNativeEvent, Category = "Abilities")
	bool CanUseAnyAbility();

	/** Cached pointer to the slotted abilities source for the owner of the component, can be null */
	UPROPERTY()
	TScriptInterface<IRPGSlottedAbilitiesInterface> SlottedAbilitiesSource;

	/** Map of slot to ability granted by that slot */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Abilities Runtime")
	TMap<FRPGItemSlot, FGameplayAbilitySpecHandle> SlottedAbilities;

	/** Map of slot to effect granted by that slot */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Abilities Runtime")
	TArray<FRPGActiveEffectAtSlot> SlottedEffects;

	/** Abilities to grant to this character on creation. Add here abilities that all derived classes should be granted, like movement */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Abilities")
	TArray<FAbilityAndLevel> CommonGameplayAbilities;

	/** Abilities to grant to this character on creation. These will be activated by tag or event and are not bound to specific inputs */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Abilities")
	TArray<FAbilityAndLevel> GameplayAbilities;

	/** Map of item slot to gameplay ability class, these are bound before any abilities added by the slotted abilities */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Abilities)
	TMap<FRPGItemSlot, FAbilityAndLevel> DefaultSlottedAbilities;

	/** Passive gameplay effects applied on creation */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Abilities")
	TArray<FEffectAndLevel> PassiveGameplayEffects;

	/** Defines combo section for the character */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Abilities")
	TArray<FName> ComboSection;

	/** Used by UI to bind to attribute changed event */
	UPROPERTY(BlueprintAssignable, meta = (DisplayName = "AttributeChanged"))
	FAttributeChangedSignature ReceiveAttributeChanged;

	/** Used by blueprints to bind to on damaged changed event */
	UPROPERTY(BlueprintAssignable, meta = (DisplayName = "OnDamaged"))
	FOnDamagedSignature ReceiveOnDamaged;

	/** Used by blueprints to bind to on death event */
	UPROPERTY(BlueprintAssignable, meta = (DisplayName = "OnDeath"))
	FOnDeathSignature ReceiveOnDeath;

private:
	/** If true we have initialized our abilities */
	UPROPERTY()
	bool bAbilitiesInitialized;

	UPROPERTY()
	bool bEnableComboPeriod;

	UPROPERTY()
	FName CurrentComboSectionName;

	UPROPERTY()
	uint8 ComboSectionIndex;

	UPROPERTY()
	FTimerHandle CheckComboTimerHandle;

	FDelegateHandle SlottedAbilitesUpdateHandle;
	FDelegateHandle SlottedAbilitiesLoadedHandle;

};
