// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "GameFramework/Actor.h"
#include "RPGInteractableInterface.h"
#include "RPGHighlightableInterface.h"
#include "RPGInteractableActor.generated.h"

// Forward
class ARPGPlayerControllerBase;

/**
* An actor implementing Interactable and highlightable interfaces
*/
UCLASS()
class ACTIONRPG_API ARPGInteractableActor : public AActor, public IRPGInteractableInterface, public IRPGHighlightableInterface
{
	GENERATED_BODY()
	
public:	
	// Constructor
	ARPGInteractableActor();
	
	// IInteractable
	virtual void Interact(ARPGPlayerControllerBase* PC) override;
	virtual void InteractClient(ARPGPlayerControllerBase* PC) override;
	virtual void StopInteraction(ARPGPlayerControllerBase* PC) override;
	virtual bool CanBeInteract() override;
	// IHighlightable
	virtual void Highlighted(bool Value) override;
	virtual bool CanBeHighlighted() override;

	UFUNCTION(BlueprintImplementableEvent, Category = "Interactable", meta = (DisplayName = "Interact"))
	void BlueprintInteract(ARPGPlayerControllerBase* PC);

	UFUNCTION(BlueprintImplementableEvent, Category = "Interactable", meta = (DisplayName = "Interact Client"))
	void BlueprintInteractClient(ARPGPlayerControllerBase* PC);

	UFUNCTION(BlueprintImplementableEvent, Category = "Interactable", meta = (DisplayName = "Stop Interaction"))
	void BlueprintStopInteraction(ARPGPlayerControllerBase* PC);
	
	UFUNCTION(BlueprintImplementableEvent, Category = "Interactable", meta = (DisplayName = "Can Be Interact"))
	bool BlueprintCanBeInteract();

	UFUNCTION(BlueprintImplementableEvent, Category = "Highlightable", meta = (DisplayName = "Can Be Highlighted"))
	bool BlueprintCanBeHighlighted();

	UFUNCTION(BlueprintImplementableEvent, Category = "Highlightable", meta = (DisplayName = "Highlighted"))
	void BlueprintHighlighted(bool Value);

};
