// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "GameFramework/Volume.h"
#include "RPGEnemyCrowdDataAsset.h"
#include "RPGSpawnEnemy.h"
#include "RPGSpawnEnemiesVolume.generated.h"

/**
 * Spawns enemies within a volume.
 */
UCLASS()
class ACTIONRPG_API ARPGSpawnEnemiesVolume : public AVolume
{
	GENERATED_BODY()

public:
	// Constructor
	ARPGSpawnEnemiesVolume();

	UFUNCTION()
	virtual void BeginPlay() override;

	/** Spawns enemies */
	UFUNCTION(BlueprintCallable, Category = "Enemies")
	void SpawnEnemies(URPGEnemyCrowdDataAsset* Data);

protected:
	/** Spawn enemy class. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Enemies")
	TSubclassOf<ARPGSpawnEnemy> SpawnEnemyClass;

	/** Chooses random crowd at the begin play to spawn. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Enemies")
	TArray<URPGEnemyCrowdDataAsset*> Enemies;

	/** Min distance from the others needed to spawn sucessfully */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Enemies")
	float MinDistance;

	/** Shifts Z up before spawning */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Enemies")
	float AdjustZ;

	/** Determines maximum elevation at which we can span the loot */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Enemies")
	int32 MaxElevation;

private:
	UFUNCTION()
	bool TryFindSpawnLocation(const TArray<FVector>& UsedLocations, FVector& SpawnLocation);

	UFUNCTION()
	bool CanPositionSpawnEnemy(const FVector& InPosition);
	
};
