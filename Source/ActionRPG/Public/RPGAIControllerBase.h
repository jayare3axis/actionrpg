// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "AIController.h"
#include "RPGAIControllerBase.generated.h"

DECLARE_MULTICAST_DELEGATE_TwoParams(FMoveCompletedDelegate, FAIRequestID, EPathFollowingResult::Type);

/**
 * Base class of AI controllers, should be blueprinted.
 */
UCLASS()
class ACTIONRPG_API ARPGAIControllerBase : public AAIController
{
	GENERATED_BODY()

public:
	// Constructor and overrides
	ARPGAIControllerBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	virtual void OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result) override;

	FMoveCompletedDelegate OnMoveEnd;
	
};
