// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "GameFramework/HUD.h"
#include "RPGInitializationInterface.h"
#include "RPGHUDBase.generated.h"

// Forward
class ARPGCharacterBase;

/**
 * Base class for HUD, should be blueprinted.
 */
UCLASS()
class ACTIONRPG_API ARPGHUDBase : public AHUD, public IRPGInitializationInterface
{
	GENERATED_BODY()

public:
	/** Constructor and overrides */
	ARPGHUDBase();
	virtual void BeginPlay() override;

	// IInitializationInterface
	virtual void OnCharacterCreated() override;
	virtual void OnHUDCreated() override;
	virtual void OnReady() override;

	/** Passes character instance of a player to a blueprint */
	UFUNCTION(BlueprintNativeEvent, Category = "UI")
	void BindCharacter();

	/** Toggles inventory */
	UFUNCTION()
	void ToggleInventory();

	/** Toggles character sheet */
	UFUNCTION()
	void ToggleCharacterSheet();

	/** Toggles skill sheet */
	UFUNCTION()
	void ToggleSkillSheet();

	/** Opens up the inventory window */
	UFUNCTION(BlueprintImplementableEvent, Category = "UI")
	void OpenInventory();

	/** Opens up the character sheet window */
	UFUNCTION(BlueprintImplementableEvent, Category = "UI")
	void OpenCharacterSheet();

	/** Opens up the skill sheet window */
	UFUNCTION(BlueprintImplementableEvent, Category = "UI")
	void OpenSkillSheet();

	/** Closes the inventory window */
	UFUNCTION(BlueprintImplementableEvent, Category = "UI")
	void CloseInventory(bool bDontPlaySound = false);

	/** Closes character sheet window */
	UFUNCTION(BlueprintImplementableEvent, Category = "UI")
	void CloseCharacterSheet(bool bDontPlaySound = false);

	/** Closes skill sheet window */
	UFUNCTION(BlueprintImplementableEvent, Category = "UI")
	void CloseSkillSheet(bool bDontPlaySound = false);

	/** Closes skill sheet bindings window */
	UFUNCTION(BlueprintImplementableEvent, Category = "UI")
	void CloseSkillBindings();

	/** Allows blueprints to open inventory */
	UFUNCTION(BlueprintCallable, meta=(DisplayName = "Open Inventory"), Category = "UI")
	void BlueprintOpenInventory();

	/** Allows blueprints to open character sheet */
	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Open Character Sheet"), Category = "UI")
	void BlueprintOpenCharacterSheet();

	/** Allows blueprints to open skill sheet */
	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Open Skill Sheet"), Category = "UI")
	void BlueprintOpenSkillSheet();

	/** Allows blueprints to open inventory */
	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Close Inventory"), Category = "UI")
	void BlueprintCloseInventory(bool bDontPlaySound = false);

	/** Allows blueprints to close character sheet */
	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Close Character Sheet"), Category = "UI")
	void BlueprintCloseCharacterSheet(bool bDontPlaySound = false);

	/** Allows blueprints to close skill sheet */
	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Close Skill Sheet"), Category = "UI")
	void BlueprintCloseSkillSheet(bool bDontPlaySound = false);

	/** Returns true if any UI window is currently open */
	UFUNCTION(BlueprintCallable, Category = "UI")
	bool IsAnyWindowOpen() const;

	/** Closes all the windows */
	UFUNCTION(BlueprintCallable, Category = "UI")
	void CloseAllWindows();

protected:
	UPROPERTY(BlueprintReadonly, Category = "UI")
	bool bIsInventoryOpen;

	UPROPERTY(BlueprintReadonly, Category = "UI")
	bool bIsCharacterSheetOpen;

	UPROPERTY(BlueprintReadonly, Category = "UI")
	bool bIsSkillSheetOpen;

};
