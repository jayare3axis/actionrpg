// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "GameFramework/GameState.h"
#include "RPGGameStateBase.generated.h"

// Forward
class ARPGFactionManagerBase;
class ARPGLootTileManagerBase;

/**
 * Base class for GameState, also manages singleton actors, should be blueprinted
 */
UCLASS()
class ACTIONRPG_API ARPGGameStateBase : public AGameStateBase
{
	GENERATED_BODY()

public:
	/** Constructor */
	ARPGGameStateBase();

};
