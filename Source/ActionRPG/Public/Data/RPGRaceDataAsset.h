// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "Engine/DataAsset.h"
#include "RPGCharacterBase.h"
#include "RPGRaceDataAsset.generated.h"

/**
 * Data related to a player race.
 */
UCLASS(BlueprintType)
class ACTIONRPG_API URPGRaceDataAsset : public UPrimaryDataAsset
{
	GENERATED_BODY()

public:
	/** Constructor */
	URPGRaceDataAsset()
	{}

	/** Holds primary asset type. */
	static const FPrimaryAssetType PrimaryAssetType;

	/** Id */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "General")
	FName Key;

	/** For sorting purposes */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "General")
	int32 Order;
	
	/** Display Name */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "General")
	FText DisplayName;

	/** Icon */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "General")
	UTexture2D* Icon;

	/** Description */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "General")
	FText Description;

	/** Character class */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "General")
	TSubclassOf<ARPGCharacterBase> CharacterClass;

	/** Intro anim */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "General")
	UAnimMontage* IntroAnim;

	/** Appearance options */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "General")
	TArray<FRPGPlayerAppearanceOptions> AppearanceOptions;

	/** Returns the logical name, equivalent to the primary asset id */
	UFUNCTION(BlueprintCallable, Category = "Item")
	FString GetIdentifierString() const;

	/** Overridden to use saved type */
	virtual FPrimaryAssetId GetPrimaryAssetId() const override;

};
