// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "Engine/DataAsset.h"
#include "RPGPartAssetBase.generated.h"

/**
 * Base class of parts used by skin component to achieve customizable characters.
 */
UCLASS(Abstract)
class ACTIONRPG_API URPGPartAssetBase : public UPrimaryDataAsset
{
	GENERATED_BODY()

public:
	/** Applies part, called from blueprint */
	UFUNCTION(BlueprintCallable, Category = "Skin", meta = (DisplayName = "Apply"))
	void BlueprintApply(USkeletalMeshComponent* SkeletalMesh);

	/** Apply implementation */
	UFUNCTION(BlueprintNativeEvent, Category = "Skin")
	void Apply(USkeletalMeshComponent* SkeletalMesh);

protected:
	virtual void ApplyInternal(USkeletalMeshComponent* SkeletalMesh);
	
};
