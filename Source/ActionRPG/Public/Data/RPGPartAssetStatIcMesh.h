// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "RPGPartAssetBase.h"
#include "RPGPartAssetStatIcMesh.generated.h"

/**
 * Static mesh part.
 */
UCLASS()
class ACTIONRPG_API URPGPartAssetStatIcMesh : public URPGPartAssetBase
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Mesh")
	UStaticMesh* StaticMesh;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Mesh")
	FName StaticMeshTag;

protected:
	virtual void ApplyInternal(USkeletalMeshComponent* SkeletalMesh) override;
	
};
