// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "Engine/DataAsset.h"
#include "RPGSkillSetDataAsset.generated.h"

// Forward
class URPGSkillTierDataAsset;

/**
 * Data about a player Skill Set.
 */
UCLASS()
class ACTIONRPG_API URPGSkillSetDataAsset : public UPrimaryDataAsset
{
	GENERATED_BODY()

public:
	/** Constructor */
	URPGSkillSetDataAsset()
	{}
	
	/** Holds primary asset type. */
	static const FPrimaryAssetType PrimaryAssetType;

	/** Id */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "General")
	FName Key;

	/** For sorting purposes */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "General")
	int32 Order;

	/** Display Name */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "General")
	FText DisplayName;

	/** Tiers */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Skills")
	TSet<URPGSkillTierDataAsset*> Tiers;

	/** Returns the logical name, equivalent to the primary asset id */
	UFUNCTION(BlueprintCallable, Category = "Item")
	FString GetIdentifierString() const;

	/** Overridden to use saved type */
	virtual FPrimaryAssetId GetPrimaryAssetId() const override;

};
