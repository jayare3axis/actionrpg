// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "Engine/DataAsset.h"
#include "Items/RPGSkillItem.h"
#include "RPGSkillTierDataAsset.generated.h"

/**
 * Holds skills related to this tier.
 */
UCLASS()
class ACTIONRPG_API URPGSkillTierDataAsset : public UPrimaryDataAsset
{
	GENERATED_BODY()

public:

	/** Constructor */
	URPGSkillTierDataAsset()
	{}

	/** Holds primary asset type. */
	static const FPrimaryAssetType PrimaryAssetType;

	/** Id */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "General")
	FName Key;

	/** Defines level of skill tier */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "General")
	int32 Level;

	/** Display Name */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "General")
	FText DisplayName;
	
	/** Skills related to this tier, first skill in the array is considered to be primary skill */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Abilities")
	TArray<URPGSkillItem*> Skills;

	/** Skill tiers related to this tier */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Abilities")
	TArray<URPGSkillTierDataAsset*> RelatedTiers;

	/** Returns the logical name, equivalent to the primary asset id */
	UFUNCTION(BlueprintCallable, Category = "Item")
	FString GetIdentifierString() const;

	/** Overridden to use saved type */
	virtual FPrimaryAssetId GetPrimaryAssetId() const override;
	
};
