// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "RPGPartAssetBase.h"
#include "RPGPartAssetSocketedActor.generated.h"

/**
 * Socketed actor part.
 */
UCLASS()
class ACTIONRPG_API URPGPartAssetSocketedActor : public URPGPartAssetBase
{
	GENERATED_BODY()
	
public:
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Mesh")
	TSubclassOf<AActor> ActorClass;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Mesh")
	FName SocketName;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Mesh")
	FRotator Rotation;

protected:
	virtual void ApplyInternal(USkeletalMeshComponent* SkeletalMesh) override;

};
