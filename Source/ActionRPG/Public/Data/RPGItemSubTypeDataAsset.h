// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "RPGItemSubTypeDataAsset.generated.h"

/**
 * Can be used to categorize any RPG item.
 */
UCLASS()
class ACTIONRPG_API URPGItemSubTypeDataAsset : public UDataAsset
{
	GENERATED_BODY()

protected:
	/** Display name */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "General")
	FText DisplayName;
	
};
