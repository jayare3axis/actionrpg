// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "Engine/DataAsset.h"
#include "RPGEnemyDataAsset.h"
#include "RPGEnemyCrowdDataAsset.generated.h"

/**
 * Defines a group of enemies to spawn, can include a boss.
 */
UCLASS(BlueprintType)
class ACTIONRPG_API URPGEnemyCrowdDataAsset : public UDataAsset
{
	GENERATED_BODY()

public:
	// Constructor
	URPGEnemyCrowdDataAsset();

	FORCEINLINE
	const TArray<URPGEnemyDataAsset*>& GetEnemies() const { return Enemies; }

	FORCEINLINE
	bool HasBoss() const { return bHasBoss; }
	
protected:
	/** Enemies to spawn. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "General")
	TArray<URPGEnemyDataAsset*> Enemies;

	/** If set to true means that the first enemy spawned is the boss. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "General")
	bool bHasBoss;

};
