// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "RPGPartAssetBase.h"
#include "RPGPartAssetSkeletalMesh.generated.h"

/**
 * Skeletal mesh part.
 */
UCLASS()
class ACTIONRPG_API URPGPartAssetSkeletalMesh : public URPGPartAssetBase
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Mesh")
	USkeletalMesh* SkeletalMesh;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Mesh")
	FName SkeletalMeshTag;

protected:
	virtual void ApplyInternal(USkeletalMeshComponent* SkeletalMesh) override;

};
