// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "Items/RPGItemAtSlot.h"
#include "Engine/DataAsset.h"
#include "RPGClassDataAsset.generated.h"

/**
 * Data related to player class.
 */
UCLASS()
class ACTIONRPG_API URPGClassDataAsset : public UPrimaryDataAsset
{
	GENERATED_BODY()

public:
	/** Constructor */
	URPGClassDataAsset()
	{}

	/** Holds primary asset type. */
	static const FPrimaryAssetType PrimaryAssetType;

	/** Id */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "General")
	FName Key;

	/** For sorting purposes */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "General")
	int32 Order;

	/** Display Name */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "General")
	FText DisplayName;

	/** Icon */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "General")
	UTexture2D* Icon;

	/** Description */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "General")
	FText Description;

	/** Starter items */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "General")
	TArray<FRPGItemAtSlot> StarterItems;

	/** Returns the logical name, equivalent to the primary asset id */
	UFUNCTION(BlueprintCallable, Category = "Item")
	FString GetIdentifierString() const;

	/** Overridden to use saved type */
	virtual FPrimaryAssetId GetPrimaryAssetId() const override;
	
};
