// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "Engine/DataAsset.h"
#include "RPGCharacterBase.h"
#include "RPGAIControllerBase.h"
#include "RPGEnemyDataAsset.generated.h"

/**
 * Holds data used to spawn an enemy.
 */
UCLASS()
class ACTIONRPG_API URPGEnemyDataAsset : public UDataAsset
{
	GENERATED_BODY()

public:
	FORCEINLINE
	TSubclassOf<ARPGCharacterBase> GetCharacterClass() const { return CharacterClass; }

	FORCEINLINE
	FText GetDisplayName() const { return DisplayName; }

	FORCEINLINE
	TSubclassOf<ARPGAIControllerBase> GetAIControllerClass() const { return AIControllerClass; }

	FORCEINLINE
	TArray<FAbilityAndLevel> GetGameplayAbilities() const { return GameplayAbilities; }

	FORCEINLINE
	TMap<FRPGItemSlot, FAbilityAndLevel> GetDefaultSlottedAbilities() const { return DefaultSlottedAbilities; }

	FORCEINLINE
	TArray<FEffectAndLevel> GetPassiveGameplayEffects() const { return PassiveGameplayEffects; }

protected:
	/** A character to spawn */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "General")
	TSubclassOf<ARPGCharacterBase> CharacterClass;

	/** AI Controller to use for the spawned enemy */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AI")
	TSubclassOf<ARPGAIControllerBase> AIControllerClass;

	/** Abilities to grant to this enemy on creation. These will be activated by tag or event and are not bound to specific inputs */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Abilities")
	TArray<FAbilityAndLevel> GameplayAbilities;

	/** Map of item slot to gameplay ability class, these are bound before any abilities added by the slotted abilities */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Abilities")
	TMap<FRPGItemSlot, FAbilityAndLevel> DefaultSlottedAbilities;

	/** Passive gameplay effects applied on creation */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Abilities")
	TArray<FEffectAndLevel> PassiveGameplayEffects;

	/** Sets display name for the enemy */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "UI")
	FText DisplayName;
	
};
