// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "UObject/Interface.h"
#include "RPGCombatComponent.h"
#include "RPGCombatInterface.generated.h"

/*
* Interface used to access Combat System Component.
*/
UINTERFACE(Blueprintable)
class URPGCombatInterface : public UInterface
{
	GENERATED_BODY()
};

class ACTIONRPG_API IRPGCombatInterface
{
	GENERATED_BODY()

public:
	/** Returns the ability system component to use for this actor. */
	virtual URPGCombatComponent* GetCombatComponent() = 0;

};
