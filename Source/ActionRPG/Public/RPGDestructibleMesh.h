// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "DestructibleActor.h"
#include "RPGInteractableInterface.h"
#include "RPGHighlightableInterface.h"
#include "RPGDestructibleMesh.generated.h"

/**
 * A mesh that on interaction triggers player attack so it then can destroy itself
 */
UCLASS()
class ACTIONRPG_API ARPGDestructibleMesh : public ADestructibleActor, public IRPGInteractableInterface, public IRPGHighlightableInterface
{
	GENERATED_BODY()

public:
	// Constructor
	ARPGDestructibleMesh();

	// IInteractable
	virtual bool CanBeInteract();
	virtual bool ShouldAttackOnInteraction(ARPGPlayerControllerBase* PC) override;
	// IHighlightable
	virtual void Highlighted(bool Value) override;
	virtual bool CanBeHighlighted();

	/** Implement in blueprint but don't forget to call base implementation to disable highlighting */
	UFUNCTION(BlueprintNativeEvent, Category = "Destructible", meta = (DisplayName = "Take Damage"))
	void TakeDamage();

	/** For use in blueprints */
	UFUNCTION(BlueprintCallable, Category = "Destructible", meta = (DisplayName = "Take Damage"))
	void BlueprintTakeDamage();

	UFUNCTION(BlueprintImplementableEvent, Category = "Interactable", meta = (DisplayName = "Highlighted"))
	void BlueprintHighlighted(bool Value);

private:
	bool bDestroyed;
	
};
