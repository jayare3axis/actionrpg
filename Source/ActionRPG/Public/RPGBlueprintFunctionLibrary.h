// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Abilities/RPGAbilityTypes.h"
#include "Items/RPGItemTypes.h"
#include "Data/RPGRaceDataAsset.h"
#include "Data/RPGClassDataAsset.h"
#include "Data/RPGSkillSetDataAsset.h"
#include "Data/RPGSkillTierDataAsset.h"
#include "RPGBlueprintFunctionLibrary.generated.h"

// Forward
class ARPGPlayerControllerBase;
class ARPGLootTileManagerBase;
class ARPGHUDBase;
class URPGItem;
class URPGGameInstanceBase;

/**
 * Blueprint function library for Action RPG
 */
UCLASS()
class ACTIONRPG_API URPGBlueprintFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	/** Returns game instance */
	UFUNCTION(BlueprintPure, Category = "Utilities", meta = (WorldContext = "WorldContextObject"))
	static URPGGameInstanceBase* GetGameInstance(const UObject* WorldContextObject);

	/** Returns false if game runs without SaveGame data, meaning it spawned Debug Player */
	UFUNCTION(BlueprintPure, Category = "Utilities", meta = (WorldContext = "WorldContextObject"))
	static bool IsSaveGameSession(const UObject* WorldContextObject);

	/** Returns first player controller present */
	UFUNCTION(BlueprintPure, Category = "Utilities", meta = (WorldContext = "WorldContextObject"))
	static ARPGPlayerControllerBase* GetLocalPlayerController(const UObject* WorldContextObject);
		
	/** Returns HUD */
	UFUNCTION(BlueprintPure, Category = "Utilities", meta = (WorldContext = "WorldContextObject"))
	static ARPGHUDBase* GetHUD(const UObject* WorldContextObject);

	/** Gets a player controller based on a character */
	UFUNCTION(BlueprintPure, Category = "Utilities", meta = (WorldContext = "WorldContextObject"))
	static ARPGPlayerControllerBase* GetPlayerControlerByCharacter(const UObject* WorldContextObject, const ARPGCharacterBase* Character);

	/** Returns true if passed controller is local player */
	UFUNCTION(BlueprintPure, Category = "Utilities", meta = (WorldContext = "WorldContextObject"))
	static bool IsLocalPlayer(const UObject* WorldContextObject, ARPGPlayerControllerBase* PC);

	/** Gets available races */
	UFUNCTION(BlueprintCallable, Category = "Utilities", meta = (WorldContext = "WorldContextObject"))
	static URPGRaceDataAsset* GetAvailableRace (const UObject* WorldContextObject, const FName& Key);

	/** Gets available class */
	UFUNCTION(BlueprintCallable, Category = "Utilities", meta = (WorldContext = "WorldContextObject"))
	static URPGClassDataAsset* GetAvailableClass(const UObject* WorldContextObject, const FName& Key);

	/** Gets available skill set */
	UFUNCTION(BlueprintCallable, Category = "Utilities", meta = (WorldContext = "WorldContextObject"))
	static URPGSkillSetDataAsset* GetAvailableSkillSet(const UObject* WorldContextObject, const FName& Key);

	/** Gets available skill tier */
	UFUNCTION(BlueprintCallable, Category = "Utilities", meta = (WorldContext = "WorldContextObject"))
	static URPGSkillTierDataAsset* GetAvailableSkillTier(const UObject* WorldContextObject, const FName& Key);

	/** Gets available races list */
	UFUNCTION(BlueprintCallable, Category = "Utilities", meta = (WorldContext = "WorldContextObject"))
	static void GetAvailableRacesList(const UObject* WorldContextObject, TArray<FRPGOrderedListItem>& Result);

	/** Gets available classes */
	UFUNCTION(BlueprintCallable, Category = "Utilities", meta = (WorldContext = "WorldContextObject"))
	static void GetAvailableClassesList(const UObject* WorldContextObject, TArray<FRPGOrderedListItem>& Result);

	/** Gets available skill sets */
	UFUNCTION(BlueprintCallable, Category = "Utilities", meta = (WorldContext = "WorldContextObject"))
	static void GetAvailableSkillSetsList(const UObject* WorldContextObject, TArray<FRPGOrderedListItem>& Result);

	/** Gets skill tiers based on a skill set */
	UFUNCTION(BlueprintCallable, Category = "Utilities", meta = (WorldContext = "WorldContextObject"))
	static void GetSkillTiersBySkillSet(const UObject* WorldContextObject, FName SkillSetName, TArray<FRPGOrderedListItem>& Result);

	/** Gets skill tiers based on a skill set */
	UFUNCTION(BlueprintCallable, Category = "Utilities", meta = (WorldContext = "WorldContextObject"))
	static bool IsSkillTierUnlockedByPlayer(const UObject* WorldContextObject, const URPGSkillTierDataAsset* SkillTier);

	/** Show the native loading screen, such as on a map transfer. If bPlayUntilStopped is false, it will be displayed for PlayTime and automatically stop */
	UFUNCTION(BlueprintCallable, Category = "Loading")
	static void PlayLoadingScreen(float PlayTime, const UTexture2D* LoadingScreenTexture, const FVector2D& BrushSize);

	/** Determines if item is equipment/weapon or not */
	UFUNCTION(BlueprintPure, Category = "Items")
	static bool IsEquipableSlot(const FRPGItemSlot& ItemSlot);

	/** Creates new item instance */
	UFUNCTION(BlueprintCallable, Category = "Utilities", meta = (WorldContext = "WorldContextObject"))
	static FRPGItemInstance CreateItemInstance(const UObject* WorldContextObject, URPGItem* Item, int32 Count, int32 Level);

	/** Returns needed yaw value difference so the actor will be facing the target */
	UFUNCTION(BlueprintPure, Category = "Utilities", meta = (WorldContext = "WorldContextObject"))
	static float GetTurnToTargetYaw(const UObject* WorldContextObject, const AActor* Actor, const AActor* Target);

	/** Returns needed yaw value difference so the actor will be facing the target */
	UFUNCTION(BlueprintPure, Category = "Utilities", meta = (WorldContext = "WorldContextObject"))
	static float GetTurnToPointYaw(const UObject* WorldContextObject, const AActor* Actor, const FVector& TargetPoint);

	/** Converts hostility ranking to highlight color */
	UFUNCTION(BlueprintPure, Category = "Highlightable", meta = (WorldContext = "WorldContextObject"))
	static void HostilityRankingToHighlightColor(const UObject* WorldContextObject, const EHostilityRanking& Value, FLinearColor& Result);

	/** Helper routine that sets highlight for all related components of actor */
	UFUNCTION(BlueprintCallable, Category = "Highlightable", meta = (WorldContext = "WorldContextObject"))
	static void SetHighlightToActor(const UObject* WorldContextObject, ARPGPlayerControllerBase* PC, AActor* Actor);

	/** Helper routine that clears highlight for all related components of actor */
	UFUNCTION(BlueprintCallable, Category = "Highlightable", meta = (WorldContext = "WorldContextObject"))
	static void ClearHighlightForActor(const UObject* WorldContextObject, ARPGPlayerControllerBase* PC, AActor* Actor);

	/** Attribute to display string */
	UFUNCTION(BlueprintPure, Category = "Ability")
	static FText AttributeToDisplayString(const UObject* WorldContextObject, const FGameplayAttribute& Attribute);

	/** Gets ability cooldown */
	UFUNCTION(BlueprintCallable, Category = "Ability")
	static float GetAbilityCooldown(const UObject* WorldContextObject, URPGGameplayAbility* Ability);

	/** Gets item description */
	UFUNCTION(BlueprintPure, Category = "Ability")
	static void GetItemDescription(const UObject* WorldContextObject, const FRPGItemInstance& ItemInstance, TArray<FRPGItemsDescriptionField>& OutFields);

	/** Adds description of the effect to the out field */
	static void AddEffectDescription(const UObject* WorldContextObject, const TSubclassOf<UGameplayEffect> EffectClass, float Level, TArray<FRPGItemsDescriptionField>& OutFields);

	/** Creates named appearance option */
	UFUNCTION(BlueprintPure, Category = "Ability")
	static FRPGNamedAppearanceOption CreateNamedAppearanceOption(const FName& InKey, int32 InValue);

	/** Converts rarity ranking to text */
	UFUNCTION(BlueprintPure, Category = "Ability")
	static FText RarityRankingToText(ERarityRanking RarityRanking);

	/** Checks if spec has any effects */
	UFUNCTION(BlueprintPure, Category = "Ability")
	static bool DoesEffectContainerSpecHaveEffects(const FRPGGameplayEffectContainerSpec& ContainerSpec);

	/** Checks if spec has any targets */
	UFUNCTION(BlueprintPure, Category = "Ability")
	static bool DoesEffectContainerSpecHaveTargets(const FRPGGameplayEffectContainerSpec& ContainerSpec);

	/** Adds targets to a copy of the passed in effect container spec and returns it */
	UFUNCTION(BlueprintCallable, Category = "Ability", meta = (AutoCreateRefTerm = "HitResults,TargetActors"))
	static FRPGGameplayEffectContainerSpec AddTargetsToEffectContainerSpec(const FRPGGameplayEffectContainerSpec& ContainerSpec, const TArray<FHitResult>& HitResults, const TArray<AActor*>& TargetActors);

	/** Applies container spec that was made from an ability */
	UFUNCTION(BlueprintCallable, Category = "Ability")
	static TArray<FActiveGameplayEffectHandle> ApplyExternalEffectContainerSpec(const FRPGGameplayEffectContainerSpec& ContainerSpec);

	/** Determines if attack is melee, attack is determined melee if it's slashing, piercing or crushing damage type */
	UFUNCTION(BlueprintCallable, Category = "Ability")
	static bool IsMeleeAttack(const FGameplayTagContainer& Tags);
	
};
