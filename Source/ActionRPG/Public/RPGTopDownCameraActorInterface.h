// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "UObject/Interface.h"
#include "RPGTopDownCameraActorInterface.generated.h"

// Forward
class URPGTopDownCameraComponent;

UINTERFACE(MinimalAPI)
class URPGTopDownCameraActorInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * Used to access Top Down Camera Component on Player Character
 */
class ACTIONRPG_API IRPGTopDownCameraActorInterface
{
	GENERATED_BODY()

public:
	/**
	* Gets top down camera component
	*/
	virtual URPGTopDownCameraComponent* GetTopDownCamera();

};
