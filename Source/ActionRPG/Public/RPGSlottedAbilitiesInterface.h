// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "Items/RPGItemAtSlot.h"
#include "RPGSlottedAbilitiesInterface.generated.h"

/**
 * Interface for actors that provide a set of RPGItems bound to ItemSlots
 * This exists so RPGCharacterBase can query slotted abilities without doing hacky player controller casts
 * It is designed only for use by native classes
 */
UINTERFACE(MinimalAPI, meta = (CannotImplementInterfaceInBlueprint))
class URPGSlottedAbilitiesInterface : public UInterface
{
	GENERATED_BODY()
};

class ACTIONRPG_API IRPGSlottedAbilitiesInterface
{
	GENERATED_BODY()

public:
	/** Returns the map of slots to items */
	virtual const TMap<FRPGItemSlot, FRPGItemAtSlot>& GetSlottedItemMap() const = 0;

	/** Gets the delegate for slotted abilities slot changes */
	virtual FOnSlottedItemChangedNative& GetSlottedItemChangedDelegate() = 0;

	/** Gets the delegate for when the inventory loads */
	virtual FOnSlottedAbilitiesLoadedNative& GetSlottedAbilitiesLoadedDelegate() = 0;
};

