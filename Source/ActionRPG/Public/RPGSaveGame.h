// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "GameFramework/SaveGame.h"
#include "Items/RPGItemTypes.h"
#include "RPGSaveGame.generated.h"

/**
 * Used to save or load player character data.
 */
UCLASS()
class ACTIONRPG_API URPGSaveGame : public USaveGame
{
	GENERATED_BODY()
	
public:
	// Constructor
	URPGSaveGame();

	/** Holds name of character */
	UPROPERTY(BlueprintReadonly, VisibleAnywhere, Category = "Data")
	FString CharacterName;

	/** Character race */
	UPROPERTY(BlueprintReadonly, VisibleAnywhere, Category = "Data")
	FName Race;

	/** Character class */
	UPROPERTY(BlueprintReadonly, VisibleAnywhere, Category = "Data")
	FName Class;

	/** Appearance options */
	UPROPERTY(BlueprintReadonly, VisibleAnywhere, Category = "Data")
	TArray<int32> AppearanceOptions;

	/** Slotted items */
	UPROPERTY(BlueprintReadonly, VisibleAnywhere, Category = "Data")
	TArray<FRPGItemAtSlotRaw> Items;

	/** Backpack items */
	UPROPERTY(BlueprintReadonly, VisibleAnywhere, Category = "Data")
	TArray<FRPGItemInContainerRaw> BackPackItems;

	/** Action bindings */
	UPROPERTY(BlueprintReadonly, VisibleAnywhere, Category = "Data")
	TMap<EPlayerActionButton, FRPGItemSlot> ActionBindings;

	/** Character level */
	UPROPERTY(BlueprintReadonly, VisibleAnywhere, Category = "Data")
	int32 Level;

	/** Available skil points */
	UPROPERTY(BlueprintReadonly, VisibleAnywhere, Category = "Data")
	int32 AvailableSkillPoints;

	/** Current health */
	UPROPERTY(BlueprintReadonly, VisibleAnywhere, Category = "Data")
	float Health;

	/** Current mana */
	UPROPERTY(BlueprintReadonly, VisibleAnywhere, Category = "Data")
	float Mana;

	/** Character experience */
	UPROPERTY(BlueprintReadonly, VisibleAnywhere, Category = "Data")
	int32 Experience;
	
};
