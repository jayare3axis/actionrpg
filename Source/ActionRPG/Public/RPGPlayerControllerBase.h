// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "GameFramework/PlayerController.h"
#include "RPGTypes.h"
#include "RPGSlottedAbilitiesInterface.h"
#include "Abilities/GameplayAbility.h"
#include "Items/RPGItemTypes.h"
#include "Items/RPGSkillItem.h"
#include "RPGPlayerMoveDummy.h"
#include "RPGDynamicTransparencyInterface.h"
#include "RPGPlayerControllerBase.generated.h"

// Forward
class ARPGCharacterBase;
class ARPGPlayerAIControllerBase;
class IRPGInteractable;
class URPGItem;
class URPGItemContainerComponent;
class URPGPlayerActionComponent;
class ARPGLootBase;
class URPGSaveGame;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FItemGrabbedSignature, FRPGItemInstance, ItemInstance);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FItemDroppedSignature, FRPGItemInstance, ItemInstance);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FKnownSkillsChangedSignature);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FActionBindingAddedSignature, EPlayerActionButton, Button, FRPGItemSlot, ItemSlot);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FActionBindingRemovedSignature, EPlayerActionButton, Button);

/**
 * Base class for PlayerController, should be blueprinted
 */
UCLASS()
class ACTIONRPG_API ARPGPlayerControllerBase : public APlayerController, public IRPGSlottedAbilitiesInterface
{
	GENERATED_BODY()

public:
	// Constructor and overrides
	ARPGPlayerControllerBase();
	virtual void BeginPlay() override;
	virtual void SetupInputComponent() override;
	virtual void Tick(float DeltaTime) override;
	virtual void Destroyed() override;

	/** Gets backpack component */
	FORCEINLINE URPGItemContainerComponent* GetBackpack() const
	{
		return BackpackItemContainer;
	}

	/** Gets slotted items */
	FORCEINLINE void GetSlottedItems(TMap<FRPGItemSlot, FRPGItemAtSlot>& _SlottedItems) const
	{
		_SlottedItems = SlottedItems;
	}

	/** Spawns player character using save game data */
	UFUNCTION(Server, Reliable, WithValidation, Category = "Save Game")
	void ServerSpawnPlayerCharacterUsingSaveGameData(const FString& CharacterName, FName Race, const TArray<int32>& AppearanceOptions, const TArray<FRPGItemAtSlotRaw>& Items);

	/** Initializes player action byndings using save game data */
	UFUNCTION(Client, Reliable, Category = "Save Game")
	void ClientInitActionBindingsUsingSaveGameData(const TMap<EPlayerActionButton, FRPGItemSlot>& ActionBindings);

	/** Called when server spawns character using save game data */
	UFUNCTION(BlueprintImplementableEvent, Category = "Character", meta = (DisplayName = "OnPlayerCharacterSpawned"))
	void BlueprintOnPlayerCharacterSpawned(ARPGCharacterBase* CharacterSpawned, const FString& CharacterName, FName Race);

	/** Called when character of a player is spawned to override attributes by save game since they are made default each time character is spawned */
	UFUNCTION(Server, Reliable, WithValidation, Category = "Save Game")
	void ServerInitAttributesBySaveGameData(float Level, float Experience, float AvailableSkillPoints, float Health, float Mana);

	/** Initializes backpack items using save game data */
	UFUNCTION(Client, Reliable, Category = "Save Game")
	void ClientInitBackPackItemsBySaveGameData(const TArray<FRPGItemInContainerRaw>& BackPackItems);
	
	/** Returns player character asociated to this controller */
	UFUNCTION(BlueprintCallable, Category = "Character")
	ARPGCharacterBase* GetPlayerCharacter() const;

	/** Blueprint wrapper for ServerLevelUp */
	UFUNCTION(BlueprintCallable, Category = "Character", meta = (DisplayName = "ServerLevelUp"))
	void BlueprintServerLevelUp();

	/** Levels up a player character, for debug purposes only */
	UFUNCTION(Server, Reliable, WithValidation, Category = "Character")
	void ServerLevelUp();

	/** Returns action button determined by current action button */
	UFUNCTION()
	void GetItemSlotByCurrentActionButton(FRPGItemSlot& ItemSlot);

#pragma region Items
	/** Blueprint wrapper for ServerAddItem */
	UFUNCTION(BlueprintCallable, Category = "Items", meta = (DisplayName = "ServerAddItem"))
	void BlueprintServerAddItem(const FRPGItemSlot& ItemSlot, const FRPGItemInstance& ItemInstance, bool bNotify = true);

	/** Adds an item */
	UFUNCTION(Server, Reliable, WithValidation, Category = "Items")
	void ServerAddItem(const FRPGItemSlot& ItemSlot, const FRPGItemInstance& ItemInstance, bool bNotify = true);

	/** Called when item has been added on client machine */
	UFUNCTION(Client, Reliable)
	void ClientOnItemAdded(const FRPGItemSlot& ItemSlot, const FRPGItemInstance& ItemInstance);

	/** Blueprint wrapper for ServerRemoveItem */
	UFUNCTION(BlueprintCallable, Category = "Items", meta = (DisplayName = "ServerRemoveItem"))
	void BlueprintServerRemoveItem(const FRPGItemSlot& ItemSlot, bool bNotify = true);

	/** Adds an item */
	UFUNCTION(Server, Reliable, WithValidation, Category = "Items")
	void ServerRemoveItem(const FRPGItemSlot& ItemSlot, bool bNotify = true);

	/** Called when item has been removed on client machine */
	UFUNCTION(Client, Reliable)
	void ClientOnItemRemoved(const FRPGItemSlot& ItemSlot);

	/** Blueprint wrapper for ServerAcquireSkill */
	UFUNCTION(BlueprintCallable, Category = "Items", meta = (DisplayName = "ServerAcquireSkill"))
	void BlueprintServerAcquireSkill(const FRPGItemInstance& ItemInstance);

	/** Adds an item */
	UFUNCTION(Server, Reliable, WithValidation, Category = "Items")
	void ServerAcquireSkill(const FRPGItemInstance& ItemInstance);

	/** Returns true if character has a given ability. */
	UFUNCTION(BlueprintCallable, Category = "Items")
	bool HasItem(URPGItem* Item);

#pragma endregion

#pragma region Item Instance Interaction
	/** Returns true if player can pick up an item */
	UFUNCTION(BlueprintCallable, Category = "Inventory")
	bool CanCarryItem(FRPGItemInstance ItemInstance);
	
	/** Adds target item to backpack */
	UFUNCTION(BlueprintNativeEvent, Category = "Inventory")
	void PickUpItem(ARPGLootBase* ItemActor);

	/** Sets target item instance into a cursor */
	UFUNCTION(BlueprintCallable, Category = "Inventory")
	void GrabItemInstance(FRPGItemInstance ItemInstance);

	/** Called when item gets grabbed */
	UFUNCTION(BlueprintNativeEvent, Category = "Inventory")
	void OnGrabItemInstance(FRPGItemInstance ItemInstance);

	/** Drops item instance if there is one being held */
	UFUNCTION(BlueprintCallable, Category = "Inventory")
	void DropItemInstance();

	/** Called when item gets dropped */
	UFUNCTION(BlueprintNativeEvent, Category = "Inventory")
	void OnDropItemInstance(FRPGItemInstance ItemInstance);

	/** Called when grabbed istance is no longer being grabbed, called by OnDropItemInstance, or blueprint directly when droping item in an item container */
	UFUNCTION(BlueprintCallable, Category = "Inventory")
	void ItemInstanceDropped();

	/** Returns true if player is currently grabbing item */
	UFUNCTION(BlueprintCallable, Category = "Inventory")
	bool IsGrabbingItem() const;
#pragma endregion

#pragma region UI Related
	/** Client wrapper of ShowDamageInfo */
	UFUNCTION(Client, Reliable)
	void ClientShowDamageInfo(float DamageAmount, ARPGCharacterBase* Target);

	/** Allows blueprint to show info to the player that he dealt X amount of damage to target */
	UFUNCTION(BlueprintImplementableEvent, Category = "Combat")
	void ShowDamageInfo(float DamageAmount, ARPGCharacterBase* Target);

	/** Client wrapper of StartAbilityCooldown */
	UFUNCTION(Client, Reliable)
	void ClientStartAbilityCooldown(float Cooldown, const FRPGItemSlot& ItemSlot);

	/** Allows blueprint to show cooldown info */
	UFUNCTION(BlueprintImplementableEvent, Category = "Ability")
	void StartAbilityCooldown(float Cooldown, const FRPGItemSlot& ItemSlot);
#pragma endregion

#pragma region Interaction public API
	/** Stops current interaction (if there is any) */
	UFUNCTION(BlueprintCallable, Client, Reliable, Category = "Action", meta = (DisplayName = "Stop Interaction"))
	void ClientStopInteraction();

	/** Called on server when client stops interaction */
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerOnInteractionStopped();

	/** This is run for the actors whose interaction is executed on the client */
	UFUNCTION(Client, Reliable)
	void ClientInteract(AActor* Target);

	UFUNCTION(BlueprintCallable, Category = "Action")
	bool IsInteracting() const;

#pragma endregion	
	/** Same as GetHitResultUnderCursorByChannel but ignores player character controlled by this controller */
	bool GetHitResultUnderCursorByChannelIgnoreSelf(const ECollisionChannel TraceChannel, bool bTraceComplex, FHitResult& HitResult) const;

	FORCEINLINE
	FLinearColor GetNeutralHighlightColor()
	{
		return NeutralHighlightColor;
	}

	FORCEINLINE
	FLinearColor GetHostileHighlightColor()
	{
		return HostileHighlightColor;
	}

	FORCEINLINE
	FLinearColor GetFriendlyHighlightColor()
	{
		return FriendlyHighlightColor;
	}

#pragma region Highlighting to Blueprints
	/** Blueprint uses this to highlight component, uses neutral color */
	UFUNCTION(BlueprintCallable, Category = "Highlightable", meta = (DisplayName = "Set Highlight To Component"))
	void BlueprintSetHighlightToComponent(UPrimitiveComponent* Component, bool bOn);

	UFUNCTION(BlueprintImplementableEvent, Category = "Highlightable")
	void BlueprintSetHighlightOn(UPrimitiveComponent* Target, FLinearColor Color);

	UFUNCTION(BlueprintImplementableEvent, Category = "Highlightable")
	void BlueprintSetHighlightOff(UPrimitiveComponent* Target);
#pragma endregion

protected:
#pragma region Slotted Abilities
	/** Map of slot, from type/num to item */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Slotted Abilities")
	TMap<FRPGItemSlot, FRPGItemAtSlot> SlottedItems;

	/** Binds skills to action buttons */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Slotted Abilities")
	TMap<EPlayerActionButton, FRPGItemSlot> ActionBindings;

	/** Used by UI to bind action binding added event */
	UPROPERTY(BlueprintAssignable, meta = (DisplayName = "ActionBindingAdded"))
	FActionBindingAddedSignature ActionBindingAdded;

	/** Used by UI to bind action binding removed event */
	UPROPERTY(BlueprintAssignable, meta = (DisplayName = "ActionBindingRemoved"))
	FActionBindingRemovedSignature ActionBindingRemoved;

	/** Holds skills that can be assigned to action key */
	UPROPERTY(BlueprintReadOnly, Replicated, Category = "Slotted Abilities")
	TArray<FRPGItemAtSlot> AssignableSkills;

	/** Delegate called when an slotted abilities slot has changed */
	UPROPERTY(BlueprintAssignable, Category = "Slotted Abilities")
	FOnSlottedItemChanged OnSlottedItemChanged;

	/** Called after an item was equipped and we notified all delegates */
	UFUNCTION(BlueprintImplementableEvent, Category = Inventory)
	void SlottedItemChanged(FRPGItemSlot ItemSlot, URPGItem* Item);

	/** Native version above, called before BP delegate */
	FOnSlottedItemChangedNative OnSlottedItemChangedNative;

	/** Delegate called when the slotted abilities has been loaded/reloaded */
	UPROPERTY(BlueprintAssignable, Category = "Slotted Abilitites")
	FOnSlottedAbilitiesLoaded OnSlottedAbilitiesLoaded;

	/** Native version above, called before BP delegate */
	FOnSlottedAbilitiesLoadedNative OnSlottedAbilitiesLoadedNative;
	
	/** Holds reference to the move dummy for that player */
	UPROPERTY(BlueprintReadOnly, Category = "Action")
	ARPGPlayerMoveDummy* MoveDummy;

	/** Holds reference to the move dummy for that player */
	UPROPERTY(BlueprintReadOnly, Category = "Action")
	FVector MoveDummyLocation;

	// Implement IRPGSlottedAbilitiesInterface
	virtual const TMap<FRPGItemSlot, FRPGItemAtSlot>& GetSlottedItemMap() const override
	{
		return SlottedItems;
	}
	virtual FOnSlottedItemChangedNative& GetSlottedItemChangedDelegate() override
	{
		return OnSlottedItemChangedNative;
	}
	virtual FOnSlottedAbilitiesLoadedNative& GetSlottedAbilitiesLoadedDelegate() override
	{
		return OnSlottedAbilitiesLoadedNative;
	}

	void NotifySlottedItemChanged(FRPGItemSlot ItemSlot, URPGItem* Item);
	void NotifySlottedAbilitiesLoaded();

	/** Returns item in slot, or null if empty */
	UFUNCTION(BlueprintPure, Category = "Slotted Abilities")
	bool GetSlottedItem(const FRPGItemSlot& ItemSlot, FRPGItemAtSlot& Result) const;
	
	/** Raised on the client whenever known skills changed so it can refresh UI */
	UPROPERTY(BlueprintAssignable, meta = (DisplayName = "KnownSkillsChanged"))
	FKnownSkillsChangedSignature ReceiveKnownSkillsChanged;

	/** Assigns item slot with action key */
	UFUNCTION(BlueprintCallable, Category = "Slotted Abilities")
	void AddActionBinding(EPlayerActionButton Button, const FRPGItemSlot& ItemSlot);

	/** Removes action binding */
	UFUNCTION(BlueprintCallable, Category = "Slotted Abilities")
	void RemoveActionBinding(EPlayerActionButton Button);

	/** Fills AssignableSkills array */
	UFUNCTION()
	void RebuildAssignableSkills();

	/** Returns true if skill is already bound to an action key */
	UFUNCTION()
	bool IsBoundToActionKey(const FRPGItemSlot& ItemSlot, EPlayerActionButton& Button);
#pragma endregion

#pragma region Item Container
	/** Used by UI to bind item added event */
	UPROPERTY(BlueprintAssignable, meta = (DisplayName = "ItemGrabbed"))
	FItemGrabbedSignature ReceiveItemGrabbed;

	/** Used by UI to bind item added event */
	UPROPERTY(BlueprintAssignable, meta = (DisplayName = "ItemDropped"))
	FItemDroppedSignature ReceiveItemDropped;
#pragma endregion
	
	UPROPERTY(BlueprintReadOnly)
	URPGItemContainerComponent* BackpackItemContainer;

	UPROPERTY(BlueprintReadOnly)
	URPGPlayerActionComponent* ActionComponent;

	/** Color to highlight interactables with neutral hostility ranking  */
	UPROPERTY(EditDefaultsOnly, BlueprintReadonly, Category = "Faction")
	FLinearColor NeutralHighlightColor;

	/** Color to highlight interactables with hostile hostility ranking  */
	UPROPERTY(EditDefaultsOnly, BlueprintReadonly, Category = "Faction")
	FLinearColor HostileHighlightColor;

	/** Color to highlight interactables with friendly hostility ranking  */
	UPROPERTY(EditDefaultsOnly, BlueprintReadonly, Category = "Faction")
	FLinearColor FriendlyHighlightColor;

	/** Class of move dummy  */
	UPROPERTY(EditDefaultsOnly, BlueprintReadonly, Category = "Action")
	TSubclassOf<ARPGPlayerMoveDummy> MoveDummyClass;

private:
	/** Holds reference to instance which is currently being grabbed by player using mouse cursor so it can be positioned in an item container or dropped */
	FRPGItemInstance GrabbedItem;

	/** Cached pointer to actor that is currently turned semi transparent because it overlaps player */
	UPROPERTY()
	TScriptInterface<IRPGDynamicTransparencyInterface> PlayerOverlappingActor;

#pragma region Move to destination / action
	UFUNCTION()
	void SetPrimaryActionOn();
	UFUNCTION()
	void SetSecondaryActionOn();
	UFUNCTION()
	void SetKey1ActionOn();
	UFUNCTION()
	void SetKey2ActionOn();
	UFUNCTION()
	void SetKey3ActionOn();
	UFUNCTION()
	void SetKey4ActionOn();
	UFUNCTION()
	void SetActionOff();

	UFUNCTION()
	void SetLockAttackOn();
	UFUNCTION()
	void SetLockAttackOff();

	UFUNCTION()
	void SetCameraZoom(float Amount);	

	/** Updates move dummy position */
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerUpdateMoveDummyLocation(const FVector& NewLocation);

	/** Turns to target and executes action by given slot */
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerTurnToTargetAndExecuteItemSlot(const FVector& Target, const FRPGItemSlot& ItemSlot);

	/** Executes action on given interactable actor, moves first if necessary */
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerExecuteActionOnActor(AActor* Target, EPlayerActionButton ActionButton, const FRPGItemSlot& ItemSlot);

	/** Executes action on given interactable actor, moves first if necessary, runs the execution on client */
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerExecuteActionOnInteractable(AActor* Target, const FVector& TargetLocation, EPlayerActionButton ActionButton, const FRPGItemSlot& ItemSlot, bool bIsActionTargetSet = false);

	/** Moves to destination determined by move dummy */
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerMoveToDestination();

	/** For combo execution */
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerCheckCombo();

	/** Moves to target and pickes up loot */
	UFUNCTION(BlueprintCallable, Client, Reliable, Category = "Action")
	void ClientPickupLoot(AActor* Loot);

	/** Blueprint wrapper for ServerAddItem */
	UFUNCTION(BlueprintCallable, Category = "Action", meta = (DisplayName = "ClientNotifyReady"))
	void BlueprintClientNotifyReady();

	/** Blueprint wrapper for ServerCheckCombo */
	UFUNCTION(BlueprintCallable, Category = "Action", meta = (DisplayName = "ServerCheckCombo"))
	void BlueprintServerCheckCombo();

	/** Notifies client it is ready for next server request */
	UFUNCTION(Client, Reliable)
	void ClientNotifyReady();

	/** Determines which aciton button is player currently holding */
	UPROPERTY()
	EPlayerActionButton CurrentActionButton;

	/** Tells if lock attack is on */
	UPROPERTY()
	bool bIsLockAttackOn;

	/** If this is true controller will not be able to set new requests to server to invoke new actions. */
	UPROPERTY()
	bool bIsBusy;
	
	/** Stores actor player is currently interacting with */
	AActor* CurrentInteractable;

	UPROPERTY()
	EPlayerActionButton LastActionButton;

	UPROPERTY()
	AActor* LastCursorActor;

	UPROPERTY()
	FVector AttackStartLocation;

#pragma endregion

#pragma region UI Controls
	UFUNCTION(BlueprintCallable, Category = "UI")
	void ToggleInventory();

	UFUNCTION(BlueprintCallable, Category = "UI")
	void ToggleCharacterSheet();

	UFUNCTION(BlueprintCallable, Category = "UI")
	void ToggleSkillSheet();
#pragma endregion
	
#pragma region Highlighting of actors under cursor
	/** Allows blueprint to clear highlight of the given actor if there is any */
	UFUNCTION(BlueprintCallable, Category = "Highlight")
	void ClearHighlightForActor(AActor* ActorToLoseHighlight);

	UFUNCTION()
	void SetHighlightOn(AActor* Tatget);
	
	UFUNCTION()
	void SetHighlightOff();

	AActor* HighlightedActor;
#pragma endregion

	UFUNCTION()
	void ProcessDynamicTransparencyQuery();
	
};
