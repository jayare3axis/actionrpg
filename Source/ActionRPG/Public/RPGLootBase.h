// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "GameFramework/Actor.h"
#include "RPGInteractableInterface.h"
#include "RPGHighlightableInterface.h"
#include "Items/RPGItem.h"
#include "Items/RPGItemTypes.h"
#include "RPGLootBase.generated.h"

/**
* Base class of all actors that can be picked up as a loot
*/
UCLASS()
class ACTIONRPG_API ARPGLootBase : public AActor, public IRPGInteractableInterface, public IRPGHighlightableInterface
{
	GENERATED_BODY()
	
public:	
	// Constructor
	ARPGLootBase();

	// IInteractable
	virtual void InteractClient(ARPGPlayerControllerBase* PC) override;
	virtual void StopInteraction(ARPGPlayerControllerBase* PC) override;
	virtual bool CanBeInteract() override;
	// IHighlightable
	virtual void Highlighted(bool Value) override;
	virtual bool CanBeHighlighted() override;

	FORCEINLINE
	FRPGItemInstance GetItemInstance() const
	{
		return ItemInstance;
	}

	UFUNCTION(BlueprintImplementableEvent, Category = "Highlightable", meta = (DisplayName = "Highlighted"))
	void BlueprintHighlighted(bool Value);

	/** Called by blueprint to notify that item can be picked up by a player */
	UFUNCTION(BlueprintCallable, Category = "Loot")
	void OnItemDropped(FRPGItemInstance ItemToPickUp, float DropHeight);

	/** Allows blueprint to animate an item before it can be picked up by a player */
	UFUNCTION(BlueprintNativeEvent, Category = "Loot")
	void LootSpawnAnimation(FRPGItemInstance ItemToPickUp, float DropHeight);

	/** Allows blueprint to animate or notify the player that he can't pick up the item (no space in the inventory left) */
	UFUNCTION(BlueprintNativeEvent, Category = "Loot")
	void CannotPickUpAnimation();
	
	/** Shows helper widget which picks the item up on interaction */
	UFUNCTION(BlueprintImplementableEvent, Category = "Loot")
	void ShowPickUpWidget(FRPGItemInstance ItemToPickUp);

	/** Hides helper widget which picks the item up on interaction */
	UFUNCTION(BlueprintImplementableEvent, Category = "Loot")
	void HidePickUpWidget();

protected:
	/** Holds item to be picked up on interaction */
	UPROPERTY(BlueprintReadonly, Category = "Loot")
	FRPGItemInstance ItemInstance;

private:
	/** Determines if item can be picked up by a player */
	UPROPERTY()
	bool bIsLootEnabled;

};
