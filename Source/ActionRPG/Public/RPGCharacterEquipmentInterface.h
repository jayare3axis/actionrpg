// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "RPGCharacterEquipmentComponent.h"
#include "RPGCharacterEquipmentInterface.generated.h"

/**
* Interface used to access Character Equipment Component
*/
UINTERFACE(Blueprintable)
class URPGCharacterEquipmentInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class ACTIONRPG_API IRPGCharacterEquipmentInterface
{
	GENERATED_BODY()

public:
	/** Returns the character equipment component to use for this actor. */
	virtual URPGCharacterEquipmentComponent* GetCharacterEquipmentComponent() = 0;
};
