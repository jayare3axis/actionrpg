// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "Engine/StaticMeshActor.h"
#include "RPGDynamicTransparencyInterface.h"
#include "RPGDynamicTransparencyStaticMesh.generated.h"

/**
 * Static mesh actor that turns semi transparent as it overlaps player
 */
UCLASS()
class ACTIONRPG_API ARPGDynamicTransparencyStaticMesh : public AStaticMeshActor, public IRPGDynamicTransparencyInterface
{
	GENERATED_BODY()
	
public:
	// IRPGDynamicTransparencyInterface
	virtual void MakeSemiTransparent(bool val) override;

	UFUNCTION(BlueprintImplementableEvent, Category = "DynamicTransparency", meta = (DisplayName = "MakeSemiTransparent"))
	void BlueprintMakeSemiTransparent(bool val);

};
