// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "RPGTypes.generated.h"

#define ECC_Interactable ECC_GameTraceChannel1

#define TAG_Highlight "Highlight"

#define GT_MoveAbility "Ability.Movement.Move"
#define GT_TurnAbility "Ability.Movement.Turn"
#define GT_MeleeAbility "Ability.Melee"
#define GT_SkillAbility "Ability.Skill"
#define GT_GainExperience "Level.GainExperience"
#define GT_DamageTypeSlashing "DamageType.Physical.Slashing"
#define GT_DamageTypePiercing "DamageType.Physical.Piercing"
#define GT_DamageTypeCrushing "DamageType.Physical.Crushing"

/** Destination where to move our player character */
USTRUCT(Blueprintable)
struct ACTIONRPG_API FMoveTarget
{
	GENERATED_USTRUCT_BODY()

	FMoveTarget() : Location(FVector::ZeroVector), Actor(NULL), AcceptanceRadius(-1) {}
	FMoveTarget(FVector _Location, AActor* _Actor, float _AcceptanceRadius) : Location(_Location), Actor(_Actor), AcceptanceRadius(_AcceptanceRadius) {}

	UPROPERTY(BlueprintReadonly, Category = "Movement")
	FVector Location;

	UPROPERTY(BlueprintReadonly, Category = "Movement")
	AActor* Actor;

	UPROPERTY(BlueprintReadonly, Category = "Movement")
	float AcceptanceRadius;
};

/** Target of an action of our player character, action is meant to be interaction or attack */
USTRUCT(Blueprintable)
struct ACTIONRPG_API FActionTarget
{
	GENERATED_USTRUCT_BODY()

	FActionTarget() : Location(FVector::ZeroVector), Actor(NULL) {}
	FActionTarget(FVector _Location, AActor* _Actor) : Location(_Location), Actor(_Actor) {}

	UPROPERTY(BlueprintReadonly, Category = "Movement")
	FVector Location;

	UPROPERTY(BlueprintReadonly, Category = "Movement")
	AActor* Actor;

};

/** Player action button */
UENUM(BlueprintType)
enum class EPlayerActionButton : uint8
{
	ABE_None 			UMETA(DisplayName = "None"),
	ABE_Primary 		UMETA(DisplayName = "Primary"),
	ABE_Secondary 		UMETA(DisplayName = "Secondary"),
	ABE_Key1			UMETA(DisplayName = "Key1"),
	ABE_Key2			UMETA(DisplayName = "Key2"),
	ABE_Key3			UMETA(DisplayName = "Key3"),
	ABE_Key4			UMETA(DisplayName = "Key4")
};

/** Hostility ranking */
UENUM(BlueprintType)
enum EHostilityRanking  
{
	HRE_Neutral 		UMETA(DisplayName = "Neutral"),
	HRE_Enemy	 		UMETA(DisplayName = "Enemy"),
	HRE_Friendly		UMETA(DisplayName = "Friendly")
};

/** Item's descripion field type */
UENUM(BlueprintType)
enum EItemDescriptionsFieldType
{
	IDE_Damage	 		UMETA(DisplayName = "Damage"),
	IDE_Modifier 		UMETA(DisplayName = "Modifier")
};

/** Attribute name */
UENUM(BlueprintType)
enum EAttributeName
{
	ANE_Level	 						UMETA(DisplayName = "Level"),
	ANE_AvailableSkillPoints			UMETA(DisplayName = "Available Skill Points"),
	ANE_Experience						UMETA(DisplayName = "Experience"),
	ANE_Health							UMETA(DisplayName = "Health"),
	ANE_MaxHealth						UMETA(DisplayName = "MaxHealth"),
	ANE_Mana							UMETA(DisplayName = "Mana"),
	ANE_MaxMana							UMETA(DisplayName = "MaxMana"),
	ANE_MoveSpeed						UMETA(DisplayName = "MoveSpeed"),
	ANE_Strength						UMETA(DisplayName = "Strength"),
	ANE_Dexterity						UMETA(DisplayName = "Dexterity"),
	ANE_Endurance						UMETA(DisplayName = "Endurance"),
	ANE_Intelligence					UMETA(DisplayName = "Intelligence"),
	ANE_AttackRatingBonus				UMETA(DisplayName = "AttackRatingBonus"),
	ANE_DefenceRatingBonus				UMETA(DisplayName = "DefenceRatingBonus")
};

/** Holds basic information about character */
USTRUCT(BlueprintType)
struct ACTIONRPG_API FRPGCharacterInfo
{
	GENERATED_BODY()

	/** Constructor */
	FRPGCharacterInfo() : CharacterName(""), Class(FName()), Level(1), Id(FName())
	{}

	FRPGCharacterInfo(FString InCharacterName, FName InClass, int32 InLevel, FName InId) : CharacterName(InCharacterName), Class(InClass), Level(InLevel), Id(InId)
	{}

	/** The type of items that can go in this slot */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Data)
	FString CharacterName;

	/** Holds name of character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Data)
	FName Class;

	/** Character level */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Data)
	int32 Level;

	/** Character identifier */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Data)
	FName Id;
};

/** Player apperance options */
USTRUCT(BlueprintType)
struct ACTIONRPG_API FRPGPlayerAppearanceOptions
{
	GENERATED_BODY()

	/** Constructor */
	FRPGPlayerAppearanceOptions() : Key(""), DisplayName(), OptionsNum(0)
	{}
	FRPGPlayerAppearanceOptions(FName InKey, FText InDisplayName, int32 InOptionsNum) : Key(InKey), DisplayName(InDisplayName), OptionsNum(InOptionsNum)
	{}

	/** Apperance option name */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Data)
	FName Key;

	/** Display name */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Data)
	FText DisplayName;

	/** Options num */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Data)
	int32 OptionsNum;
};

/** Named appearance option setting */
USTRUCT(BlueprintType)
struct ACTIONRPG_API FRPGNamedAppearanceOption
{
	GENERATED_BODY()

	/** Constructor */
	FRPGNamedAppearanceOption() : Key(""), Value(0)
	{}
	FRPGNamedAppearanceOption(FName InKey, int32 InValue) : Key(InKey), Value(InValue)
	{}

	/** Apperance option name */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Data)
	FName Key;

	/** Appearance option value */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Data)
	int32 Value;
};

/** Item's description field, can be damage, armor etc. */
USTRUCT(BlueprintType)
struct ACTIONRPG_API FRPGItemsDescriptionField
{
	GENERATED_BODY()

	/** Constructor */
	FRPGItemsDescriptionField() : ValueA(0.f), ValueB(0.f), ValueText(), FieldType(EItemDescriptionsFieldType::IDE_Damage)
	{}
	FRPGItemsDescriptionField(float _ValueA, float _ValueB, FText _ValueText, EItemDescriptionsFieldType _FieldType) : ValueA(_ValueA), ValueB(_ValueB), ValueText(_ValueText), FieldType(_FieldType)
	{}

	/** First value, for example min damage, or armor value */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Item")
	float ValueA;

	/** Second value, for example max damage */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Item")
	float ValueB;

	/** Some text value, for example description */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Item")
	FText ValueText;
	
	/** Defines what field actually is */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Item")
	TEnumAsByte<EItemDescriptionsFieldType> FieldType;
};

/** Item meant to be displayed in a List UI */
USTRUCT(BlueprintType)
struct ACTIONRPG_API FRPGOrderedListItem
{
	GENERATED_BODY()

	/** Constructor */
	FRPGOrderedListItem() : Key(""), DisplayName(), Order(0)
	{}
	FRPGOrderedListItem(FName _Key, FText _DisplayName, int32 _Order) : Key(_Key), DisplayName(_DisplayName), Order(_Order)
	{}

	/** Key */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Data)
	FName Key;

	/** Display Name */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Data)
	FText DisplayName;

	int32 Order;

	bool operator==(const FRPGOrderedListItem& Other) const
	{
		return Key == Other.Key;
	}
	bool operator!=(const FRPGOrderedListItem& Other) const
	{
		return !(*this == Other);
	}
	bool operator<(const FRPGOrderedListItem& Other) const
	{
		return Order < Other.Order;
	}
};

/** Rarity ranking */
UENUM(BlueprintType)
enum ERarityRanking
{
	RRE_Common	 		UMETA(DisplayName = "Common"),
	RRE_Uncommon 		UMETA(DisplayName = "Uncommon"),
	RRE_Rare			UMETA(DisplayName = "Rare"),
	RRE_Epic			UMETA(DisplayName = "Epic"),
	RRE_Legendary		UMETA(DisplayName = "Legendary")
};

/** Update property */
UENUM(BlueprintType)
enum EUpdateProperty
{
	UPE_Level	 		UMETA(DisplayName = "Level"),
	UPE_Experience 		UMETA(DisplayName = "Experience"),
	UPE_SkillPoints		UMETA(DisplayName = "SkillPoints"),
	UPE_Health			UMETA(DisplayName = "Health"),
	UPE_Mana			UMETA(DisplayName = "Mana")
};

/** Initalization phaze */
UENUM(BlueprintType)
enum EInitializationPhase
{
	IPE_Pre	 			UMETA(DisplayName = "Pre"),
	IPE_Character 		UMETA(DisplayName = "Character"),
	IPE_UI				UMETA(DisplayName = "UI"),
	IPE_Done			UMETA(DisplayName = "Done")
};
