// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "Components/ActorComponent.h"
#include "Items/RPGItemTypes.h"
#include "Abilities/GameplayAbility.h"
#include "RPGPlayerActionComponent.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ACTIONRPG_API URPGPlayerActionComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	URPGPlayerActionComponent();

	/**
	 * Turns player towards target and when move finishes activates ability by item slot
	*/
	void TurnToTargetAndExecuteItemSlot(const FVector& Target, const FRPGItemSlot& ItemSlot);

	/**
	 * Moves player towards target and when move finishes activates ability by item slot
	*/
	void MoveToTargetAndExecuteItemSlot(AActor* Target, const FRPGItemSlot& ItemSlot);
	
	/**
	 * Moves player towards target and when move finishes runs interaction, if object exists only for client it has action target already set here
	*/
	void MoveToTargetAndInteract(AActor* Target, const FVector& TargetLocation, bool bIsActionTargetSet = false);

	/**
	 * Moves player towards target actor
	*/
	void MoveToActor(AActor* Target, bool bUnbind = true);

	/**
	 * Moves player towards target point
	*/
	void MoveToPoint(const FVector& Target, bool bUnbind = true);

	/**
	* Called on destruction
	*/
	void CleanUp();

	/**
	* Minimal distance needed to start interaction without making player to move
	*/
	float MinInteractionDistance;
			
private:
	UFUNCTION()
	void OnTurnEnd();

	UFUNCTION()
	void OnAbilityEnd(const FAbilityEndedData& Event);

	UFUNCTION()
	void OnInteract();

	UFUNCTION()
	void UnbindTurnEndedIfBound();

	UFUNCTION()
	void UnbindAbilityEndedIfBound();

	UFUNCTION()
	void CancelPreviousMovement();

	FDelegateHandle TurnEndHandle;
	FDelegateHandle AbilityEndedHandle;

	bool bIsInteraction;
	FRPGItemSlot ItemSlotToUse;

};
