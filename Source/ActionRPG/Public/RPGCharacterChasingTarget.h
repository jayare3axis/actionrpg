// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "GameFramework/Actor.h"
#include "RPGCharacterChasingTarget.generated.h"

// Forward
class ARPGCharacterBase;

/**
 * This is a dummy actor which is attached to a Player Character for enemies to chase. Enemies are chasing multiple targets on Player rather
 * than Player itself so they don't stack on each other but rather try to surround the Player.
*/
UCLASS()
class ACTIONRPG_API ARPGCharacterChasingTarget : public AActor
{
	GENERATED_BODY()
	
public:	
	// Constructor
	ARPGCharacterChasingTarget();

	/** Assigns aggro to this chasing target. */
	UFUNCTION()
	void AssignAggro(ARPGCharacterBase* Aggro);

	/** Tells how many aggros are assigned to this chasing target. */
	UFUNCTION()
	int32 GetAggroCount() const;

	/** Resets assigned aggros to none. */
	UFUNCTION()
	void ResetAggros();

private:
	UPROPERTY()
	TSet<ARPGCharacterBase*> Aggros;

};
