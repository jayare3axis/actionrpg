// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "GameFramework/Actor.h"
#include "RPGEnemyDataAsset.h"
#include "RPGSpawnEnemy.generated.h"

/**
* Spawns an enemy in a world.
*/
UCLASS()
class ACTIONRPG_API ARPGSpawnEnemy : public AActor
{
	GENERATED_BODY()
	
public:	
	// Constructors and overrides
	ARPGSpawnEnemy();
	virtual void BeginPlay() override;

	UFUNCTION()
	void SpawnEnemy(URPGEnemyDataAsset* Data);

protected:
	UFUNCTION(BlueprintNativeEvent, Category = "Enemy")
	void SpawnEnemyAtMyLocation();

	/** Enemy to spawn */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Enemy")
	URPGEnemyDataAsset* Data;

	UPROPERTY(BlueprintReadOnly, Category = "Enemy")
	ARPGCharacterBase* SpawnedCharacter;

};
