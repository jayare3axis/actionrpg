// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "Subsystems/LocalPlayerSubsystem.h"
#include "RPGPlayerManagerSubsystem.generated.h"

// Forward
class ARPGCharacterBase;

/**
 * Keeps track of currently present player characters.
 */
UCLASS()
class ACTIONRPG_API URPGPlayerManagerSubsystem : public ULocalPlayerSubsystem
{
	GENERATED_BODY()

public:
	/** Called by blueprint to notify player character is spawned */
	UFUNCTION(BlueprintCallable, Category = "Player")
	void OnPlayerCharacterCreated(ARPGCharacterBase* Character);

	/** Called by blueprint to notify player character is destroyed */
	UFUNCTION(BlueprintCallable, Category = "Player")
	void OnPlayerCharacterDestroyed(ARPGCharacterBase* Character);

	FORCEINLINE
	const TSet<ARPGCharacterBase*>& GetPlayerCharacters() { return PlayerCharacters; }

private:
	UPROPERTY()
	TSet<ARPGCharacterBase*> PlayerCharacters;

};
