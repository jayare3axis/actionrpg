// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "GameFramework/PlayerState.h"
#include "RPGPlayerStateBase.generated.h"

/**
 * Base class of player state, should be blueprinted
 */
UCLASS()
class ACTIONRPG_API ARPGPlayerStateBase : public APlayerState
{
	GENERATED_BODY()

public:
	// Constructor
	ARPGPlayerStateBase();
	
};
