// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

// Forward
class ARPGCharacterBase;
class URPGSaveGame;

#include "ActionRPG.h"
#include "GameFramework/Pawn.h"
#include "RPGPlayerProxyBase.generated.h"

/**
* This is base of a default Pawn object controlled by the RPGPlayerControllerBase, should by blueprinted
*/
UCLASS()
class ACTIONRPG_API ARPGPlayerProxyBase : public APawn
{
	GENERATED_BODY()

public:
	// Contructor and overrides
	ARPGPlayerProxyBase();
	virtual void BeginPlay() override;
	virtual void PossessedBy(AController* NewController) override;

	/** Character class used to spawn when there is no save game data present - game is created directly from UE editor */
	UPROPERTY(EditDefaultsOnly, Category = "Character")
	TSubclassOf<ARPGCharacterBase> DebugPlayerCharacterClass;

	UPROPERTY(BlueprintReadOnly, ReplicatedUsing = OnRep_PlayerCharacter, Category = "Character")
	ARPGCharacterBase* PlayerCharacter;

	/** Sets view target to player character belonging to the proxy, runs only if proxy is attached to controller */
	void CheckSetCameraViewTarget();

private:
	/** Spawns debug player character using the DebugPlayerCharacterClass */
	void SpawnDebugPlayerCharacter();

	/** Initializes slotted abilities source for this player */
	void InitSlottedAbilitiesSource();

	UFUNCTION()
	void OnRep_PlayerCharacter();
};
