// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "Items/RPGItem.h"
#include "RPGTypes.h"
#include "RPGInventoryItem.generated.h"

/**
 *  Native base class for items that can be picked up
 */
UCLASS()
class ACTIONRPG_API URPGInventoryItem : public URPGItem
{
	GENERATED_BODY()
		
public:
	/** Constructor */
	URPGInventoryItem() : URPGItem(), XSize(1), YSize(1), MaxCount(1) {}

	/** Item's rarity ranking */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "General")
	TEnumAsByte<ERarityRanking> RarityRanking;

	/** Actor to spawn as a loot */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Loot")
	TSubclassOf<AActor> LootActorClass;

	/** How much horizontal tiles item ocupies in the inventory */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Loot")
	int32 XSize;

	/** How much vertical tiles item ocupies in the inventory */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Loot")
	int32 YSize;

	/** Maximum number of instances that can be in inventory at once, <= 0 means infinite */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Max")
	int32 MaxCount;

	/** Sound played when item is grabbed */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Loot")
	USoundBase* GrabSound;

	/** Sound played when item is dropped */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Loot")
	USoundBase* DropSound;
	
};
