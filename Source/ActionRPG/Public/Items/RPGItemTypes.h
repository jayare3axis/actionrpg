// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "RPGItemTypes.generated.h"

/** Struct representing a slot for an item, shown in the UI */
USTRUCT(BlueprintType)
struct ACTIONRPG_API FRPGItemSlot
{
	GENERATED_BODY()

	/** Constructor, -1 means an invalid slot */
	FRPGItemSlot() : SlotNumber(-1)
	{}

	FRPGItemSlot(const FPrimaryAssetType& InItemType, int32 InSlotNumber) : ItemType(InItemType), SlotNumber(InSlotNumber)
	{}

	/** The type of items that can go in this slot */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Item)
	FPrimaryAssetType ItemType;

	/** The number of this slot, 0 indexed */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Item)
	int32 SlotNumber;

	/** Equality operators */
	bool operator==(const FRPGItemSlot& Other) const
	{
		return ItemType == Other.ItemType && SlotNumber == Other.SlotNumber;
	}
	bool operator!=(const FRPGItemSlot& Other) const
	{
		return !(*this == Other);
	}

	/** Implemented so it can be used in Maps/Sets */
	friend inline uint32 GetTypeHash(const FRPGItemSlot& Key)
	{
		uint32 Hash = 0;

		Hash = HashCombine(Hash, GetTypeHash(Key.ItemType));
		Hash = HashCombine(Hash, (uint32)Key.SlotNumber);
		return Hash;
	}

	/** Returns true if slot is valid */
	bool IsValid() const
	{
		return ItemType.IsValid() && SlotNumber >= 0;
	}
};

/** Holds Item Slot with associated Item, used to send this information via network instead of using TMap it doesn't support replication */
USTRUCT(BlueprintType)
struct ACTIONRPG_API FRPGItemAtSlotRaw
{
	GENERATED_USTRUCT_BODY()

	FRPGItemAtSlotRaw() : ItemSlot(FRPGItemSlot()), Item(FPrimaryAssetId()), Level(1) {}
	FRPGItemAtSlotRaw(FRPGItemSlot _ItemSlot, FPrimaryAssetId _Item, int32 _Level) : ItemSlot(_ItemSlot), Item(_Item), Level(_Level) {}

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Item)
	FRPGItemSlot ItemSlot;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Item)
	FPrimaryAssetId Item;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Item)
	int32 Level;
};

/** Instance of an item with level and count */
USTRUCT(BlueprintType)
struct ACTIONRPG_API FRPGItemInstance
{
	GENERATED_USTRUCT_BODY()

	FRPGItemInstance() : Item(NULL), Count(1), Level(1), Id(-1) {}
	FRPGItemInstance(URPGItem* _Item, int32 _Count, int32 _Level, int32 _Id) : Item(_Item), Count(_Count), Level(_Level), Id(_Id) {}
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Item)
	URPGItem* Item;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Item)
	int32 Count;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Item)
	int32 Level;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Item)
	int32 Id;

	bool operator==(const FRPGItemInstance& Other) const
	{
		return Id == Other.Id;
	}
	bool operator!=(const FRPGItemInstance& Other) const
	{
		return !(*this == Other);
	}
};

/** Defines position of an item within an item coordinate */
USTRUCT(Blueprintable)
struct ACTIONRPG_API FItemContainerPosition
{
	GENERATED_USTRUCT_BODY()

	FItemContainerPosition() : X(0), Y(0) {}
	FItemContainerPosition(int32 _X, int32 _Y) : X(_X), Y(_Y) {}

	UPROPERTY(BlueprintReadWrite, Category = "Inventory")
	int32 X;

	UPROPERTY(BlueprintReadWrite, Category = "Inventory")
	int32 Y;

	bool operator==(const FItemContainerPosition& Other) const
	{
		return X == Other.X && Y == Other.Y;
	}
	bool operator!=(const FItemContainerPosition& Other) const
	{
		return !(*this == Other);
	}

	/** Implemented so it can be used in Maps/Sets */
	friend inline uint32 GetTypeHash(const FItemContainerPosition& Key)
	{
		uint32 Hash = 0;

		Hash = HashCombine(Hash, (int32)Key.X);
		Hash = HashCombine(Hash, (int32)Key.Y);
		return Hash;
	}
};

/** Item at container position */
USTRUCT(BlueprintType)
struct ACTIONRPG_API FRPGItemInContainerRaw
{
	GENERATED_USTRUCT_BODY()

	FRPGItemInContainerRaw() : Position(FItemContainerPosition()), Item(FPrimaryAssetId()), Count(1), Level(1) {}
	FRPGItemInContainerRaw(FItemContainerPosition _Position, FPrimaryAssetId _Item, int32 _Count, int32 _Level) : Position(_Position), Item(_Item), Count(_Count), Level(_Level) {}

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Item)
	FItemContainerPosition Position;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Item)
	FPrimaryAssetId Item;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Item)
	int32 Count;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Item)
	int32 Level;

	bool operator==(const FRPGItemInContainerRaw& Other) const
	{
		return Position == Other.Position;
	}
	bool operator!=(const FRPGItemInContainerRaw& Other) const
	{
		return !(*this == Other);
	}
};

/** Delegate called when the contents of an slotted abilities slot change */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnSlottedItemChanged, FRPGItemSlot, ItemSlot, URPGItem*, Item);
DECLARE_MULTICAST_DELEGATE_TwoParams(FOnSlottedItemChangedNative, FRPGItemSlot, URPGItem*);

/** Delegate called when the entire slotted abilities has been loaded, all items may have been replaced */
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnSlottedAbilitiesLoaded);
DECLARE_MULTICAST_DELEGATE(FOnSlottedAbilitiesLoadedNative);
