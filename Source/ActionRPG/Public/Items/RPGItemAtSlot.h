// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "RPGItemTypes.h"
#include "Items/RPGItem.h"
#include "RPGItemAtSlot.generated.h"

/** Struct representing item at given slot */
USTRUCT(BlueprintType)
struct ACTIONRPG_API FRPGItemAtSlot
{
	GENERATED_BODY()

	/** Constructor */
	FRPGItemAtSlot() : Level(1) {}
	/** Constructor */
	FRPGItemAtSlot(FRPGItemSlot _ItemSlot, URPGItem* _Item, int32 _Level) : Slot(_ItemSlot), Item(_Item), Level(_Level) {}

	/** Slot that item is in */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Item)
	FRPGItemSlot Slot;

	/** The item that is in that slot */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Item)
	URPGItem* Item;

	/** The item that is in that slot */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Item)
	int32 Level;

	/** Equality operators */
	bool operator==(const FRPGItemAtSlot& Other) const
	{
		return Slot == Other.Slot && Item == Other.Item;
	}
	bool operator!=(const FRPGItemAtSlot& Other) const
	{
		return !(*this == Other);
	}

};
