// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "Items/RPGEquipmentItem.h"
#include "RPGWeaponItem.generated.h"

/** Native base class for weapons, should be blueprinted */
UCLASS()
class ACTIONRPG_API URPGWeaponItem : public URPGEquipmentItem
{
	GENERATED_BODY()

public:
	/** Constructor and overrides */
	URPGWeaponItem()
	{
		ItemType = URPGAssetManager::WeaponItemType;
	}

};
