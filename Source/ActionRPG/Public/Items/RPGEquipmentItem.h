// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "Items/RPGInventoryItem.h"
#include "RPGPartAssetBase.h"
#include "RPGEquipmentItem.generated.h"

/** Native base class for items that can be equiped, should be blueprinted */
UCLASS()
class ACTIONRPG_API URPGEquipmentItem : public URPGInventoryItem
{
	GENERATED_BODY()

public:
	/** Part applied to character when item is equiped */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Character")
	URPGPartAssetBase* PartAsset;

	/** Identifies slot where this item belongs as an equipment */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "General")
	int32 SlotNumber;

};
