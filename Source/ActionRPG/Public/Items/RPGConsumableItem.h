// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "Items/RPGInventoryItem.h"
#include "RPGConsumableItem.generated.h"

/** Native base class for consumables, should be blueprinted */
UCLASS()
class ACTIONRPG_API URPGConsumableItem : public URPGInventoryItem
{
	GENERATED_BODY()

public:
	/** Constructor */
	URPGConsumableItem()
	{
		ItemType = URPGAssetManager::ConsumableItemType;
	}

};
