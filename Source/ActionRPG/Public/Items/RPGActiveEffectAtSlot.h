// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "RPGItemTypes.h"
#include "GameplayEffectTypes.h"
#include "RPGActiveEffectAtSlot.generated.h"

/** Struct representing active effect at given slot */
USTRUCT(BlueprintType)
struct ACTIONRPG_API FRPGActiveEffectAtSlot
{
	GENERATED_BODY()

	/** Constructor */
	FRPGActiveEffectAtSlot() {}
	/** Constructor */
	FRPGActiveEffectAtSlot(FRPGItemSlot _ItemSlot, FActiveGameplayEffectHandle _Effect) : Slot(_ItemSlot), Effect(_Effect) {}

	/** Slot that item is in */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Abilities)
	FRPGItemSlot Slot;

	/** The item that is in that slot */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Abilities)
	FActiveGameplayEffectHandle Effect;


	/** Equality operators */
	bool operator==(const FRPGActiveEffectAtSlot& Other) const
	{
		return Slot == Other.Slot && Effect.IsValid() && Effect == Other.Effect;
	}
	bool operator!=(const FRPGActiveEffectAtSlot& Other) const
	{
		return !(*this == Other);
	}

};
