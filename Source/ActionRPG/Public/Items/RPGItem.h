// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "Engine/DataAsset.h"
#include "Styling/SlateBrush.h"
#include "RPGAssetManager.h"
#include "Data/RPGItemSubTypeDataAsset.h"
#include "Abilities/GameplayAbility.h"
#include "RPGGameplayEffect.h"
#include "RPGItem.generated.h"

// Forward
class URPGGameplayAbility;

/** Base class for all items, do not blueprint directly */
UCLASS(Abstract, BlueprintType)
class ACTIONRPG_API URPGItem : public UPrimaryDataAsset
{
	GENERATED_BODY()

public:
	/** Constructor */
	URPGItem() : Price(0), MaxLevel(1)
	{}

	/** Type of this item, set in native parent class */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "General")
	FPrimaryAssetType ItemType;

	/**Can be used to categorize items, for example weapon types */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "General")
	URPGItemSubTypeDataAsset* ItemSubType;

	/** User-visible short name */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "General")
	FText ItemName;

	/** User-visible long description */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "General")
	FText ItemDescription;

	/** Icon to display */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "General")
	UTexture2D* ItemTexture;

	/** Price in game */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "General")
	int32 Price;

	/** Max level of an item */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Abilities")
	int32 MaxLevel;

	/** Ability to grant if this item is slotted */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Abilities")
	TSubclassOf<URPGGameplayAbility> GrantedAbility;

	/** Passive gameplay effects applied on creation */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Abilities")
	TArray<FEffectAndLevel> GrantedGameplayEffects;

	/** Returns the logical name, equivalent to the primary asset id */
	UFUNCTION(BlueprintCallable, Category = "Item")
	FString GetIdentifierString() const;

	/** Overridden to use saved type */
	virtual FPrimaryAssetId GetPrimaryAssetId() const override;

};
