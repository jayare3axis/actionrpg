// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "Items/RPGEquipmentItem.h"
#include "RPGArmorItem.generated.h"

/** Native base class for armor, should be blueprinted */
UCLASS()
class ACTIONRPG_API URPGArmorItem : public URPGEquipmentItem
{
	GENERATED_BODY()

public:
	/** Constructor and overrides */
	URPGArmorItem()
	{
		ItemType = URPGAssetManager::ArmorItemType;
	}

};
