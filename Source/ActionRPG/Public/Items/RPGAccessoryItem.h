// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "Items/RPGEquipmentItem.h"
#include "RPGAccessoryItem.generated.h"

/** Native base class for accessories, should be blueprinted */
UCLASS()
class ACTIONRPG_API URPGAccessoryItem : public URPGEquipmentItem
{
	GENERATED_BODY()

public:
	/** Constructor and overrides */
	URPGAccessoryItem()
	{
		ItemType = URPGAssetManager::AccessoryItemType;
	}

};
