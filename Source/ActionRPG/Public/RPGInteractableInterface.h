// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

// Forward
class ARPGPlayerControllerBase;

#include "ActionRPG.h"
#include "UObject/Interface.h"
#include "RPGInteractableInterface.generated.h"

UINTERFACE(MinimalAPI)
class URPGInteractableInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * Interface of all actors that react to player interacting with them
 */
class ACTIONRPG_API IRPGInteractableInterface
{
	GENERATED_BODY()

public:
	/**
	* Called when player interacts so it cun run some logic related to this actor
	*/
	UFUNCTION()
	virtual void Interact(ARPGPlayerControllerBase* PC);

	/**
	* Same as Interact, except it runs on client if interaction should run logic for both client and server
	*/
	UFUNCTION()
	virtual void InteractClient(ARPGPlayerControllerBase* PC);

	/** 
	* Called when players stops interacting with this actor, don't call this directly, instead use StopInteraction method on player controller
	*/
	UFUNCTION()
	virtual void StopInteraction(ARPGPlayerControllerBase* PC);

	/**
	* Same as StopInteraction, except it runs on client if interaction should run logic for both client and server
	*/
	UFUNCTION()
	virtual void StopInteractionClient(ARPGPlayerControllerBase* PC);

	/**
	* Return true if object can be interact
	*/
	UFUNCTION()
	virtual bool CanBeInteract();

	/**
	* Return true if interaction should cause player to attack, useful for example for destructible objects
	*/
	UFUNCTION()
	virtual bool ShouldAttackOnInteraction(ARPGPlayerControllerBase* PC);

};
