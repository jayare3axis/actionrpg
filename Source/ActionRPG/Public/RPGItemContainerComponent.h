// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "RPGItemTypes.h"
#include "RPGItemContainerComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FItemAddedSignature, FItemContainerPosition, Position, FRPGItemInstance, ItemInstance);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FItemRemovedSignature, FRPGItemInstance, ItemInstance);

/**
* Holds up itens to be managed by player
*/
UCLASS(Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class ACTIONRPG_API URPGItemContainerComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Constructor
	URPGItemContainerComponent();

	/** Sets size of item conteiner */
	UFUNCTION()
	void SetSize(int32 XSize, int32 YSize);

	/** Attempts to find free slot where can be added target item */
	UFUNCTION(BlueprintCallable, Category = "Inventory")
	bool FindPositionForItem(FRPGItemInstance ItemInstance, FItemContainerPosition& Position);

	/** Attempts to add item at first free spot */
	UFUNCTION(BlueprintCallable, Category = "Inventory")
	bool AddItem(FRPGItemInstance ItemInstance);

	/** Adds item to given position, doesn't perform any check */
	UFUNCTION(BlueprintCallable, Category = "Inventory")
	void AddItemAtPosition(const FItemContainerPosition& Position, FRPGItemInstance ItemInstance);

	/** Removes item instance from container, if it's present */
	UFUNCTION(BlueprintCallable, Category = "Inventory")
	bool RemoveItem(FRPGItemInstance ItemInstance);

	/** Gets all unique items present in this container */
	UFUNCTION(BlueprintPure, Category = "Inventory")
	void GetItems(TMap<FItemContainerPosition, FRPGItemInstance>& Items);

	/** Returns true if target item can be placed at specified position */
	UFUNCTION(BlueprintCallable, Category = "Inventory")
	bool CanPlaceItemAtPosition(const FItemContainerPosition& Position, FRPGItemInstance ItemInstance);

	/** Gets all items present at tiles */
	UFUNCTION(BlueprintCallable, Category = "Inventory")
	void GetItemsByTiles(const FItemContainerPosition& Position, int32 SizeX, int32 SizeY, TArray<FRPGItemInstance>& Items);

	/** Initializes container from raw data */
	UFUNCTION(BlueprintCallable, Category = "Inventory")
	void LoadFromData(const TArray<FRPGItemInContainerRaw>& Items);

protected:
	/** Used by UI to bind item added event */
	UPROPERTY(BlueprintAssignable, meta = (DisplayName = "ItemAdded"))
	FItemAddedSignature ReceiveItemAdded;

	/** Used by UI to bind item removed event */
	UPROPERTY(BlueprintAssignable, meta = (DisplayName = "ItemRmoved"))
	FItemRemovedSignature ReceiveItemRemoved;

	/** Horizontal size of item container */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Inventory")
	int32 XSize;

	/** Vertical size of item container */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Inventory")
	int32 YSize;

	/** Holds all items presented in a container */
	UPROPERTY()
	TMap<FItemContainerPosition, FRPGItemInstance> Items;

	/** Holds only first positions of items in a container */
	UPROPERTY()
	TMap<FItemContainerPosition, FRPGItemInstance> ItemsUnique;

private:
	/** Returns true if item can occupy certain position */
	UFUNCTION()
	bool CanItemOccupyPosition(const FItemContainerPosition& Position, FRPGItemInstance ItemInstance);
};
