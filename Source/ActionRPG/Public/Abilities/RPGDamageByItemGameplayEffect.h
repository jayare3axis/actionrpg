// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "GameplayEffect.h"
#include "Items\RPGItemTypes.h"
#include "RPGDamageByItemGameplayEffect.generated.h"

/**
 * Damage effect which calculates damage by target item slot.
 */
UCLASS()
class ACTIONRPG_API URPGDamageByItemGameplayEffect : public UGameplayEffect
{
	GENERATED_BODY()

public:
	FORCEINLINE
	FRPGItemSlot GetItemSlot() const { return ItemSlot; }

protected:
	/** Item slot used to calculate damage */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Damage")
	FRPGItemSlot ItemSlot;
	
};
