// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "GameplayEffect.h"
#include "RPGDamageGameplayEffect.generated.h"

/**
 * Gameplay effect storing min and max value for applying damage
 */
UCLASS()
class ACTIONRPG_API URPGDamageGameplayEffect : public UGameplayEffect
{
	GENERATED_BODY()

public:
	/** Evaluates minimal value */
	UFUNCTION()
	float EvalMinValue(float X) const;

	/** Evaluates maximal value */
	UFUNCTION()
	float EvalMaxValue(float X) const;
		
protected:
	/** Curve for minimal damage */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Damage")
	FCurveTableRowHandle MinCurve;

	/** Curve for maximal damage */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Damage")
	FCurveTableRowHandle MaxCurve;
		
};
