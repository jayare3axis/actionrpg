// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "Abilities/Tasks/AbilityTask.h"
#include "AIController.h"
#include "RPGAbilityTask_MoveToTarget.generated.h"

// Forward
class ARPGAIControllerBase;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FRPGAbilityTaskMoveToTargetDelegate, FGameplayTag, EventTag, FGameplayEventData, EventData);

/**
 * Moves RPG character to destination defined by MoveTarget, uses navmesh
 */
UCLASS()
class ACTIONRPG_API URPGAbilityTask_MoveToTarget : public UAbilityTask
{
	GENERATED_BODY()

public:
	// Constructor and overrides
	URPGAbilityTask_MoveToTarget(const FObjectInitializer& ObjectInitializer);
	virtual void Activate() override;
	virtual void ExternalCancel() override;
	virtual FString GetDebugString() const override;
	virtual void OnDestroy(bool AbilityEnded) override;

	/**
	* Uses pathfinding to move target character to specified target, fires OnSuccess event if it reaches the destination
	*/
	UFUNCTION(BlueprintCallable, Category = "Ability|Tasks", meta = (HidePin = "OwningAbility", DefaultToSelf = "OwningAbility", BlueprintInternalUseOnly = "TRUE"))
	static URPGAbilityTask_MoveToTarget* MoveToTarget(UGameplayAbility* OwningAbility, FName TaskInstanceName);

	/** Move result has completed succesfully */
	UPROPERTY(BlueprintAssignable)
	FRPGAbilityTaskMoveToTargetDelegate OnSuccess;

	/** Move request has been aborted */
	UPROPERTY(BlueprintAssignable)
	FRPGAbilityTaskMoveToTargetDelegate OnAborted;

	/** Move request has been blocked */
	UPROPERTY(BlueprintAssignable)
	FRPGAbilityTaskMoveToTargetDelegate OnBlocked;

	/** Target is off path */
	UPROPERTY(BlueprintAssignable)
	FRPGAbilityTaskMoveToTargetDelegate OnOffPath;

	/**Request is canceled */
	UPROPERTY(BlueprintAssignable)
	FRPGAbilityTaskMoveToTargetDelegate OnInvalid;

	/**Ability was canceled */
	UPROPERTY(BlueprintAssignable)
	FRPGAbilityTaskMoveToTargetDelegate OnCancelled;

private:
	UFUNCTION()
	void OnMoveEnd(FAIRequestID RequestID, EPathFollowingResult::Type Result);

	void OnAbilityCancelled();
	void UnbindMoveEndDelegate();

	ARPGAIControllerBase* GetAIController() const;

	FDelegateHandle MoveEndHandle;
	FDelegateHandle CancelledHandle;

};
