// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "GameplayTagContainer.h"
#include "Abilities/RPGAbilityTypes.h"
#include "Abilities/GameplayAbility.h"
#include "RPGGameplayAbility.generated.h"

class URPGGameplayAbility;

/** Ability with a level */
USTRUCT(Blueprintable)
struct FAbilityAndLevel
{
	GENERATED_USTRUCT_BODY()

	FAbilityAndLevel() {}
	FAbilityAndLevel(TSubclassOf<URPGGameplayAbility> _Ability, int32 _Level) : Ability(_Ability), Level(_Level) {}

	UPROPERTY(EditAnywhere, BlueprintReadonly, Category = "Ability")
	TSubclassOf<URPGGameplayAbility> Ability;

	UPROPERTY(EditAnywhere, BlueprintReadonly, Category = "Ability")
	int32 Level;

	bool operator==(const FAbilityAndLevel& Other) const
	{
		// We don't need to compare ability level
		return Ability == Other.Ability;
	}
	bool operator!=(const FAbilityAndLevel& Other) const
	{
		return !(*this == Other);
	}
};

/**
 * Subclass of ability blueprint type with game-specific data
 * This class uses GameplayEffectContainers to allow easier execution of gameplay effects based on a triggering tag
 */
UCLASS()
class ACTIONRPG_API URPGGameplayAbility : public UGameplayAbility
{
	GENERATED_BODY()

public:
	// Constructor
	URPGGameplayAbility();

	/** Make gameplay effect container spec to be applied later, using the passed in container */
	UFUNCTION(BlueprintCallable, Category = Ability, meta = (AutoCreateRefTerm = "EventData"))
	virtual FRPGGameplayEffectContainerSpec MakeEffectContainerSpecFromContainer(const FRPGGameplayEffectContainer& Container, const FGameplayEventData& EventData, int32 OverrideGameplayLevel = -1);

	/** Search for and make a gameplay effect container spec to be applied later, from the EffectContainerMap */
	UFUNCTION(BlueprintCallable, Category = Ability, meta = (AutoCreateRefTerm = "EventData"))
	virtual FRPGGameplayEffectContainerSpec MakeEffectContainerSpec(FGameplayTag ContainerTag, const FGameplayEventData& EventData, int32 OverrideGameplayLevel = -1);

	/** Applies a gameplay effect container spec that was previously created */
	UFUNCTION(BlueprintCallable, Category = Ability)
	virtual TArray<FActiveGameplayEffectHandle> ApplyEffectContainerSpec(const FRPGGameplayEffectContainerSpec& ContainerSpec);

	/** Applies a gameplay effect container, by creating and then applying the spec */
	UFUNCTION(BlueprintCallable, Category = Ability, meta = (AutoCreateRefTerm = "EventData"))
	virtual TArray<FActiveGameplayEffectHandle> ApplyEffectContainer(FGameplayTag ContainerTag, const FGameplayEventData& EventData, int32 OverrideGameplayLevel = -1);
	
	/** Map of gameplay tags to gameplay effect containers */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "GameplayEffects")
	TMap<FGameplayTag, FRPGGameplayEffectContainer> EffectContainerMap;

};
