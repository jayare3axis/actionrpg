// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "GameplayModMagnitudeCalculation.h"
#include "RPGDamageByItemCalculation.generated.h"

/**
 * Calculates damage using damage defined by item at given slot.
 */
UCLASS()
class ACTIONRPG_API URPGDamageByItemCalculation : public UGameplayModMagnitudeCalculation
{
	GENERATED_BODY()

public:
	/** Overrides */
	virtual float CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec& Spec) const override;
	
};
