// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "GameplayModMagnitudeCalculation.h"
#include "RPGRandomDamageCalculation.generated.h"

/**
 * Calculates a damage using a min - max random formula
 */
UCLASS()
class ACTIONRPG_API URPGRandomDamageCalculation : public UGameplayModMagnitudeCalculation
{
	GENERATED_BODY()

public:
	/** Overrides */
	virtual float CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec& Spec) const override;

};
