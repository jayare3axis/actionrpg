// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "GameplayEffect.h"
#include "RPGGameplayEffect.generated.h"

/** Effect with a level */
USTRUCT(Blueprintable)
struct FEffectAndLevel
{
	GENERATED_USTRUCT_BODY()

	FEffectAndLevel() {}
	FEffectAndLevel(TSubclassOf<UGameplayEffect> _Effect, int32 _Level) : Effect(_Effect), Level(_Level) {}

	UPROPERTY(EditAnywhere, BlueprintReadonly, Category = "Ability")
	TSubclassOf<UGameplayEffect> Effect;

	UPROPERTY(EditAnywhere, BlueprintReadonly, Category = "Ability")
	int32 Level;

	bool operator==(const FEffectAndLevel& Other) const
	{
		return Effect == Other.Effect;
	}
	bool operator!=(const FEffectAndLevel& Other) const
	{
		return !(*this == Other);
	}
};
