// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "Abilities/Tasks/AbilityTask.h"
#include "RPGAbilityTask_TurnToTarget.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FRPGAbilityTaskTurnToTargetDelegate, FGameplayTag, EventTag, FGameplayEventData, EventData);

/**
 * Turns RPG character to targat defined by TurnTarget
 */
UCLASS()
class ACTIONRPG_API URPGAbilityTask_TurnToTarget : public UAbilityTask
{
	GENERATED_BODY()

public:
	// Constructor and overrides
	URPGAbilityTask_TurnToTarget(const FObjectInitializer& ObjectInitializer);
	virtual void Activate() override;
	virtual void ExternalCancel() override;
	virtual FString GetDebugString() const override;
	virtual void OnDestroy(bool AbilityEnded) override;

	/**
	* Uses lerp to rotate target character to specified target, fires OnSuccess event when it finishes
	*/
	UFUNCTION(BlueprintCallable, Category = "Ability|Tasks", meta = (HidePin = "OwningAbility", DefaultToSelf = "OwningAbility", BlueprintInternalUseOnly = "TRUE"))
	static URPGAbilityTask_TurnToTarget* TurnToTarget(UGameplayAbility* OwningAbility, FName TaskInstanceName);

	/** Move result has completed succesfully */
	UPROPERTY(BlueprintAssignable)
	FRPGAbilityTaskTurnToTargetDelegate OnSuccess;

	/**Ability was canceled */
	UPROPERTY(BlueprintAssignable)
	FRPGAbilityTaskTurnToTargetDelegate OnCancelled;

private:
	UFUNCTION()
	void OnTurnEnd();

	void OnAbilityCancelled();
	void UnbindTurnEndDelegate();

	FDelegateHandle TurnEndHandle;
	FDelegateHandle CancelledHandle;

};
