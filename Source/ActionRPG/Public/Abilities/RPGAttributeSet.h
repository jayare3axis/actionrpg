// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "AttributeSet.h"
#include "AbilitySystemComponent.h"
#include "RPGAttributeSet.generated.h"

// Forward
class ARPGCharacterBase;
class URPGCombatComponent;

// Uses macros from AttributeSet.h
#define ATTRIBUTE_ACCESSORS(ClassName, PropertyName) \
	GAMEPLAYATTRIBUTE_PROPERTY_GETTER(ClassName, PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_GETTER(PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_SETTER(PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_INITTER(PropertyName)

/**
 * This holds all of the attributes used by abilities, it instantiates a copy of this on every character
 */
UCLASS()
class ACTIONRPG_API URPGAttributeSet : public UAttributeSet
{
	GENERATED_BODY()

public:
	// Constructor and overrides
	URPGAttributeSet();
	virtual void PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue) override;
	virtual void PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data) override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	/** Level of the character */
	UPROPERTY(BlueprintReadOnly, Category = "Level", ReplicatedUsing = OnRep_Level)
	FGameplayAttributeData Level;
	ATTRIBUTE_ACCESSORS(URPGAttributeSet, Level)

	/** Available skill points for character */
	UPROPERTY(BlueprintReadOnly, Category = "Level", ReplicatedUsing = OnRep_AvailableSkillPoints)
	FGameplayAttributeData AvailableSkillPoints;
	ATTRIBUTE_ACCESSORS(URPGAttributeSet, AvailableSkillPoints)

	/** Current experience points */
	UPROPERTY(BlueprintReadOnly, Category = "Level", ReplicatedUsing = OnRep_Experience)
	FGameplayAttributeData Experience;
	ATTRIBUTE_ACCESSORS(URPGAttributeSet, Experience)

	/** Current Health, when 0 we expect owner to die. Capped by MaxHealth */
	UPROPERTY(BlueprintReadOnly, Category = "Health", ReplicatedUsing = OnRep_Health)
	FGameplayAttributeData Health;
	ATTRIBUTE_ACCESSORS(URPGAttributeSet, Health)

	/** MaxHealth is its own attribute, since GameplayEffects may modify it */
	UPROPERTY(BlueprintReadOnly, Category = "Health", ReplicatedUsing = OnRep_MaxHealth)
	FGameplayAttributeData MaxHealth;
	ATTRIBUTE_ACCESSORS(URPGAttributeSet, MaxHealth)

	/** Current Mana, used to execute special abilities. Capped by MaxMana */
	UPROPERTY(BlueprintReadOnly, Category = "Mana", ReplicatedUsing = OnRep_Mana)
	FGameplayAttributeData Mana;
	ATTRIBUTE_ACCESSORS(URPGAttributeSet, Mana)

	/** MaxMana is its own attribute, since GameplayEffects may modify it */
	UPROPERTY(BlueprintReadOnly, Category = "Mana", ReplicatedUsing = OnRep_MaxMana)
	FGameplayAttributeData MaxMana;
	ATTRIBUTE_ACCESSORS(URPGAttributeSet, MaxMana)
		
	/** MoveSpeed affects how fast characters can move */
	UPROPERTY(BlueprintReadOnly, Category = "MoveSpeed", ReplicatedUsing = OnRep_MoveSpeed)
	FGameplayAttributeData MoveSpeed;
	ATTRIBUTE_ACCESSORS(URPGAttributeSet, MoveSpeed)

	/** Damage is a 'temporary' attribute used by the DamageExecution to calculate final damage, which then turns into -Health */
	UPROPERTY(BlueprintReadOnly, Category = "Mana", meta = (HideFromLevelInfos))
	FGameplayAttributeData Damage;
	ATTRIBUTE_ACCESSORS(URPGAttributeSet, Damage)

	/** Strength of the character */
	UPROPERTY(BlueprintReadOnly, Category = "Attributes", ReplicatedUsing = OnRep_Strength)
	FGameplayAttributeData Strength;
	ATTRIBUTE_ACCESSORS(URPGAttributeSet, Strength)

	/** Dexterity of the character */
	UPROPERTY(BlueprintReadOnly, Category = "Attributes", ReplicatedUsing = OnRep_Dexterity)
	FGameplayAttributeData Dexterity;
	ATTRIBUTE_ACCESSORS(URPGAttributeSet, Dexterity)

	/** Endurance of the character */
	UPROPERTY(BlueprintReadOnly, Category = "Attributes", ReplicatedUsing = OnRep_Endurance)
	FGameplayAttributeData Endurance;
	ATTRIBUTE_ACCESSORS(URPGAttributeSet, Endurance)

	/** Intelligence of the character */
	UPROPERTY(BlueprintReadOnly, Category = "Attributes", ReplicatedUsing = OnRep_Intelligence)
	FGameplayAttributeData Intelligence;
	ATTRIBUTE_ACCESSORS(URPGAttributeSet, Intelligence)

	/** Increases change to hit */
	UPROPERTY(BlueprintReadOnly, Category = "Hit", ReplicatedUsing = OnRep_AttackRatingBonus)
	FGameplayAttributeData AttackRatingBonus;
	ATTRIBUTE_ACCESSORS(URPGAttributeSet, AttackRatingBonus)

	/** Decreases change to hit */
	UPROPERTY(BlueprintReadOnly, Category = "Hit", ReplicatedUsing = OnRep_DefenceRatingBonus)
	FGameplayAttributeData DefenceRatingBonus;
	ATTRIBUTE_ACCESSORS(URPGAttributeSet, DefenceRatingBonus)

protected:
	/** Gets combat component of the character */
	URPGCombatComponent* GetCombatComponent(ARPGCharacterBase* Char) const;

	/** Helper function to proportionally adjust the value of an attribute when it's associated max attribute changes. (i.e. When MaxHealth increases, Health increases by an amount that maintains the same percentage as before) */
	void AdjustAttributeForMaxChange(FGameplayAttributeData& AffectedAttribute, const FGameplayAttributeData& MaxAttribute, float NewMaxValue, const FGameplayAttribute& AffectedAttributeProperty);

	// These OnRep functions exist to make sure that the ability system internal representations are synchronized properly during replication
	UFUNCTION()
	virtual void OnRep_Level();

	UFUNCTION()
	virtual void OnRep_AvailableSkillPoints();

	UFUNCTION()
	virtual void OnRep_Experience();
	
	UFUNCTION()
	virtual void OnRep_Health();

	UFUNCTION()
	virtual void OnRep_MaxHealth();

	UFUNCTION()
	virtual void OnRep_Mana();

	UFUNCTION()
	virtual void OnRep_MaxMana();

	UFUNCTION()
	virtual void OnRep_MoveSpeed();

	UFUNCTION()
	virtual void OnRep_Strength();

	UFUNCTION()
	virtual void OnRep_Dexterity();

	UFUNCTION()
	virtual void OnRep_Endurance();

	UFUNCTION()
	virtual void OnRep_Intelligence();

	UFUNCTION()
	virtual void OnRep_AttackRatingBonus();

	UFUNCTION()
	virtual void OnRep_DefenceRatingBonus();

private:
	UFUNCTION()
	void ProcessDamage(ARPGCharacterBase* TargetCharacter, float DeltaValue, UAbilitySystemComponent* Source, const FGameplayTagContainer& SourceTags, const FGameplayEffectContextHandle& Context);

	UFUNCTION()
	void ProcessLevel(ARPGCharacterBase* TargetCharacter, float DeltaValue, UAbilitySystemComponent* Source, const FGameplayTagContainer& SourceTags);

	UFUNCTION()
	void ProcessExperience(ARPGCharacterBase* TargetCharacter, float DeltaValue, UAbilitySystemComponent* Source, const FGameplayTagContainer& SourceTags);
	
	UFUNCTION()
	void ProcessHealth(ARPGCharacterBase* TargetCharacter, float DeltaValue, UAbilitySystemComponent* Source, const FGameplayTagContainer& SourceTags);

	UFUNCTION()
	void ProcessMaxHealth(ARPGCharacterBase* TargetCharacter, float DeltaValue, UAbilitySystemComponent* Source, const FGameplayTagContainer& SourceTags);

	UFUNCTION()
	void ProcessMana(ARPGCharacterBase* TargetCharacter, float DeltaValue, UAbilitySystemComponent* Source, const FGameplayTagContainer& SourceTags);

	UFUNCTION()
	void ProcessMaxMana(ARPGCharacterBase* TargetCharacter, float DeltaValue, UAbilitySystemComponent* Source, const FGameplayTagContainer& SourceTags);

	UFUNCTION()
	void ProcessMoveSpeed(ARPGCharacterBase* TargetCharacter, float DeltaValue, UAbilitySystemComponent* Source, const FGameplayTagContainer& SourceTags);

	UFUNCTION()
	void ProcessStrength(ARPGCharacterBase* TargetCharacter, float DeltaValue, UAbilitySystemComponent* Source, const FGameplayTagContainer& SourceTags);

	UFUNCTION()
	void ProcessDexterity(ARPGCharacterBase* TargetCharacter, float DeltaValue, UAbilitySystemComponent* Source, const FGameplayTagContainer& SourceTags);

	UFUNCTION()
	void ProcessEndurance(ARPGCharacterBase* TargetCharacter, float DeltaValue, UAbilitySystemComponent* Source, const FGameplayTagContainer& SourceTags);

	UFUNCTION()
	void ProcessIntelligence(ARPGCharacterBase* TargetCharacter, float DeltaValue, UAbilitySystemComponent* Source, const FGameplayTagContainer& SourceTags);

	UFUNCTION()
	void ProcessAttackRatingBonus(ARPGCharacterBase* TargetCharacter, float DeltaValue, UAbilitySystemComponent* Source, const FGameplayTagContainer& SourceTags);

	UFUNCTION()
	void ProcessDefenceRatingBonus(ARPGCharacterBase* TargetCharacter, float DeltaValue, UAbilitySystemComponent* Source, const FGameplayTagContainer& SourceTags);
	
};
