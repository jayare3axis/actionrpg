// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "Components/ActorComponent.h"
#include "RPGInitializationInterface.h"
#include "RPGMiniMapPOIComponent.generated.h"

/**
 * When attached to an actor, it will be shown as an icon on the mini-map.
 */
UCLASS( Blueprintable, Abstract, ClassGroup = MiniMap, meta=(BlueprintSpawnableComponent) )
class ACTIONRPG_API URPGMiniMapPOIComponent : public UActorComponent, public IRPGInitializationInterface
{
	GENERATED_BODY()

public:	
	// Constructor and overrides
	URPGMiniMapPOIComponent();
	virtual void BeginPlay() override;

	// IInitializationInterface
	virtual void OnCharacterCreated() override;
	virtual void OnHUDCreated() override;
	virtual void OnReady() override;

	UFUNCTION(BlueprintImplementableEvent, Category = "Mini Map")
	void AddActorIcon();
		
};
