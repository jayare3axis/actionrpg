// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "RPGInitializationInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class URPGInitializationInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * Interface of actors that hook to the initialization sequence.
 */
class ACTIONRPG_API IRPGInitializationInterface
{
	GENERATED_BODY()

public:
	/**
	* Called when local player character is created.
	*/
	UFUNCTION()
	virtual void OnCharacterCreated();

	/**
	* Called when ui for local player is created.
	*/
	UFUNCTION()
	virtual void OnHUDCreated();

	/**
	* Called when initialization process is done.
	*/
	UFUNCTION()		
	virtual void OnReady();

};
