// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "RPGHighlightableInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class URPGHighlightableInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * Interface of actors that are highlighted when cursor hovers above them
 */
class ACTIONRPG_API IRPGHighlightableInterface
{
	GENERATED_BODY()

public:
	/**
	* Called when this actor gets or loses the highlight
	*/
	UFUNCTION()
	virtual void Highlighted(bool Value);

	/**
	* Return true if actor can be highlighted
	*/
	UFUNCTION()
	virtual bool CanBeHighlighted();
	
};
