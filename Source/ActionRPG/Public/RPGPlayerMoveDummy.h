// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "GameFramework/Actor.h"
#include "RPGPlayerMoveDummy.generated.h"

UCLASS()
class ACTIONRPG_API ARPGPlayerMoveDummy : public AActor
{
	GENERATED_BODY()
	
public:	
	ARPGPlayerMoveDummy();
	
};
