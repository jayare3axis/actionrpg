// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "Engine/GameInstance.h"
#include "Engine/DataTable.h"
#include "Items/RPGItemAtSlot.h"
#include "RPGCharacterBase.h"
#include "RPGInitializationInterface.h"
#include "RPGGameInstanceBase.generated.h"

// Forward
class URPGSaveGame;
class ARPGCharacterBase;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCharacterAddedSignature, FRPGCharacterInfo, Character);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCharacterRemovedSignature, FName, Id);

/**
 * Base class for GameInstance, should be blueprinted
 */
UCLASS()
class ACTIONRPG_API URPGGameInstanceBase : public UGameInstance, public IRPGInitializationInterface
{
	GENERATED_BODY()
	
public:
	// Constructor
	URPGGameInstanceBase();

	// IInitializationInterface
	virtual void OnCharacterCreated() override;
	virtual void OnHUDCreated() override;
	virtual void OnReady() override;

	/** Gets current save game */
	FORCEINLINE FName GetCurrentSaveGame() const
	{
		return CurrentSaveGame;
	}
	
	/** Returns save game slot name based on index */
	UFUNCTION()
	static FString GetSaveGameName(int32 Index);

	/** Returns save game slot name based on index */
	UFUNCTION()
	static FString GetSaveGameNameStr(FString IndexAsStr);

	/** Creates new character and saves it */
	UFUNCTION(BlueprintCallable, Category = "Save Game")
	FName CreateNewCharacter(FString CharName, FName Race, FName Class, TArray<int32> AppearanceOptions, TArray<FRPGItemAtSlot> StarterItems);

	/** Removes character */
	UFUNCTION(BlueprintCallable, Category = "Save Game")
	bool RemoveCharacter(FName Id);

	/** Returns list of all available characters */
	UFUNCTION(BlueprintCallable, Category = "Save Game")
	void GetAvailableCharacters(TArray<FRPGCharacterInfo>& List);

	/** Gets character info */
	UFUNCTION(BlueprintCallable, Category = "Save Game")
	bool GetCharacterInfo(FName Id, FRPGCharacterInfo& Info);

	/** Gets save game based on id */
	UFUNCTION(BlueprintCallable, Category = "Save Gane")
	URPGSaveGame* GetSaveGame(FName Id);

	/** Spawns character based on save game data  */
	UFUNCTION(BlueprintCallable, Category = "Save Game", meta = (DisplayName = "SpawnPlayerCharacterUsingSaveGame"))
	ARPGCharacterBase* BlueprintSpawnPlayerCharacterUsingSaveGame(URPGSaveGame* SaveGame, const FVector& SpawnLocation, const FRotator& SpawnRotation, AActor* Owner, APawn* Instigator);

	/** Blueprint implements this to spawn player character based on save game data */
	UFUNCTION(BlueprintImplementableEvent, Category = "Save Game")
	ARPGCharacterBase* SpawnPlayerCharacterUsingSaveGameData(const FString& CharacterName, FName Race, const TArray<FRPGItemAtSlot>& Items, const FVector& SpawnLocation, const FRotator& SpawnRotation, AActor* Owner, APawn* Instigator);

	/** Saves property to save game data */
	UFUNCTION(BlueprintCallable, Category = "Save Game")
	void SaveProperty(const EUpdateProperty& UpdateProperty, float PropertyValue);

	/** Blueprint calls this to notify that back pack items have changed so this info is propagated to save game data */
	UFUNCTION(BlueprintCallable, Category = "Save Game")
	void OnBackPackItemsChanged();

	/** Player controller calls this to update action bindings in save game data */
	UFUNCTION()
	void OnActionBindingAdded(EPlayerActionButton Button, const FRPGItemSlot& ItemSlot);

	/** Player controller calls this to update action bindings in save game data */
	UFUNCTION()
	void OnActionBindingRemoved(EPlayerActionButton Button);

	/** Attempts to find new available user index for save slot */
	UFUNCTION()
	int32 GetAvailableUserCharacterSlot();

	/** Gets experience needed for level */
	UFUNCTION(BlueprintCallable, Category = "Character")
	int32 GetNeededExperienceForLevel(int32 Level);

	/** Gets experience needed for next level */
	UFUNCTION(BlueprintCallable, Category = "Character")
	int32 GetNeededExperienceForNextLevel(int32 CurrentLevel);

	/** Called when player character is spawned */
	UFUNCTION(BlueprintNativeEvent, Category = "Save Game")
	void OnPlayerCharacterCreated();

	/** Called when player character is destroyed */
	UFUNCTION(BlueprintNativeEvent, Category = "Save Game")
	void OnPlayerCharacterDestroyed();

	/** Called when player adds item to reflect changes to save game */
	UFUNCTION()
	void OnPlayerItemAdded(const FRPGItemSlot& ItemSlot, const FRPGItemInstance& ItemInstance);

	/** Called when player removed item to reflect changes to save game */
	UFUNCTION()
	void OnPlayerItemRemoved(const FRPGItemSlot& ItemSlot);

protected:
	/** Used by UI to bind character added event */
	UPROPERTY(BlueprintAssignable, meta = (DisplayName = "CharacterAdded"))
	FCharacterAddedSignature ReceiveCharacterAdded;

	/** Used by UI to bind character removed event */
	UPROPERTY(BlueprintAssignable, meta = (DisplayName = "CharacterRemoved"))
	FCharacterRemovedSignature ReceiveCharacterRemoved;

	/** Determines savae game data to use when game starts */
	UPROPERTY(BlueprintReadWrite, Category = "Data")
	FName CurrentSaveGame;

	/** Curve for minimal damage */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Data")
	FCurveTableRowHandle NeededExperience;

	/** Loads save game or gets it from cache */
	UFUNCTION()
	URPGSaveGame* LoadOrGetSaveGame(int32 Index);

	/** Holds loaded save games */
	UPROPERTY()
	TMap<int32, URPGSaveGame*> SaveGameCache;
	
};
