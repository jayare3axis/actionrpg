// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "RPGMeleeRangeComponent.generated.h"

/**
* This component is responsible of choosing valid melee target during an attack as there could be more candidates but single melee attack can hit only one
*/
UCLASS(Blueprintable, ClassGroup=(Combat), meta=(BlueprintSpawnableComponent))
class ACTIONRPG_API URPGMeleeRangeComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Constructor
	URPGMeleeRangeComponent();

	/** Used by blueprint to tell that target is within character's melee range */
	UFUNCTION(BlueprintCallable, Category = "Attack")
	void TargetRecieved(AActor* Actor);

	/** Used by blueprint to tell that target is no longer within character's melee range */
	UFUNCTION(BlueprintCallable, Category = "Attack")
	void TargetLost(AActor* Actor);

	/** Iterates recieved targets to choose a valid one so damage is applied to one correct actor and not on all the weapon hit */
	UFUNCTION(BlueprintCallable, Category = "Attack")
	AActor* ChooseValidActorFromRecievedTargets();

private:
	UPROPERTY()
	TArray<AActor*> RecievedTargets;
	
};
