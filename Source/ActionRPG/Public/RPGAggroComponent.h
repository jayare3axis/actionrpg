// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "Components/ActorComponent.h"
#include "RPGAggroComponent.generated.h"

// Forward
class ARPGCharacterBase;

/**
* This component allows AI storing sensed enemies and choosing the one which will be current aggro
*/
UCLASS(Blueprintable, ClassGroup = AI, meta = (BlueprintSpawnableComponent))
class ACTIONRPG_API URPGAggroComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Constructor
	URPGAggroComponent();

	/** Called by the perception component to tell that an actor started sensing the target */
	UFUNCTION(BlueprintCallable, Category = "AI")
	void TargetRecieved(AActor* Actor);

	/** Called by the perception component to tell that an actor is no longer being sensed */
	UFUNCTION(BlueprintCallable, Category = "AI")
	void TargetLost(AActor* Actor);

	/** Processes all received targets to choose current aggro */
	UFUNCTION(BlueprintCallable, Category = "AI")
	void UpdateCurrentAggro();

	UPROPERTY(BlueprintReadonly, VisibleAnywhere, Category = "AI")
	AActor* CurrentAggro;

private:
	UFUNCTION()
	ARPGCharacterBase* GetControlledCharacter() const;

	UPROPERTY()
	TArray<AActor*> RecievedTargets;
	
};
