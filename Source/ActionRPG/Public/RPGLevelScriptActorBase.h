// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionRPG.h"
#include "Engine/LevelScriptActor.h"
#include "RPGLevelScriptActorBase.generated.h"

/**
 * Base class for level blueprints of Action RPG.
 */
UCLASS()
class ACTIONRPG_API ARPGLevelScriptActor : public ALevelScriptActor
{
	GENERATED_BODY()

public:
	// Constructor and overrides
	ARPGLevelScriptActor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Level")
	bool bIsOutdoor;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Level")
	float WorldDimensions;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Level")
	float MiniMapZoom;
	
};
