// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;

public class ActionRPG : ModuleRules
{
	public ActionRPG(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
	
		PublicDependencyModuleNames.AddRange(
            new string[] {
                "Core",
                "CoreUObject",
                "Engine"
            }
        );

		PrivateDependencyModuleNames.AddRange(
            new string[] {
                "ActionRPGLoadingScreen",
                "Slate",
                "SlateCore",
                "InputCore",
                "GameplayAbilities",
                "GameplayTags",
                "GameplayTasks",
                "AIModule"
            }
        );
		
		// Uncomment if you are using online features
		// PrivateDependencyModuleNames.Add("OnlineSubsystem");

		// To include OnlineSubsystemSteam, add it to the plugins section in your uproject file with the Enabled attribute set to true
	}
}
