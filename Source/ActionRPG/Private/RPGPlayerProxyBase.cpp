// Fill out your copyright notice in the Description page of FLProject Settings.

#include "RPGPlayerProxyBase.h"
#include "ActionRPG.h"
#include "RPGCharacterBase.h"
#include "RPGPlayerControllerBase.h"
#include "RPGSaveGame.h"
#include "Net/UnrealNetwork.h"
#include "RPGBlueprintFunctionLibrary.h"
#include "RPGInitManagerSubsystem.h"
#include "RPGGameInstanceBase.h"

ARPGPlayerProxyBase::ARPGPlayerProxyBase()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ARPGPlayerProxyBase::BeginPlay()
{
	Super::BeginPlay();
	
	FName CurrentSaveGameId = "";
	
	// If we are attached, try to get current save game and spawn player character
	if (GetController())
	{
		URPGGameInstanceBase* GameInstance = URPGBlueprintFunctionLibrary::GetGameInstance(this);
		CurrentSaveGameId = GameInstance->GetCurrentSaveGame();

		if (!CurrentSaveGameId.IsNone())
		{
			URPGSaveGame* SaveGame = GameInstance->GetSaveGame(CurrentSaveGameId);
			ARPGPlayerControllerBase* PC = Cast<ARPGPlayerControllerBase>(GetController());			
			PC->ServerSpawnPlayerCharacterUsingSaveGameData(SaveGame->CharacterName, SaveGame->Race, SaveGame->AppearanceOptions, SaveGame->Items);
		}		
	}
	
	// If there is not save game and we are running game from the editor, spawn debug player character
	if (HasAuthority() && CurrentSaveGameId.IsNone() && GetWorld()->WorldType == EWorldType::PIE)
	{
		SpawnDebugPlayerCharacter();
		CheckSetCameraViewTarget();
	}
}

void ARPGPlayerProxyBase::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);
	
	if (HasAuthority() && PlayerCharacter)
	{
		InitSlottedAbilitiesSource();
	}
}

void ARPGPlayerProxyBase::CheckSetCameraViewTarget()
{
	// If this proxy is not attached, do not continue
	if (!GetController())
	{
		return;
	}
	
	ARPGPlayerControllerBase* PC = Cast<ARPGPlayerControllerBase>(GetController());
	if (!PC)
	{
		UE_LOG(LogActionRPG, Warning, TEXT("ARPGPlayerProxyBase: Controller must be ARPGPlayerControllerBase!"));
		return;
	}
	
	PC->PlayerCameraManager->SetViewTarget(PlayerCharacter);

	// Override audio listener so 3D sound is working properly because it's attached to the camera by default
	PC->SetAudioListenerOverride(PlayerCharacter->GetRootComponent(), FVector::ZeroVector, FRotator::ZeroRotator);
}

void ARPGPlayerProxyBase::SpawnDebugPlayerCharacter()
{
	if (!DebugPlayerCharacterClass)
	{
		UE_LOG(LogActionRPG, Warning, TEXT("ARPGPlayerProxyBase: DebugPlayerCharacterClass empty!"));
		return;
	}

	FActorSpawnParameters SpawnInfo;
	SpawnInfo.Owner = this;
	SpawnInfo.Instigator = this;
	PlayerCharacter = GetWorld()->SpawnActor<ARPGCharacterBase>(DebugPlayerCharacterClass, GetActorLocation(), GetActorRotation(), SpawnInfo);

	if (HasAuthority() && GetController())
	{
		InitSlottedAbilitiesSource();

		URPGGameInstanceBase* GameInstance = URPGBlueprintFunctionLibrary::GetGameInstance(this);
		URPGInitManagerSubsystem* InitManager = GameInstance->GetSubsystem<URPGInitManagerSubsystem>();
		check(InitManager);

		InitManager->NextStep();
	}
}

void ARPGPlayerProxyBase::InitSlottedAbilitiesSource()
{
	check(PlayerCharacter);

	PlayerCharacter->GetCombatComponent()->InitSlottedAbilitiesSourceAsController(GetController());
}

void ARPGPlayerProxyBase::OnRep_PlayerCharacter()
{
	CheckSetCameraViewTarget();

	if (URPGBlueprintFunctionLibrary::GetLocalPlayerController(this) == GetOwner())
	{
		URPGGameInstanceBase* GameInstance = URPGBlueprintFunctionLibrary::GetGameInstance(this);
		URPGInitManagerSubsystem* InitManager = GameInstance->GetSubsystem<URPGInitManagerSubsystem>();
		check(InitManager);

		InitManager->NextStep();
	}	
}

void ARPGPlayerProxyBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ARPGPlayerProxyBase, PlayerCharacter);
}
