// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGMiniMapPOIComponent.h"
#include "RPGInitManagerSubsystem.h"
#include "RPGBlueprintFunctionLibrary.h"
#include "RPGGameInstanceBase.h"

URPGMiniMapPOIComponent::URPGMiniMapPOIComponent()
{
}

void URPGMiniMapPOIComponent::BeginPlay()
{
	Super::BeginPlay();

	URPGGameInstanceBase* GameInstance = URPGBlueprintFunctionLibrary::GetGameInstance(this);
	URPGInitManagerSubsystem* InitManager = GameInstance->GetSubsystem<URPGInitManagerSubsystem>();
	check(InitManager);

	InitManager->Bind(this);
}

void URPGMiniMapPOIComponent::OnCharacterCreated()
{
}

void URPGMiniMapPOIComponent::OnHUDCreated()
{
}

void URPGMiniMapPOIComponent::OnReady()
{
	AddActorIcon();
}
