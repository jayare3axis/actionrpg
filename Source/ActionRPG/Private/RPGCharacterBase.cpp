// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGCharacterBase.h"
#include "Animation/AnimInstance.h"
#include "RPGAssetManager.h"
#include "Items/RPGItem.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "RPGPlayerControllerBase.h"
#include "GameplayTagsModule.h"
#include "Abilities/RPGGameplayAbility.h"
#include "Components/SkeletalMeshComponent.h"
#include "RPGPlayerProxyBase.h"
#include "RPGTopDownCameraComponent.h"
#include "RPGChasingManagerComponent.h"
#include "RPGPlayerControllerBase.h"
#include "RPGBlueprintFunctionLibrary.h"
#include "RPGGameInstanceBase.h"
#include "RPGCharacterEquipmentComponent.h"
#include "RPGSaveGame.h"

#define ROTATION_INTERP_SPEED 5.f
#define MIN_YAW_DIFF_TO_ANIMATE 10.f

ARPGCharacterBase::ARPGCharacterBase()
{
	// Create ability system component, and set it to be explicitly replicated
	AbilitySystemComponent = CreateDefaultSubobject<URPGAbilitySystemComponent>(TEXT("AbilitySystemComponent"));
	AbilitySystemComponent->SetIsReplicated(true);

	// Create the attribute set, this replicates by default
	AttributeSet = CreateDefaultSubobject<URPGAttributeSet>(TEXT("AttributeSet"));

	// Create chasing manager
	ChasingManagerComponent = CreateDefaultSubobject<URPGChasingManagerComponent>(TEXT("ChasingManagerComponent"));

	// Make character rotate as it moves
	GetCharacterMovement()->bOrientRotationToMovement = true;

	PrimaryActorTick.bCanEverTick = true;

	IsTurning = false;
}

void ARPGCharacterBase::BeginPlay()
{
	Super::BeginPlay();
}

void ARPGCharacterBase::Destroyed()
{
	Super::Destroyed();
}

void ARPGCharacterBase::Tick(float DeltaTime)
{
	//Super::Tick(DeltaTime);

	// Process lerp rotation
	if (IsTurning)
	{
		FRotator NewRotation = FMath::RInterpTo(GetActorRotation(), FinalRotation, DeltaTime, ROTATION_INTERP_SPEED);
		SetActorRotation(NewRotation);

		FRotator Delta = (FinalRotation - GetActorRotation()).GetNormalized();
		if (FMath::Abs(Delta.Yaw) <= MIN_YAW_DIFF_TO_ANIMATE)
		{
			IsTurning = false;
			MulticastAnimateTurnToYaw(0.f, false);

			SetActorRotation(FinalRotation);

			OnTurnEnd.Broadcast();
		}
	}

	// Process chasing targets
	if (HasAuthority())
	{
		ChasingManagerComponent->Process();
	}
}

void ARPGCharacterBase::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);

	// Initialize our abilities
	if (GetCombatComponent())
	{
		GetCombatComponent()->GrantAbilities();
	}
}

void ARPGCharacterBase::UnPossessed()
{
	Super::UnPossessed();

	if (GetCombatComponent())
	{
		GetCombatComponent()->ResetSlottedAbilitiesSource();
	}
}

void ARPGCharacterBase::OnRep_Controller()
{
	Super::OnRep_Controller();

	// Our controller changed, must update ActorInfo on AbilitySystemComponent
	if (AbilitySystemComponent)
	{
		AbilitySystemComponent->RefreshAbilityActorInfo();
	}
}

void ARPGCharacterBase::Interact(ARPGPlayerControllerBase* PC)
{
	BlueprintInteract(PC);
}

void ARPGCharacterBase::InteractClient(ARPGPlayerControllerBase* PC)
{
	BlueprintInteractClient(PC);
}

void ARPGCharacterBase::StopInteraction(ARPGPlayerControllerBase* PC)
{
	BlueprintStopInteraction(PC);
}

void ARPGCharacterBase::StopInteractionClient(ARPGPlayerControllerBase* PC)
{
	BlueprintStopInteractionClient(PC);
}

void ARPGCharacterBase::Highlighted(bool Value)
{
	BlueprintHighlighted(Value);
}

void ARPGCharacterBase::SetSocketedActor(TSubclassOf<AActor> ActorClass, FRotator Rotation, FName SocketName)
{	
	// Destroy previous attached actor
	if (SocketedActors.Contains(SocketName))
	{
		AActor* Actor = SocketedActors[SocketName];
		if (Actor)
		{
			Actor->Destroy();
		}
		SocketedActors.Remove(SocketName);
	}

	if (!ActorClass)
	{		
		return;
	}
	
	// Spawn and attach new actor
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.Owner = this;
	SpawnInfo.Instigator = this;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	AActor* SpawnedActor = GetWorld()->SpawnActor<AActor>(ActorClass, SpawnInfo);

	if (!SpawnedActor)
	{
		return;
	}

	SpawnedActor->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, SocketName);
	SpawnedActor->SetActorRelativeRotation(Rotation);
	SocketedActors.Add(SocketName, SpawnedActor);
}

AActor* ARPGCharacterBase::GetActorAtSocket(FName SocketName)
{
	if (SocketedActors.Contains(SocketName))
	{
		return SocketedActors[SocketName];
	}

	return NULL;
}

URPGTopDownCameraComponent* ARPGCharacterBase::GetTopDownCamera()
{
	return BlueprintGetTopDownCamera();
}

URPGCombatComponent* ARPGCharacterBase::GetCombatComponent()
{
	return BlueprintGetCombatComponent();
}

URPGPlayerAppearanceComponent* ARPGCharacterBase::GetPlayerAppearanceComponent()
{
	return BlueprintGetPlayerAppearanceComponent();
}

URPGCharacterEquipmentComponent* ARPGCharacterBase::GetCharacterEquipmentComponent()
{
	return BlueprintGetCharacterEquipmentComponent();
}

void ARPGCharacterBase::BlueprintTurnToActor(AActor* TargetActor)
{
	SetActionTargetByActor(TargetActor);
	StartTurningAnimation();
}

void ARPGCharacterBase::StartTurningAnimation()
{
	FVector Point = ActionTarget.Actor ? ActionTarget.Actor->GetActorLocation() : ActionTarget.Location;

	FVector Direction = Point - GetActorLocation();
	FRotator NewRotation = Direction.Rotation();

	NewRotation.Yaw = FRotator::ClampAxis(NewRotation.Yaw);

	IsTurning = true;

	const FRotator& CurrentRotation = GetActorRotation();
	FinalRotation = FRotator(CurrentRotation.Pitch, NewRotation.Yaw, CurrentRotation.Roll);

	MulticastAnimateTurnToYaw(NewRotation.Yaw, true);
}

void ARPGCharacterBase::MulticastAnimateTurnToYaw_Implementation(float Yaw, bool bIsTurning)
{
	if (!bIsTurning)
	{
		IsTurning = false;
	}

	BlueprintAnimateTurnToYaw(Yaw, bIsTurning);
}

UAbilitySystemComponent* ARPGCharacterBase::GetAbilitySystemComponent() const
{
	return AbilitySystemComponent;
}

void ARPGCharacterBase::SetMoveTargetByLocation(FVector Target, float AcceptanceRadius)
{	
	MoveTarget = FMoveTarget(Target, NULL, AcceptanceRadius);
}

void ARPGCharacterBase::SetMoveTargetByActor(AActor* Target, float AcceptanceRadius)
{
	if (!Target)
	{
		UE_LOG(LogActionRPG, Warning, TEXT("ARPGCharacterBase: Move target not set!"));
		return;
	}
	MoveTarget = FMoveTarget(Target->GetActorLocation(), Target, AcceptanceRadius);
}

void ARPGCharacterBase::SetActionTargetByLocation(FVector Target)
{
	ActionTarget = FActionTarget(Target, NULL);
}

void ARPGCharacterBase::SetActionTargetByActor(AActor* Target)
{
	if (!Target)
	{
		UE_LOG(LogActionRPG, Warning, TEXT("ARPGCharacterBase: Action target not set!"));
		return;
	}
	ActionTarget = FActionTarget(Target->GetActorLocation(), Target);
}

void ARPGCharacterBase::SetMoveTarget(FMoveTarget Value)
{
	MoveTarget = Value;
	MoveTarget = Value;
}

void ARPGCharacterBase::SetActionTarget(FActionTarget Value)
{
	ActionTarget = Value;
}

void ARPGCharacterBase::TurnToTarget()
{
	FGameplayTagContainer TurnTag;
	UGameplayTagsManager::Get().RequestGameplayTagContainer({ GT_TurnAbility }, TurnTag);
	GetAbilitySystemComponent()->CancelAbilities(&TurnTag);
	GetAbilitySystemComponent()->TryActivateAbilitiesByTag(TurnTag);
}
