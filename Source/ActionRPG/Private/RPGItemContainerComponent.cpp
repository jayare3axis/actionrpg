// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGItemContainerComponent.h"
#include "RPGBlueprintFunctionLibrary.h"
#include "Items/RPGInventoryItem.h"

URPGItemContainerComponent::URPGItemContainerComponent()
{
	XSize = 0;
	YSize = 0;
}

void URPGItemContainerComponent::SetSize(int32 _XSize, int32 _YSize)
{
	XSize = _XSize;
	YSize = _YSize;
}

bool URPGItemContainerComponent::FindPositionForItem(FRPGItemInstance ItemInstance, FItemContainerPosition& Position)
{
	URPGInventoryItem* Item = Cast<URPGInventoryItem>(ItemInstance.Item);
	check(Item);

	for (int32 Y = 0; Y <= YSize - Item->YSize; Y++)
	{
		for (int32 X = 0; X <= XSize - Item->XSize; X++)
		{
			if (CanItemOccupyPosition(FItemContainerPosition(X, Y), ItemInstance))
			{
				Position = FItemContainerPosition(X, Y);
				return true;
			}
		}
	}

	Position = FItemContainerPosition(0, 0);
	return false;
}

bool URPGItemContainerComponent::AddItem(FRPGItemInstance ItemInstance)
{
	FItemContainerPosition Position;
	if (!FindPositionForItem(ItemInstance, Position))
	{
		return false;
	}

	AddItemAtPosition(Position, ItemInstance);

	return true;
}

void URPGItemContainerComponent::AddItemAtPosition(const FItemContainerPosition& Position, FRPGItemInstance ItemInstance)
{
	URPGInventoryItem* Item = Cast<URPGInventoryItem>(ItemInstance.Item);
	check(Item);

	ItemsUnique.Add(Position, ItemInstance);

	int32 PositionX = 0;
	int32 PositionY = 0;
	for (int32 Y = 0; Y < Item->YSize; Y++)
	{
		for (int32 X = 0; X < Item->XSize; X++)
		{
			PositionX = Position.X + X;
			PositionY = Position.Y + Y;
			Items.Add(FItemContainerPosition(PositionX, PositionY), ItemInstance);
		}
	}

	ReceiveItemAdded.Broadcast(Position, ItemInstance);
}

bool URPGItemContainerComponent::RemoveItem(FRPGItemInstance ItemInstance)
{
	bool IsFound = false;

	TArray<FItemContainerPosition> PositionsToRemove;

	for (const TPair<FItemContainerPosition, FRPGItemInstance>& Pair : Items)
	{
		if (Pair.Value == ItemInstance)
		{
			PositionsToRemove.Add(Pair.Key);
		}
	}

	for (const TPair<FItemContainerPosition, FRPGItemInstance>& Pair : ItemsUnique)
	{
		if (Pair.Value == ItemInstance)
		{
			ItemsUnique.Remove(Pair.Key);
			break;
		}
	}

	IsFound = PositionsToRemove.Num() > 0;

	if (IsFound)
	{
		for (const FItemContainerPosition Position : PositionsToRemove)
		{
			Items.Remove(Position);
		}
		PositionsToRemove.Empty();

		ReceiveItemRemoved.Broadcast(ItemInstance);
	}

	return IsFound;
}

void URPGItemContainerComponent::GetItems(TMap<FItemContainerPosition, FRPGItemInstance>& Result)
{
	Result = ItemsUnique;
}

bool URPGItemContainerComponent::CanPlaceItemAtPosition(const FItemContainerPosition& Position, FRPGItemInstance ItemInstance)
{
	TArray<FRPGItemInstance> Items;

	URPGInventoryItem* Item = Cast<URPGInventoryItem>(ItemInstance.Item);
	check(Item);

	GetItemsByTiles(Position, Item->XSize, Item->YSize, Items);
	
	return Items.Num() <= 1;
}

void URPGItemContainerComponent::GetItemsByTiles(const FItemContainerPosition& Position, int32 SizeX, int32 SizeY, TArray<FRPGItemInstance>& _Items)
{
	_Items = TArray<FRPGItemInstance>();

	for (int32 Y = Position.Y; Y < Position.Y + SizeY; Y++)
	{
		for (int32 X = Position.X; X < Position.X + SizeX; X++)
		{
			FItemContainerPosition Position = FItemContainerPosition(X, Y);
			if (Items.Contains(Position) && !_Items.Contains(Items[Position]))
			{
				_Items.Add(Items[Position]);
			}
		}
	}
}

void URPGItemContainerComponent::LoadFromData(const TArray<FRPGItemInContainerRaw>& Items)
{
	URPGAssetManager& AssetManager = URPGAssetManager::Get();

	for (const FRPGItemInContainerRaw& InItem : Items)
	{
		URPGItem* LoadedItem = AssetManager.ForceLoadItem(InItem.Item);
		FRPGItemInstance ItemInstance = URPGBlueprintFunctionLibrary::CreateItemInstance(GetOwner(), LoadedItem, InItem.Count, InItem.Level);

		AddItemAtPosition(FItemContainerPosition(InItem.Position.X, InItem.Position.Y), ItemInstance);
	}
}

bool URPGItemContainerComponent::CanItemOccupyPosition(const FItemContainerPosition& Position, FRPGItemInstance ItemInstance)
{
	for (const TPair<FItemContainerPosition, FRPGItemInstance>& Pair : Items)
	{
		if (Pair.Key == Position)
		{
			return false;
		}
	}

	return true;
}
