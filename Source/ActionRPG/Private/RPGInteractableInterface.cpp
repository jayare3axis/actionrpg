// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGInteractableInterface.h"
#include "ActionRPG.h"
#include "RPGPlayerControllerBase.h"

void IRPGInteractableInterface::Interact(ARPGPlayerControllerBase* PC)
{
}

void IRPGInteractableInterface::InteractClient(ARPGPlayerControllerBase* PC)
{
}

void IRPGInteractableInterface::StopInteraction(ARPGPlayerControllerBase* PC)
{
}

void IRPGInteractableInterface::StopInteractionClient(ARPGPlayerControllerBase* PC)
{
}

bool IRPGInteractableInterface::CanBeInteract()
{
	return true;
}

bool IRPGInteractableInterface::ShouldAttackOnInteraction(ARPGPlayerControllerBase* PC)
{
	return false;
}
