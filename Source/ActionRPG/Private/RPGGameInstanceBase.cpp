// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGGameInstanceBase.h"
#include "RPGSaveGame.h"
#include "RPGCharacterBase.h"
#include "RPGBlueprintFunctionLibrary.h"
#include "RPGPlayerControllerBase.h"
#include "RPGItemContainerComponent.h"
#include "RPGInitManagerSubsystem.h"
#include "ActionRPG.h"

URPGGameInstanceBase::URPGGameInstanceBase()
{
}

void URPGGameInstanceBase::OnCharacterCreated()
{
	OnPlayerCharacterCreated();
}

void URPGGameInstanceBase::OnHUDCreated()
{
	FName CurrentSaveGameId = GetCurrentSaveGame();
	if (!CurrentSaveGameId.IsNone())
	{
		ARPGPlayerControllerBase* PC = URPGBlueprintFunctionLibrary::GetLocalPlayerController(this);
		URPGSaveGame* SaveGame = GetSaveGame(CurrentSaveGameId);
		PC->ClientInitActionBindingsUsingSaveGameData(SaveGame->ActionBindings);
	}

	URPGInitManagerSubsystem* InitManager = GetSubsystem<URPGInitManagerSubsystem>();
	check(InitManager);
	InitManager->NextStep();
}

void URPGGameInstanceBase::OnReady()
{
}

FString URPGGameInstanceBase::GetSaveGameName(int32 Index)
{
	return FString("RPGCharacter-") + FString::FromInt(Index);
}

FString URPGGameInstanceBase::GetSaveGameNameStr(FString IndexAsStr)
{
	return FString("RPGCharacter-") + IndexAsStr;
}

FName URPGGameInstanceBase::CreateNewCharacter(FString CharName, FName Race, FName Class, TArray<int32> AppearanceOptions, TArray<FRPGItemAtSlot> StarterItems)
{
	int32 SlotNum = GetAvailableUserCharacterSlot();
	if (SlotNum == -1)
	{
		// TODO handle failure, propably in the blueprint
		return FName();
	}
	
	URPGSaveGame* SaveGameInstance = Cast<URPGSaveGame>(UGameplayStatics::CreateSaveGameObject(URPGSaveGame::StaticClass()));
	SaveGameInstance->CharacterName = CharName;
	SaveGameInstance->Race = Race;
	SaveGameInstance->Class = Class;
	SaveGameInstance->AppearanceOptions = AppearanceOptions;
	for (FRPGItemAtSlot ItemAtSlot : StarterItems)
	{
		SaveGameInstance->Items.Add(FRPGItemAtSlotRaw(ItemAtSlot.Slot, ItemAtSlot.Item->GetPrimaryAssetId(), ItemAtSlot.Level));
	}

	UGameplayStatics::SaveGameToSlot(SaveGameInstance, URPGGameInstanceBase::GetSaveGameName(SlotNum), 0);

	SaveGameCache.Add(SlotNum, SaveGameInstance);

	// Fire event to blueprint
	FString Id = FString::FromInt(SlotNum);
	ReceiveCharacterAdded.Broadcast(
		FRPGCharacterInfo(SaveGameInstance->CharacterName, SaveGameInstance->Class, SaveGameInstance->Level, FName(*Id))
	);

	return FName(*Id);
}

bool URPGGameInstanceBase::RemoveCharacter(FName Id)
{
	bool Result = UGameplayStatics::DeleteGameInSlot( URPGGameInstanceBase::GetSaveGameNameStr(Id.ToString()), 0 );

	ReceiveCharacterRemoved.Broadcast(Id);

	return Result;
}

void URPGGameInstanceBase::GetAvailableCharacters(TArray<FRPGCharacterInfo>& List)
{
	List = TArray<FRPGCharacterInfo>();

	URPGSaveGame* SaveGameInstance;
	for (int32 i = 0; i < 256; i++)
	{
		if (UGameplayStatics::DoesSaveGameExist(URPGGameInstanceBase::GetSaveGameName(i), 0))
		{
			SaveGameInstance = LoadOrGetSaveGame(i);
			check(SaveGameInstance);

			FString Id = FString::FromInt(i);
			List.Add(
				FRPGCharacterInfo(SaveGameInstance->CharacterName, SaveGameInstance->Class, SaveGameInstance->Level, FName(*Id))
			);
		}
	}
}

bool URPGGameInstanceBase::GetCharacterInfo(FName Id, FRPGCharacterInfo& Info)
{
	Info = FRPGCharacterInfo();

	TArray<FRPGCharacterInfo> List;
	GetAvailableCharacters(List);

	for (FRPGCharacterInfo Character : List)
	{
		if (Character.Id == Id)
		{
			Info = Character;
			return true;
		}
	}

	return false;
}

URPGSaveGame* URPGGameInstanceBase::GetSaveGame(FName Id)
{
	int32 IdInt = FCString::Atoi(*Id.ToString());
	return LoadOrGetSaveGame(IdInt);
}

ARPGCharacterBase* URPGGameInstanceBase::BlueprintSpawnPlayerCharacterUsingSaveGame(URPGSaveGame* SaveGame, const FVector& SpawnLocation, const FRotator& SpawnRotation, AActor* Owner, APawn* Instigator)
{
	TArray<FRPGItemAtSlot> Items;
	URPGAssetManager& AssetManager = URPGAssetManager::Get();
	for (const FRPGItemAtSlotRaw& Val : SaveGame->Items)
	{
		URPGItem* LoadedItem = AssetManager.ForceLoadItem(Val.Item);
		Items.Add(FRPGItemAtSlot(Val.ItemSlot, LoadedItem, Val.Level));
	}

	ARPGCharacterBase* PlayerCharacter = SpawnPlayerCharacterUsingSaveGameData(SaveGame->CharacterName, SaveGame->Race, Items, SpawnLocation, SpawnRotation, Owner, Instigator);
	PlayerCharacter->GetPlayerAppearanceComponent()->InitAppearanceOptions(SaveGame->Race, SaveGame->AppearanceOptions);

	return PlayerCharacter;
}

void URPGGameInstanceBase::SaveProperty(const EUpdateProperty& UpdateProperty, float PropertyValue)
{
	FName CurrentSaveGameId = GetCurrentSaveGame();
	if (CurrentSaveGameId.IsNone())
	{
		return;
	}

	URPGSaveGame* SaveGameInstance = GetSaveGame(CurrentSaveGameId);
	if (!SaveGameInstance)
	{
		return;
	}

	switch (UpdateProperty)
	{
		case EUpdateProperty::UPE_Level:
			SaveGameInstance->Level = PropertyValue;
			break;
		case EUpdateProperty::UPE_Experience:
			SaveGameInstance->Experience = PropertyValue;
			break;
		case EUpdateProperty::UPE_SkillPoints:
			SaveGameInstance->AvailableSkillPoints = PropertyValue;
			break;
		case EUpdateProperty::UPE_Health:
			SaveGameInstance->Health = PropertyValue;
			break;
		case EUpdateProperty::UPE_Mana:
			SaveGameInstance->Mana = PropertyValue;
			break;
	}

	FString SlotName = URPGGameInstanceBase::GetSaveGameNameStr(CurrentSaveGameId.ToString());
	UGameplayStatics::SaveGameToSlot(SaveGameInstance, SlotName, 0);
}

void URPGGameInstanceBase::OnBackPackItemsChanged()
{
	FName CurrentSaveGameId = GetCurrentSaveGame();
	if (CurrentSaveGameId.IsNone())
	{
		return;
	}

	URPGSaveGame* SaveGameInstance = GetSaveGame(CurrentSaveGameId);
	if (!SaveGameInstance)
	{
		return;
	}

	SaveGameInstance->BackPackItems.Empty();

	URPGItemContainerComponent* BackPack = URPGBlueprintFunctionLibrary::GetLocalPlayerController(this)->GetBackpack();
	TMap<FItemContainerPosition, FRPGItemInstance> BackPackItems;
	BackPack->GetItems(BackPackItems);

	for (TPair<FItemContainerPosition, FRPGItemInstance> Pair : BackPackItems)
	{
		SaveGameInstance->BackPackItems.Add(
			FRPGItemInContainerRaw(Pair.Key, Pair.Value.Item->GetPrimaryAssetId(), Pair.Value.Count, Pair.Value.Level)
		);
	}

	FString SlotName = URPGGameInstanceBase::GetSaveGameNameStr(CurrentSaveGameId.ToString());
	UGameplayStatics::SaveGameToSlot(SaveGameInstance, SlotName, 0);
}

void URPGGameInstanceBase::OnActionBindingAdded(EPlayerActionButton Button, const FRPGItemSlot& ItemSlot)
{
	FName CurrentSaveGameId = GetCurrentSaveGame();
	if (CurrentSaveGameId.IsNone())
	{
		return;
	}

	URPGSaveGame* SaveGameInstance = GetSaveGame(CurrentSaveGameId);
	if (!SaveGameInstance)
	{
		return;
	}

	SaveGameInstance->ActionBindings.Add(Button, ItemSlot);

	FString SlotName = URPGGameInstanceBase::GetSaveGameNameStr(CurrentSaveGameId.ToString());
	UGameplayStatics::SaveGameToSlot(SaveGameInstance, SlotName, 0);
}

void URPGGameInstanceBase::OnActionBindingRemoved(EPlayerActionButton Button)
{
	FName CurrentSaveGameId = GetCurrentSaveGame();
	if (CurrentSaveGameId.IsNone())
	{
		return;
	}

	URPGSaveGame* SaveGameInstance = GetSaveGame(CurrentSaveGameId);
	if (!SaveGameInstance)
	{
		return;
	}

	SaveGameInstance->ActionBindings.Remove(Button);

	FString SlotName = URPGGameInstanceBase::GetSaveGameNameStr(CurrentSaveGameId.ToString());
	UGameplayStatics::SaveGameToSlot(SaveGameInstance, SlotName, 0);
}

int32 URPGGameInstanceBase::GetAvailableUserCharacterSlot()
{
	bool IsAvailable = false;
	int32 AttemptNum = 0;

	// For now we just try get first available index under 256
	while (AttemptNum < 256)
	{
		if (!UGameplayStatics::DoesSaveGameExist(URPGGameInstanceBase::GetSaveGameName(AttemptNum), 0))
		{
			return AttemptNum;
		}

		AttemptNum++;
	}

	return -1;
}

int32 URPGGameInstanceBase::GetNeededExperienceForLevel(int32 Level)
{
	if (Level == 1)
	{
		return 0;
	}

	if (NeededExperience.IsNull())
	{
		return 0.f;
	}

	return NeededExperience.Eval(Level, "");
}

void URPGGameInstanceBase::OnPlayerCharacterCreated_Implementation()
{
	ARPGPlayerControllerBase* PC = URPGBlueprintFunctionLibrary::GetLocalPlayerController(this);
	FName CurrentSaveGameId = GetCurrentSaveGame();

	if (!CurrentSaveGameId.IsNone())
	{
		URPGSaveGame* SaveGame = GetSaveGame(GetCurrentSaveGame());
		if (SaveGame)
		{
			PC->ServerInitAttributesBySaveGameData(
				SaveGame->Level, SaveGame->Experience, SaveGame->AvailableSkillPoints, SaveGame->Health, SaveGame->Mana
			);
			PC->ClientInitBackPackItemsBySaveGameData(SaveGame->BackPackItems);
		}
	}
}

void URPGGameInstanceBase::OnPlayerCharacterDestroyed_Implementation()
{

}

void URPGGameInstanceBase::OnPlayerItemAdded(const FRPGItemSlot& ItemSlot, const FRPGItemInstance& ItemInstance)
{
	FName CurrentSaveGameId = GetCurrentSaveGame();
	if (CurrentSaveGameId.IsNone())
	{
		return;
	}

	URPGSaveGame* SaveGameInstance = GetSaveGame(CurrentSaveGameId);
	if (!SaveGameInstance)
	{
		return;
	}

	int32 Index = -1;
	for (int32 i = 0; i < SaveGameInstance->Items.Num(); i++)
	{
		const FRPGItemAtSlotRaw& ItemAtSlot = SaveGameInstance->Items[i];
		if (ItemAtSlot.ItemSlot == ItemSlot)
		{
			Index = i;
			break;
		}
	}

	FRPGItemAtSlotRaw NewItem = FRPGItemAtSlotRaw(ItemSlot, ItemInstance.Item->GetPrimaryAssetId(), ItemInstance.Level);
	if (Index == -1)
	{
		SaveGameInstance->Items.Add(NewItem);
	}
	else
	{
		SaveGameInstance->Items[Index] = NewItem;
	}

	UGameplayStatics::SaveGameToSlot(SaveGameInstance, URPGGameInstanceBase::GetSaveGameNameStr(CurrentSaveGameId.ToString()), 0);
}

void URPGGameInstanceBase::OnPlayerItemRemoved(const FRPGItemSlot& ItemSlot)
{
	FName CurrentSaveGameId = GetCurrentSaveGame();
	if (CurrentSaveGameId.IsNone())
	{
		return;
	}

	URPGSaveGame* SaveGameInstance = GetSaveGame(CurrentSaveGameId);
	if (!SaveGameInstance)
	{
		return;
	}
	
	int32 Index = -1;
	for (int32 i = 0; i < SaveGameInstance->Items.Num(); i++)
	{
		const FRPGItemAtSlotRaw& ItemAtSlot = SaveGameInstance->Items[i];
		if (ItemAtSlot.ItemSlot == ItemSlot)
		{
			Index = i;
			break;
		}
	}

	if (Index > -1)
	{
		SaveGameInstance->Items.RemoveAt(Index);
	}

	UGameplayStatics::SaveGameToSlot(SaveGameInstance, URPGGameInstanceBase::GetSaveGameNameStr(CurrentSaveGameId.ToString()), 0);
}

int32 URPGGameInstanceBase::GetNeededExperienceForNextLevel(int32 CurrentLevel)
{
	return GetNeededExperienceForLevel(CurrentLevel + 1);
}

URPGSaveGame* URPGGameInstanceBase::LoadOrGetSaveGame(int32 Index)
{
	if (SaveGameCache.Contains(Index))
	{
		return SaveGameCache[Index];
	}

	URPGSaveGame* LoadGameInstance = Cast<URPGSaveGame>(UGameplayStatics::CreateSaveGameObject(URPGSaveGame::StaticClass()));
	LoadGameInstance = Cast<URPGSaveGame>(UGameplayStatics::LoadGameFromSlot(URPGGameInstanceBase::GetSaveGameName(Index), 0));

	return LoadGameInstance;
}
