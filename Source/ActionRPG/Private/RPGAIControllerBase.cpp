// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGAIControllerBase.h"
#include "ActionRPG.h"
#include "Navigation/CrowdFollowingComponent.h"

// For now - we use RVO avoidance rather than Crow Following, it could be found in Character Movement - Avoidance section,
// note that different characters may need different Avoidance Consideration Radius values
ARPGAIControllerBase::ARPGAIControllerBase(const FObjectInitializer& ObjectInitializer)
	//: Super(ObjectInitializer.SetDefaultSubobjectClass<UCrowdFollowingComponent>(TEXT("PathFollowingComponent")))
{
}

void ARPGAIControllerBase::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result)
{
	Super::OnMoveCompleted(RequestID, Result);

	OnMoveEnd.Broadcast(RequestID, Result.Code);
}
