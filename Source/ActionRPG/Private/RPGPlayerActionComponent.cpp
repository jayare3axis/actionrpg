// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGPlayerActionComponent.h"
#include "RPGCharacterBase.h"
#include "RPGPlayerControllerBase.h"
#include "RPGCombatComponent.h"
#include "GameplayTagsModule.h"

URPGPlayerActionComponent::URPGPlayerActionComponent()
{
	
}

void URPGPlayerActionComponent::TurnToTargetAndExecuteItemSlot(const FVector& Target, const FRPGItemSlot& ItemSlot)
{
	ARPGPlayerControllerBase* PC = Cast<ARPGPlayerControllerBase>( GetOwner() );
	check(PC);

	ARPGCharacterBase* Char = PC->GetPlayerCharacter();
	if (!Char)
	{
		return;
	}

	UnbindTurnEndedIfBound();
	UnbindAbilityEndedIfBound();
	
	ItemSlotToUse = ItemSlot;
	
	Char->SetActionTarget(FActionTarget(Target, NULL));

	CancelPreviousMovement();

	Char->TurnToTarget();
	TurnEndHandle = Char->OnTurnEnd.AddUObject(this, &URPGPlayerActionComponent::OnTurnEnd);
}

void URPGPlayerActionComponent::MoveToTargetAndExecuteItemSlot(AActor* Target, const FRPGItemSlot& ItemSlot)
{
	if (!Target)
	{
		return;
	}

	ARPGPlayerControllerBase* PC = Cast<ARPGPlayerControllerBase>(GetOwner());
	check(PC);

	ARPGCharacterBase* Char = PC->GetPlayerCharacter();
	if (!Char)
	{
		return;
	}

	UnbindTurnEndedIfBound();
	UnbindAbilityEndedIfBound();

	ItemSlotToUse = ItemSlot;
	bIsInteraction = false;

	CancelPreviousMovement();

	Char->SetActionTarget(FActionTarget(
		Target->GetActorLocation(),
		Target
	));

	float Distance = (Char->GetActorLocation() - Target->GetActorLocation()).Size();
	if (Distance > MinInteractionDistance)
	{

		AbilityEndedHandle = Char->GetAbilitySystemComponent()->OnAbilityEnded.AddUObject(this, &URPGPlayerActionComponent::OnAbilityEnd);
		MoveToActor(Target, false);
	}
	else
	{
		TurnToTargetAndExecuteItemSlot(Target->GetActorLocation(), ItemSlot);
	}
}

void URPGPlayerActionComponent::MoveToTargetAndInteract(AActor* Target, const FVector& TargetLocation, bool bIsActionTargetSet)
{
	ARPGPlayerControllerBase* PC = Cast<ARPGPlayerControllerBase>(GetOwner());
	check(PC);

	ARPGCharacterBase* Char = PC->GetPlayerCharacter();
	if (!Char)
	{
		return;
	}

	UnbindTurnEndedIfBound();
	UnbindAbilityEndedIfBound();

	bIsInteraction = true;

	if (!bIsActionTargetSet)
	{
		Char->SetActionTarget(FActionTarget(
			TargetLocation,
			Target
		));
	}

	float Distance = (Char->GetActorLocation() - TargetLocation).Size();
	if (Distance > MinInteractionDistance)
	{

		AbilityEndedHandle = Char->GetAbilitySystemComponent()->OnAbilityEnded.AddUObject(this, &URPGPlayerActionComponent::OnAbilityEnd);
		if (Target)
		{
			MoveToActor(Target, false);
		}
		else
		{
			MoveToPoint(TargetLocation, false);
		}
	}
	else
	{
		OnInteract();
	}
}

void URPGPlayerActionComponent::MoveToActor(AActor* Target, bool bUnbind)
{
	ARPGPlayerControllerBase* PC = Cast<ARPGPlayerControllerBase>(GetOwner());
	check(PC);

	ARPGCharacterBase* Char = PC->GetPlayerCharacter();
	if (!Char)
	{
		return;
	}

	if (bUnbind)
	{
		UnbindAbilityEndedIfBound();
	}
	UnbindTurnEndedIfBound();

	CancelPreviousMovement();	

	FGameplayTagContainer MovementTag;
	UGameplayTagsManager::Get().RequestGameplayTagContainer({ GT_MoveAbility }, MovementTag);
	Char->SetMoveTarget(FMoveTarget(Target->GetActorLocation(), Target, -1.f));
	Char->GetAbilitySystemComponent()->TryActivateAbilitiesByTag(MovementTag);
}

void URPGPlayerActionComponent::MoveToPoint(const FVector& Target, bool bUnbind)
{
	ARPGPlayerControllerBase* PC = Cast<ARPGPlayerControllerBase>(GetOwner());
	check(PC);

	ARPGCharacterBase* Char = PC->GetPlayerCharacter();
	if (!Char)
	{
		return;
	}

	if (bUnbind)
	{
		UnbindAbilityEndedIfBound();
	}
	UnbindTurnEndedIfBound();

	CancelPreviousMovement();

	FGameplayTagContainer MovementTag;
	UGameplayTagsManager::Get().RequestGameplayTagContainer({ GT_MoveAbility }, MovementTag);
	Char->SetMoveTarget(FMoveTarget(Target, NULL, -1.f));
	Char->GetAbilitySystemComponent()->TryActivateAbilitiesByTag(MovementTag);
}

void URPGPlayerActionComponent::CleanUp()
{
	UnbindTurnEndedIfBound();
	UnbindAbilityEndedIfBound();
}

void URPGPlayerActionComponent::OnTurnEnd()
{
	ARPGPlayerControllerBase* PC = Cast<ARPGPlayerControllerBase>(GetOwner());
	check(PC);

	ARPGCharacterBase* Char = PC->GetPlayerCharacter();

	if (Char)
	{
		// We are now facing the target - attack
		URPGCombatComponent* CombatComponent = Char->GetCombatComponent();
		if (CombatComponent)
		{
			CombatComponent->TryAbilityByItemSlot(ItemSlotToUse);
		}
		else
		{
			UE_LOG(LogActionRPG, Warning, TEXT("Player Action Component: Combat Component not present!"));
		}		
	}

	UnbindTurnEndedIfBound();
}

void URPGPlayerActionComponent::OnAbilityEnd(const FAbilityEndedData& Event)
{
	ARPGPlayerControllerBase* PC = Cast<ARPGPlayerControllerBase>(GetOwner());
	check(PC);	

	ARPGCharacterBase* Char = PC->GetPlayerCharacter();

	// Check if ended ability was succesfull movement
	if (Event.AbilityThatEnded->AbilityTags.HasTag(UGameplayTagsManager::Get().RequestGameplayTag(GT_MoveAbility)) && !Event.bWasCancelled)
	{
		if (bIsInteraction)
		{
			OnInteract();
		}
		else
		{
			const FActionTarget& ActionTarget = Char->GetActionTarget();
			TurnToTargetAndExecuteItemSlot(ActionTarget.Location, ItemSlotToUse);
		}

		UnbindAbilityEndedIfBound();
	}
}

void URPGPlayerActionComponent::OnInteract()
{
	ARPGPlayerControllerBase* PC = Cast<ARPGPlayerControllerBase>(GetOwner());
	check(PC);

	ARPGCharacterBase* Char = PC->GetPlayerCharacter();

	const FActionTarget& ActionTarget = Char->GetActionTarget();
	if (ActionTarget.Actor)
	{
		IRPGInteractableInterface* ITarget = Cast<IRPGInteractableInterface>(ActionTarget.Actor);
		if (ITarget->CanBeInteract())
		{
			// Runs interaction on server
			ITarget->Interact(PC);
		}
	}

	// Runs interaction on client
	PC->ClientInteract(ActionTarget.Actor);
}

void URPGPlayerActionComponent::UnbindTurnEndedIfBound()
{
	ARPGPlayerControllerBase* PC = Cast<ARPGPlayerControllerBase>(GetOwner());
	check(PC);

	ARPGCharacterBase* Char = PC->GetPlayerCharacter();

	if (Char && TurnEndHandle.IsValid())
	{
		Char->OnTurnEnd.Remove(TurnEndHandle);
		TurnEndHandle.Reset();
	}
}

void URPGPlayerActionComponent::UnbindAbilityEndedIfBound()
{
	ARPGPlayerControllerBase* PC = Cast<ARPGPlayerControllerBase>(GetOwner());
	check(PC);

	ARPGCharacterBase* Char = PC->GetPlayerCharacter();

	if (Char && AbilityEndedHandle.IsValid())
	{
		Char->GetAbilitySystemComponent()->OnAbilityEnded.Remove(AbilityEndedHandle);
		AbilityEndedHandle.Reset();
	}
}

void URPGPlayerActionComponent::CancelPreviousMovement()
{
	ARPGPlayerControllerBase* PC = Cast<ARPGPlayerControllerBase>(GetOwner());
	check(PC);

	ARPGCharacterBase* Char = PC->GetPlayerCharacter();
	if (!Char)
	{
		return;
	}

	FGameplayTagContainer MovementTag;
	UGameplayTagsManager::Get().RequestGameplayTagContainer({ GT_MoveAbility }, MovementTag);
	Char->GetAbilitySystemComponent()->CancelAbilities(&MovementTag);
}