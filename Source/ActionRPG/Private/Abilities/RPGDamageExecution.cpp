// Fill out your copyright notice in the Description page of Project Settings.

#include "Abilities/RPGDamageExecution.h"
#include "Abilities/RPGAttributeSet.h"
#include "RPGBlueprintFunctionLibrary.h"
#include "AbilitySystemComponent.h"

struct RPGSourceDamageStatics
{
	DECLARE_ATTRIBUTE_CAPTUREDEF(Level);
	DECLARE_ATTRIBUTE_CAPTUREDEF(Strength);
	DECLARE_ATTRIBUTE_CAPTUREDEF(Dexterity);
	DECLARE_ATTRIBUTE_CAPTUREDEF(AttackRatingBonus);
	DECLARE_ATTRIBUTE_CAPTUREDEF(Damage);

	RPGSourceDamageStatics()
	{
		// Capture the Source's AttackPower. We do want to snapshot this at the moment we create the GameplayEffectSpec that will execute the damage.
		// (imagine we fire a projectile: we create the GE Spec when the projectile is fired. When it hits the target, we want to use the AttackPower at the moment
		// the projectile was launched, not when it hits).
		DEFINE_ATTRIBUTE_CAPTUREDEF(URPGAttributeSet, Level, Source, true);
		DEFINE_ATTRIBUTE_CAPTUREDEF(URPGAttributeSet, Strength, Source, true);
		DEFINE_ATTRIBUTE_CAPTUREDEF(URPGAttributeSet, Dexterity, Source, true);
		DEFINE_ATTRIBUTE_CAPTUREDEF(URPGAttributeSet, AttackRatingBonus, Source, true);

		// Also capture the source's raw Damage, which is normally passed in directly via the execution
		DEFINE_ATTRIBUTE_CAPTUREDEF(URPGAttributeSet, Damage, Source, true);
	}
};

static const RPGSourceDamageStatics& SourceDamageStatics()
{
	static RPGSourceDamageStatics DmgStatics;
	return DmgStatics;
}

struct RPGTargetDamageStatics
{
	DECLARE_ATTRIBUTE_CAPTUREDEF(Level);
	DECLARE_ATTRIBUTE_CAPTUREDEF(Dexterity);
	DECLARE_ATTRIBUTE_CAPTUREDEF(Endurance);
	DECLARE_ATTRIBUTE_CAPTUREDEF(DefenceRatingBonus);

	RPGTargetDamageStatics()
	{
		// Capture the Target's DefensePower attribute. Do not snapshot it, because we want to use the health value at the moment we apply the execution.
		DEFINE_ATTRIBUTE_CAPTUREDEF(URPGAttributeSet, Level, Target, false);
		DEFINE_ATTRIBUTE_CAPTUREDEF(URPGAttributeSet, Dexterity, Target, false);
		DEFINE_ATTRIBUTE_CAPTUREDEF(URPGAttributeSet, Endurance, Target, false);
		DEFINE_ATTRIBUTE_CAPTUREDEF(URPGAttributeSet, DefenceRatingBonus, Target, false);
	}
};

static const RPGTargetDamageStatics& TargetDamageStatics()
{
	static RPGTargetDamageStatics DmgStatics;
	return DmgStatics;
}

URPGDamageExecution::URPGDamageExecution()
{
	RelevantAttributesToCapture.Add(SourceDamageStatics().LevelDef);
	RelevantAttributesToCapture.Add(SourceDamageStatics().StrengthDef);
	RelevantAttributesToCapture.Add(SourceDamageStatics().DexterityDef);	
	RelevantAttributesToCapture.Add(SourceDamageStatics().AttackRatingBonusDef);
	RelevantAttributesToCapture.Add(SourceDamageStatics().DamageDef);

	RelevantAttributesToCapture.Add(TargetDamageStatics().LevelDef);
	RelevantAttributesToCapture.Add(TargetDamageStatics().DexterityDef);
	RelevantAttributesToCapture.Add(TargetDamageStatics().EnduranceDef);
	RelevantAttributesToCapture.Add(TargetDamageStatics().DefenceRatingBonusDef);
}

void URPGDamageExecution::Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, OUT FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const
{
	UAbilitySystemComponent* TargetAbilitySystemComponent = ExecutionParams.GetTargetAbilitySystemComponent();
	UAbilitySystemComponent* SourceAbilitySystemComponent = ExecutionParams.GetSourceAbilitySystemComponent();

	AActor* SourceActor = SourceAbilitySystemComponent ? SourceAbilitySystemComponent->AvatarActor : nullptr;
	AActor* TargetActor = TargetAbilitySystemComponent ? TargetAbilitySystemComponent->AvatarActor : nullptr;

	const FGameplayEffectSpec& Spec = ExecutionParams.GetOwningSpec();

	// Gather the tags from the source and target as that can affect which buffs should be used
	const FGameplayTagContainer* SourceTags = Spec.CapturedSourceTags.GetAggregatedTags();
	const FGameplayTagContainer* TargetTags = Spec.CapturedTargetTags.GetAggregatedTags();

	FAggregatorEvaluateParameters EvaluationParameters;
	EvaluationParameters.SourceTags = SourceTags;
	EvaluationParameters.TargetTags = TargetTags;

	// Get required attributes
	float SourceLevel = 0.f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(SourceDamageStatics().LevelDef, EvaluationParameters, SourceLevel);
	float SourceStrength = 0.f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(SourceDamageStatics().StrengthDef, EvaluationParameters, SourceStrength);
	float SourceDexterity = 0.f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(SourceDamageStatics().DexterityDef, EvaluationParameters, SourceDexterity);
	float SourceAttackRatingBonus = 0.f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(SourceDamageStatics().AttackRatingBonusDef, EvaluationParameters, SourceAttackRatingBonus);

	float TargetLevel = 0.f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(TargetDamageStatics().LevelDef, EvaluationParameters, TargetLevel);
	float TargetDexterity = 0.f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(TargetDamageStatics().DexterityDef, EvaluationParameters, TargetDexterity);
	float TargetEndurance = 0.f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(TargetDamageStatics().EnduranceDef, EvaluationParameters, TargetEndurance);
	float TargetDefenceRatingBonus = 0.f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(TargetDamageStatics().DefenceRatingBonusDef, EvaluationParameters, TargetDefenceRatingBonus);

	// Damage
	float Damage = 0.f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(SourceDamageStatics().DamageDef, EvaluationParameters, Damage);

	float DamageDone = Damage;
	
	// Process hit rating
	bool bIsHit = true;
	if (URPGBlueprintFunctionLibrary::IsMeleeAttack(*SourceTags))
	{
		float SourceAttackRating = SourceDexterity + SourceAttackRatingBonus;
		float TargetDefenceRating = TargetDexterity + TargetDefenceRatingBonus;
		float HitChance = SourceAttackRating / (SourceAttackRating + TargetDefenceRating) * 2 * SourceLevel / (SourceLevel + TargetLevel);
		
		bIsHit = FMath::RandRange(0.f, 1.f) >= 1.f - HitChance;
	}

	// Process damage multiplier
	bool bUseStrength = URPGBlueprintFunctionLibrary::IsMeleeAttack(*SourceTags);
	float SourceAttribute = bUseStrength ? SourceStrength : SourceDexterity;
	float TargetAttribute = TargetEndurance;
	float DamageMultiplier = FMath::Clamp(SourceAttribute / TargetAttribute, 0.5f, 2.f);
	DamageDone = FMath::RoundToInt(DamageDone * DamageMultiplier);

	// Minimal damage is 1
	if (DamageDone == 0.f)
	{
		DamageDone = 1.f;
	}

	if (!bIsHit)
	{
		DamageDone = 0.f;
	}

	if (DamageDone > 0.f)
	{
		OutExecutionOutput.AddOutputModifier(FGameplayModifierEvaluatedData(SourceDamageStatics().DamageProperty, EGameplayModOp::Additive, DamageDone));
	}
}
