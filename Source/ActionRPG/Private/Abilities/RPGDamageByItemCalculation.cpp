// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGDamageByItemCalculation.h"
#include "RPGDamageByItemGameplayEffect.h"

float URPGDamageByItemCalculation::CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec& Spec) const
{
	const URPGDamageByItemGameplayEffect* DamageEffect = Cast<URPGDamageByItemGameplayEffect>(Spec.Def);
	if (!DamageEffect)
	{
		UE_LOG(LogActionRPG, Warning, TEXT("URPGRandomDamageCalculation: Effect to efecute must be instance of URPGDamageByItemGameplayEffect!"));
		return 0.f;
	}

	const FRPGItemSlot& ItemSlot = DamageEffect->GetItemSlot();

	if (Spec.GetContext().GetEffectCauser()) {
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Green, Spec.GetContext().GetEffectCauser()->GetName());
	}

	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, TEXT("Here!"));
	
	return 1.f;
}
 