// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGAttributeSet.h"
#include "Abilities/RPGAbilitySystemComponent.h"
#include "RPGCharacterBase.h"
#include "RPGBlueprintFunctionLibrary.h"
#include "RPGGameInstanceBase.h"
#include "GameplayEffect.h"
#include "GameplayEffectExtension.h"
#include "RPGCombatComponent.h"

URPGAttributeSet::URPGAttributeSet()
	: Level(1.f)
	, AvailableSkillPoints(0.f)
	, Experience(0.f)
	, Health(1.f)
	, MaxHealth(1.f)
	, Mana(0.f)
	, MaxMana(0.f)
	, MoveSpeed(1.0f)
	, Damage(0.0f)
	, Strength(0.0f)
	, Dexterity(0.0f)
	, Endurance(0.0f)
	, Intelligence(0.0f)
	, AttackRatingBonus(0.0f)
	, DefenceRatingBonus(0.0f)
{
}

void URPGAttributeSet::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(URPGAttributeSet, Level);
	DOREPLIFETIME(URPGAttributeSet, AvailableSkillPoints);
	DOREPLIFETIME(URPGAttributeSet, Experience);
	DOREPLIFETIME(URPGAttributeSet, Health);
	DOREPLIFETIME(URPGAttributeSet, MaxHealth);
	DOREPLIFETIME(URPGAttributeSet, Mana);
	DOREPLIFETIME(URPGAttributeSet, MaxMana);
	DOREPLIFETIME(URPGAttributeSet, MoveSpeed);
	DOREPLIFETIME(URPGAttributeSet, Strength);
	DOREPLIFETIME(URPGAttributeSet, Dexterity);
	DOREPLIFETIME(URPGAttributeSet, Endurance);
	DOREPLIFETIME(URPGAttributeSet, Intelligence);
	DOREPLIFETIME(URPGAttributeSet, AttackRatingBonus);
	DOREPLIFETIME(URPGAttributeSet, DefenceRatingBonus);
}

void URPGAttributeSet::OnRep_Level()
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(URPGAttributeSet, Level);
}

void URPGAttributeSet::OnRep_AvailableSkillPoints()
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(URPGAttributeSet, AvailableSkillPoints);
}

void URPGAttributeSet::OnRep_Experience()
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(URPGAttributeSet, Experience);
}

void URPGAttributeSet::OnRep_Health()
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(URPGAttributeSet, Health);
}

void URPGAttributeSet::OnRep_MaxHealth()
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(URPGAttributeSet, MaxHealth);
}

void URPGAttributeSet::OnRep_Mana()
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(URPGAttributeSet, Mana);
}

void URPGAttributeSet::OnRep_MaxMana()
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(URPGAttributeSet, MaxMana);
}

void URPGAttributeSet::OnRep_MoveSpeed()
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(URPGAttributeSet, MoveSpeed);
}

void URPGAttributeSet::OnRep_Strength()
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(URPGAttributeSet, Strength);
}

void URPGAttributeSet::OnRep_Dexterity()
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(URPGAttributeSet, Dexterity);
}

void URPGAttributeSet::OnRep_Endurance()
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(URPGAttributeSet, Endurance);
}

void URPGAttributeSet::OnRep_Intelligence()
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(URPGAttributeSet, Intelligence);
}

void URPGAttributeSet::OnRep_AttackRatingBonus()
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(URPGAttributeSet, AttackRatingBonus);
}

void URPGAttributeSet::OnRep_DefenceRatingBonus()
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(URPGAttributeSet, DefenceRatingBonus);
}

URPGCombatComponent* URPGAttributeSet::GetCombatComponent(ARPGCharacterBase* Char) const
{
	URPGCombatComponent* CombatComponent = Char->GetCombatComponent();
	if (!CombatComponent)
	{
		UE_LOG(LogActionRPG, Error, TEXT("Combat component not present!"));
		return NULL;
	}

	return CombatComponent;
}

void URPGAttributeSet::AdjustAttributeForMaxChange(FGameplayAttributeData& AffectedAttribute, const FGameplayAttributeData& MaxAttribute, float NewMaxValue, const FGameplayAttribute& AffectedAttributeProperty)
{
	UAbilitySystemComponent* AbilityComp = GetOwningAbilitySystemComponent();
	const float CurrentMaxValue = MaxAttribute.GetCurrentValue();
	if (!FMath::IsNearlyEqual(CurrentMaxValue, NewMaxValue) && AbilityComp)
	{
		// Change current value to maintain the current Val / Max percent
		const float CurrentValue = AffectedAttribute.GetCurrentValue();
		float NewDelta = (CurrentMaxValue > 0.f) ? (CurrentValue * NewMaxValue / CurrentMaxValue) - CurrentValue : NewMaxValue;

		AbilityComp->ApplyModToAttributeUnsafe(AffectedAttributeProperty, EGameplayModOp::Additive, NewDelta);
	}
}

void URPGAttributeSet::PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue)
{
	// This is called whenever attributes change, so for max we want to scale the current totals to match
	Super::PreAttributeChange(Attribute, NewValue);

	if (Attribute == GetMaxHealthAttribute())
	{
		AdjustAttributeForMaxChange(Health, MaxHealth, NewValue, GetHealthAttribute());
	}
	else if (Attribute == GetMaxManaAttribute())
	{
		AdjustAttributeForMaxChange(Mana, MaxMana, NewValue, GetManaAttribute());
	}
}

void URPGAttributeSet::PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data)
{
	Super::PostGameplayEffectExecute(Data);

	FGameplayEffectContextHandle Context = Data.EffectSpec.GetContext();
	UAbilitySystemComponent* Source = Context.GetOriginalInstigatorAbilitySystemComponent();
	const FGameplayTagContainer& SourceTags = *Data.EffectSpec.CapturedSourceTags.GetAggregatedTags();

	// Compute the delta between old and new, if it is available
	float DeltaValue = 0;
	if (Data.EvaluatedData.ModifierOp == EGameplayModOp::Type::Additive)
	{
		// If this was additive, store the raw delta value to be passed along later
		DeltaValue = Data.EvaluatedData.Magnitude;
	}

	// Get the Target actor, which should be our owner
	AActor* TargetActor = NULL;
	AController* TargetController = NULL;
	ARPGCharacterBase* TargetCharacter = NULL;
	if (Data.Target.AbilityActorInfo.IsValid() && Data.Target.AbilityActorInfo->AvatarActor.IsValid())
	{
		TargetActor = Data.Target.AbilityActorInfo->AvatarActor.Get();
		TargetController = Data.Target.AbilityActorInfo->PlayerController.Get();
		TargetCharacter = Cast<ARPGCharacterBase>(TargetActor);
	}

	if (!TargetCharacter)
	{
		return;
	}

	if (Data.EvaluatedData.Attribute == GetDamageAttribute())
	{
		ProcessDamage(TargetCharacter, DeltaValue, Source, SourceTags, Context);
	}
	else if (Data.EvaluatedData.Attribute == GetLevelAttribute())
	{
		ProcessLevel(TargetCharacter, DeltaValue, Source, SourceTags);
	}
	else if (Data.EvaluatedData.Attribute == GetExperienceAttribute())
	{
		ProcessExperience(TargetCharacter, DeltaValue, Source, SourceTags);
	}
	else if (Data.EvaluatedData.Attribute == GetHealthAttribute())
	{
		ProcessHealth(TargetCharacter, DeltaValue, Source, SourceTags);
	}
	else if (Data.EvaluatedData.Attribute == GetMaxHealthAttribute())
	{
		ProcessMaxHealth(TargetCharacter, DeltaValue, Source, SourceTags);
	}
	else if (Data.EvaluatedData.Attribute == GetManaAttribute())
	{
		ProcessMana(TargetCharacter, DeltaValue, Source, SourceTags);
	}
	else if (Data.EvaluatedData.Attribute == GetMaxManaAttribute())
	{
		ProcessMaxMana(TargetCharacter, DeltaValue, Source, SourceTags);
	}
	else if (Data.EvaluatedData.Attribute == GetMoveSpeedAttribute())
	{
		ProcessMoveSpeed(TargetCharacter, DeltaValue, Source, SourceTags);
	}
	else if (Data.EvaluatedData.Attribute == GetStrengthAttribute())
	{
		ProcessStrength(TargetCharacter, DeltaValue, Source, SourceTags);
	}
	else if (Data.EvaluatedData.Attribute == GetDexterityAttribute())
	{
		ProcessDexterity(TargetCharacter, DeltaValue, Source, SourceTags);
	}
	else if (Data.EvaluatedData.Attribute == GetEnduranceAttribute())
	{
		ProcessEndurance(TargetCharacter, DeltaValue, Source, SourceTags);
	}
	else if (Data.EvaluatedData.Attribute == GetIntelligenceAttribute())
	{
		ProcessIntelligence(TargetCharacter, DeltaValue, Source, SourceTags);
	}
	else if (Data.EvaluatedData.Attribute == GetAttackRatingBonusAttribute())
	{
		ProcessAttackRatingBonus(TargetCharacter, DeltaValue, Source, SourceTags);
	}
	else if (Data.EvaluatedData.Attribute == GetDefenceRatingBonusAttribute())
	{
		ProcessDefenceRatingBonus(TargetCharacter, DeltaValue, Source, SourceTags);
	}
}

void URPGAttributeSet::ProcessDamage(ARPGCharacterBase* TargetCharacter, float DeltaValue, UAbilitySystemComponent* Source, const FGameplayTagContainer& SourceTags, const FGameplayEffectContextHandle& Context)
{
	URPGCombatComponent* CombatComponent = TargetCharacter->GetCombatComponent();
	check(CombatComponent);

	// Get the Source actor
	AActor* SourceActor = NULL;
	AController* SourceController = NULL;
	ARPGCharacterBase* SourceCharacter = NULL;
	if (Source && Source->AbilityActorInfo.IsValid() && Source->AbilityActorInfo->AvatarActor.IsValid())
	{
		SourceActor = Source->AbilityActorInfo->AvatarActor.Get();
		SourceController = Source->AbilityActorInfo->PlayerController.Get();
		if (SourceController == NULL && SourceActor != NULL)
		{
			if (APawn* Pawn = Cast<APawn>(SourceActor))
			{
				SourceController = Pawn->GetController();
			}
		}

		// Use the controller to find the source pawn
		if (SourceController)
		{
			SourceCharacter = Cast<ARPGCharacterBase>(SourceController->GetPawn());
		}
		else
		{
			SourceCharacter = Cast<ARPGCharacterBase>(SourceActor);
		}

		// Set the causer actor based on context if it's set
		if (Context.GetEffectCauser())
		{
			SourceActor = Context.GetEffectCauser();
		}
	}

	// Try to extract a hit result
	FHitResult HitResult;
	if (Context.GetHitResult())
	{
		HitResult = *Context.GetHitResult();
	}

	// Store a local copy of the amount of damage done and clear the damage attribute
	const float LocalDamageDone = GetDamage();
	SetDamage(0.f);

	if (LocalDamageDone == 0)
	{
		return;
	}
	
	// Apply the health change and then clamp it
	const float OldHealth = GetHealth();
	SetHealth(FMath::Clamp(OldHealth - LocalDamageDone, 0.0f, GetMaxHealth()));

	// This is proper damage
	CombatComponent->HandleDamage(LocalDamageDone, HitResult, SourceTags, SourceCharacter, SourceActor);

	// Call for all health changes
	CombatComponent->HandleAttributeChanged(EAttributeName::ANE_Health, -LocalDamageDone, SourceTags);
}

void URPGAttributeSet::ProcessLevel(ARPGCharacterBase* TargetCharacter, float DeltaValue, UAbilitySystemComponent* Source, const FGameplayTagContainer& SourceTags)
{
	URPGCombatComponent* CombatComponent = TargetCharacter->GetCombatComponent();
	check(CombatComponent);

	// Call for all level changes
	CombatComponent->HandleAttributeChanged(EAttributeName::ANE_Level, DeltaValue, SourceTags);
}

void URPGAttributeSet::ProcessExperience(ARPGCharacterBase* TargetCharacter, float DeltaValue, UAbilitySystemComponent* Source, const FGameplayTagContainer& SourceTags)
{
	URPGCombatComponent* CombatComponent = TargetCharacter->GetCombatComponent();
	check(CombatComponent);

	// Call for all experience changes
	CombatComponent->HandleAttributeChanged(EAttributeName::ANE_Experience, DeltaValue, SourceTags);

	URPGGameInstanceBase* GameInstance = URPGBlueprintFunctionLibrary::GetGameInstance(TargetCharacter);
	int32 GainedLevels = 0;

	while (GameInstance->GetNeededExperienceForNextLevel(CombatComponent->GetAttributeValue(EAttributeName::ANE_Level) + GainedLevels) > GameInstance->GetNeededExperienceForLevel(CombatComponent->GetAttributeValue(EAttributeName::ANE_Level) + GainedLevels) &&
		CombatComponent->GetAttributeValue(EAttributeName::ANE_Experience) >= GameInstance->GetNeededExperienceForNextLevel(CombatComponent->GetAttributeValue(EAttributeName::ANE_Level) + GainedLevels))
	{
		GainedLevels++;
	}

	// Add levels, if any
	if (GainedLevels > 0)
	{
		SetLevel(GetLevel() + GainedLevels);
		CombatComponent->HandleAttributeChanged(EAttributeName::ANE_Level, GainedLevels, SourceTags);
	}
}

void URPGAttributeSet::ProcessHealth(ARPGCharacterBase* TargetCharacter, float DeltaValue, UAbilitySystemComponent* Source, const FGameplayTagContainer& SourceTags)
{
	URPGCombatComponent* CombatComponent = TargetCharacter->GetCombatComponent();
	check(CombatComponent);

	// Handle other health changes such as from healing or direct modifiers
	// First clamp it
	SetHealth(FMath::Clamp(GetHealth(), 0.0f, GetMaxHealth()));

	// Call for all health changes
	CombatComponent->HandleAttributeChanged(EAttributeName::ANE_Health, DeltaValue, SourceTags);
}

void URPGAttributeSet::ProcessMaxHealth(ARPGCharacterBase* TargetCharacter, float DeltaValue, UAbilitySystemComponent* Source, const FGameplayTagContainer& SourceTags)
{
	URPGCombatComponent* CombatComponent = TargetCharacter->GetCombatComponent();
	check(CombatComponent);

	// Call for all max health changes
	CombatComponent->HandleAttributeChanged(EAttributeName::ANE_MaxHealth, DeltaValue, SourceTags);
}

void URPGAttributeSet::ProcessMana(ARPGCharacterBase* TargetCharacter, float DeltaValue, UAbilitySystemComponent* Source, const FGameplayTagContainer& SourceTags)
{
	URPGCombatComponent* CombatComponent = TargetCharacter->GetCombatComponent();
	check(CombatComponent);

	// Clamp mana
	SetMana(FMath::Clamp(GetMana(), 0.0f, GetMaxMana()));

	// Call for all mana changes
	CombatComponent->HandleAttributeChanged(EAttributeName::ANE_Mana, DeltaValue, SourceTags);
}

void URPGAttributeSet::ProcessMaxMana(ARPGCharacterBase* TargetCharacter, float DeltaValue, UAbilitySystemComponent* Source, const FGameplayTagContainer& SourceTags)
{
	URPGCombatComponent* CombatComponent = TargetCharacter->GetCombatComponent();
	check(CombatComponent);

	// Call for all max health changes
	CombatComponent->HandleAttributeChanged(EAttributeName::ANE_MaxMana, DeltaValue, SourceTags);
}

void URPGAttributeSet::ProcessMoveSpeed(ARPGCharacterBase* TargetCharacter, float DeltaValue, UAbilitySystemComponent* Source, const FGameplayTagContainer& SourceTags)
{
	URPGCombatComponent* CombatComponent = TargetCharacter->GetCombatComponent();
	check(CombatComponent);

	// Call for all movespeed changes
	CombatComponent->HandleAttributeChanged(EAttributeName::ANE_MoveSpeed, DeltaValue, SourceTags);
}

void URPGAttributeSet::ProcessStrength(ARPGCharacterBase* TargetCharacter, float DeltaValue, UAbilitySystemComponent* Source, const FGameplayTagContainer& SourceTags)
{
	URPGCombatComponent* CombatComponent = TargetCharacter->GetCombatComponent();
	check(CombatComponent);

	// Call for all strength changes
	CombatComponent->HandleAttributeChanged(EAttributeName::ANE_Strength, DeltaValue, SourceTags);
}

void URPGAttributeSet::ProcessDexterity(ARPGCharacterBase* TargetCharacter, float DeltaValue, UAbilitySystemComponent* Source, const FGameplayTagContainer& SourceTags)
{
	URPGCombatComponent* CombatComponent = TargetCharacter->GetCombatComponent();
	check(CombatComponent);

	// Call for all dexterity changes
	CombatComponent->HandleAttributeChanged(EAttributeName::ANE_Dexterity,  DeltaValue, SourceTags);
}

void URPGAttributeSet::ProcessEndurance(ARPGCharacterBase* TargetCharacter, float DeltaValue, UAbilitySystemComponent* Source, const FGameplayTagContainer& SourceTags)
{
	URPGCombatComponent* CombatComponent = TargetCharacter->GetCombatComponent();
	check(CombatComponent);

	// Call for all toughness changes
	CombatComponent->HandleAttributeChanged(EAttributeName::ANE_Endurance, DeltaValue, SourceTags);
}

void URPGAttributeSet::ProcessIntelligence(ARPGCharacterBase* TargetCharacter, float DeltaValue, UAbilitySystemComponent* Source, const FGameplayTagContainer& SourceTags)
{
	URPGCombatComponent* CombatComponent = TargetCharacter->GetCombatComponent();
	check(CombatComponent);

	// Call for all intelligence changes
	CombatComponent->HandleAttributeChanged(EAttributeName::ANE_Intelligence, DeltaValue, SourceTags);
}

void URPGAttributeSet::ProcessAttackRatingBonus(ARPGCharacterBase* TargetCharacter, float DeltaValue, UAbilitySystemComponent* Source, const FGameplayTagContainer& SourceTags)
{
	URPGCombatComponent* CombatComponent = TargetCharacter->GetCombatComponent();
	check(CombatComponent);

	// Call for all attack rating bonus changes
	CombatComponent->HandleAttributeChanged(EAttributeName::ANE_AttackRatingBonus, DeltaValue, SourceTags);
}

void URPGAttributeSet::ProcessDefenceRatingBonus(ARPGCharacterBase* TargetCharacter, float DeltaValue, UAbilitySystemComponent* Source, const FGameplayTagContainer& SourceTags)
{
	URPGCombatComponent* CombatComponent = TargetCharacter->GetCombatComponent();
	check(CombatComponent);

	// Call for all defence rating bonus changes
	CombatComponent->HandleAttributeChanged(EAttributeName::ANE_DefenceRatingBonus, DeltaValue, SourceTags);
}
