// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGAbilityTask_TurnToTarget.h"
#include "ActionRPG.h"
#include "RPGTypes.h"
#include "RPGAIControllerBase.h"
#include "RPGCharacterBase.h"

URPGAbilityTask_TurnToTarget::URPGAbilityTask_TurnToTarget(const FObjectInitializer& ObjectInitializer)	: Super(ObjectInitializer)
{
}

void URPGAbilityTask_TurnToTarget::Activate()
{
	if (Ability == NULL)
	{
		return;
	}

	const FGameplayAbilityActorInfo* ActorInfo = Ability->GetCurrentActorInfo();
	ARPGCharacterBase* Character = Cast<ARPGCharacterBase>(ActorInfo->OwnerActor.Get());

	TurnEndHandle = Character->OnTurnEnd.AddUObject(this, &URPGAbilityTask_TurnToTarget::OnTurnEnd);
	CancelledHandle = Ability->OnGameplayAbilityCancelled.AddUObject(this, &URPGAbilityTask_TurnToTarget::OnAbilityCancelled);

	Character->StartTurningAnimation();

	// If character is performing move - stop
	ARPGAIControllerBase* AIController = Cast<ARPGAIControllerBase>(Character->GetController());
	if (AIController)
	{
		// If controller is not here it means the character is dead while the ability got executed
		AIController->StopMovement();
	}
}

void URPGAbilityTask_TurnToTarget::ExternalCancel()
{
	check(AbilitySystemComponent);

	OnAbilityCancelled();
	
	Super::ExternalCancel();
}

FString URPGAbilityTask_TurnToTarget::GetDebugString() const
{
	// TODO implement
	return FString::Printf(TEXT("TurnToTarget. Location: %s  "), "");
}

void URPGAbilityTask_TurnToTarget::OnDestroy(bool AbilityEnded)
{
	UnbindTurnEndDelegate();

	if (Ability)
	{
		Ability->OnGameplayAbilityCancelled.Remove(CancelledHandle);
	}

	Super::OnDestroy(AbilityEnded);

}

URPGAbilityTask_TurnToTarget* URPGAbilityTask_TurnToTarget::TurnToTarget(UGameplayAbility* OwningAbility, FName TaskInstanceName)
{
	URPGAbilityTask_TurnToTarget* Task = NewAbilityTask<URPGAbilityTask_TurnToTarget>(OwningAbility, TaskInstanceName);
	
	return Task;
}

void URPGAbilityTask_TurnToTarget::OnTurnEnd()
{
	if (ShouldBroadcastAbilityTaskDelegates())
	{
		OnSuccess.Broadcast(FGameplayTag(), FGameplayEventData());
	}
}

void URPGAbilityTask_TurnToTarget::OnAbilityCancelled()
{
	const FGameplayAbilityActorInfo* ActorInfo = Ability->GetCurrentActorInfo();
	ARPGCharacterBase* Character = Cast<ARPGCharacterBase>(ActorInfo->OwnerActor.Get());

	Character->MulticastAnimateTurnToYaw(0.f, false);

	OnCancelled.Broadcast(FGameplayTag(), FGameplayEventData());
}

void URPGAbilityTask_TurnToTarget::UnbindTurnEndDelegate()
{
	const FGameplayAbilityActorInfo* ActorInfo = Ability->GetCurrentActorInfo();
	ARPGCharacterBase* Character = Cast<ARPGCharacterBase>(ActorInfo->OwnerActor.Get());

	Character->OnTurnEnd.Remove(TurnEndHandle);
}
