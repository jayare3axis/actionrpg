// Fill out your copyright notice in the Description page of Project Settings.

#include "Abilities/RPGTargetType.h"
#include "Abilities/RPGGameplayAbility.h"
#include "Engine/LocalPlayer.h"
#include "RPGCharacterBase.h"
#include "RPGPlayerControllerBase.h"
#include "RPGBlueprintFunctionLibrary.h"
#include "RPGPlayerManagerSubsystem.h"

void URPGTargetType::GetTargets_Implementation(ARPGCharacterBase* TargetingCharacter, AActor* TargetingActor, FGameplayEventData EventData, TArray<FHitResult>& OutHitResults, TArray<AActor*>& OutActors) const
{
	return;
}

void URPGTargetType_UseOwner::GetTargets_Implementation(ARPGCharacterBase* TargetingCharacter, AActor* TargetingActor, FGameplayEventData EventData, TArray<FHitResult>& OutHitResults, TArray<AActor*>& OutActors) const
{
	OutActors.Add(TargetingCharacter);
}

void URPGTargetType_UseEventData::GetTargets_Implementation(ARPGCharacterBase* TargetingCharacter, AActor* TargetingActor, FGameplayEventData EventData, TArray<FHitResult>& OutHitResults, TArray<AActor*>& OutActors) const
{
	const FHitResult* FoundHitResult = EventData.ContextHandle.GetHitResult();
	if (FoundHitResult)
	{
		OutHitResults.Add(*FoundHitResult);
	}
	else if (EventData.Target)
	{
		OutActors.Add(const_cast<AActor*>(EventData.Target));
	}
}

void URPGTargetType_UseParty::GetTargets_Implementation(ARPGCharacterBase* TargetingCharacter, AActor* TargetingActor, FGameplayEventData EventData, TArray<FHitResult>& OutHitResults, TArray<AActor*>& OutActors) const
{
	ARPGPlayerControllerBase* PC = URPGBlueprintFunctionLibrary::GetLocalPlayerController(TargetingCharacter);
	check(PC);

	ULocalPlayer* LocalPlayer = PC->GetLocalPlayer();
	check(LocalPlayer);

	for (ARPGCharacterBase* Character : LocalPlayer->GetSubsystem<URPGPlayerManagerSubsystem>()->GetPlayerCharacters())
	{
		OutActors.Add(Character);
	}
}