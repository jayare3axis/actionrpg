// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGDamageGameplayEffect.h"

float URPGDamageGameplayEffect::EvalMinValue(float X) const
{
	if (MinCurve.IsNull())
	{
		return 0.f;
	}

	return MinCurve.Eval(X, "");
}

float URPGDamageGameplayEffect::EvalMaxValue(float X) const
{
	if (MaxCurve.IsNull())
	{
		return 0.f;
	}

	return MaxCurve.Eval(X, "");
}
