// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGRandomDamageCalculation.h"
#include "RPGDamageGameplayEffect.h"

float URPGRandomDamageCalculation::CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec& Spec) const
{
	const URPGDamageGameplayEffect* DamageEffect = Cast<URPGDamageGameplayEffect>(Spec.Def);
	if (!DamageEffect)
	{
		UE_LOG(LogActionRPG, Warning, TEXT("URPGRandomDamageCalculation: Effect to efecute must be instance of URPGDamageGameplayEffect!"));
		return 0.f;
	}

	float Min = DamageEffect->EvalMinValue(Spec.GetLevel());
	float Max = DamageEffect->EvalMaxValue(Spec.GetLevel());

	return FMath::RoundHalfFromZero(FMath::RandRange(Min, Max));
}
