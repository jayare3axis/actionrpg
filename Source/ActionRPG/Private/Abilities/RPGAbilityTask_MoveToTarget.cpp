// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGAbilityTask_MoveToTarget.h"
#include "ActionRPG.h"
#include "RPGTypes.h"
#include "RPGAIControllerBase.h"
#include "RPGCharacterBase.h"

URPGAbilityTask_MoveToTarget::URPGAbilityTask_MoveToTarget(const FObjectInitializer& ObjectInitializer)	: Super(ObjectInitializer)
{
}

void URPGAbilityTask_MoveToTarget::Activate()
{
	if (Ability == NULL)
	{
		return;
	}

	const FGameplayAbilityActorInfo* ActorInfo = Ability->GetCurrentActorInfo();

	ARPGAIControllerBase* AIController = GetAIController();

	if (!AIController)
	{
		// If controller is not here the ability might get triggered while the character is dead
		return;
	}

	ARPGCharacterBase* Character = Cast<ARPGCharacterBase>(ActorInfo->OwnerActor.Get());

	MoveEndHandle = AIController->OnMoveEnd.AddUObject(this, &URPGAbilityTask_MoveToTarget::OnMoveEnd);
	CancelledHandle = Ability->OnGameplayAbilityCancelled.AddUObject(this, &URPGAbilityTask_MoveToTarget::OnAbilityCancelled);

	const FMoveTarget& MT = Character->GetMoveTarget();
	if (MT.Actor)
	{
		AIController->MoveToActor(MT.Actor, MT.AcceptanceRadius, true, true, false);
	}
	else
	{
		AIController->MoveToLocation(MT.Location, MT.AcceptanceRadius, true, true, false, false);
	}
}

void URPGAbilityTask_MoveToTarget::ExternalCancel()
{
	check(AbilitySystemComponent);

	OnAbilityCancelled();
	
	Super::ExternalCancel();
}

FString URPGAbilityTask_MoveToTarget::GetDebugString() const
{
	// TODO implement
	return FString::Printf(TEXT("MoveToTarget. Location: %s  "), "");
}

void URPGAbilityTask_MoveToTarget::OnDestroy(bool AbilityEnded)
{
	UnbindMoveEndDelegate();

	if (Ability)
	{
		Ability->OnGameplayAbilityCancelled.Remove(CancelledHandle);
	}

	Super::OnDestroy(AbilityEnded);

}

URPGAbilityTask_MoveToTarget* URPGAbilityTask_MoveToTarget::MoveToTarget(UGameplayAbility* OwningAbility, FName TaskInstanceName)
{
	URPGAbilityTask_MoveToTarget* Task = NewAbilityTask<URPGAbilityTask_MoveToTarget>(OwningAbility, TaskInstanceName);
	
	return Task;
}

void URPGAbilityTask_MoveToTarget::OnMoveEnd(FAIRequestID RequestID, EPathFollowingResult::Type Result)
{
	if (Result == EPathFollowingResult::Type::Success)
	{
		if (ShouldBroadcastAbilityTaskDelegates())
		{
			OnSuccess.Broadcast(FGameplayTag(), FGameplayEventData());
		}
	}
	else if (Result == EPathFollowingResult::Type::Aborted)
	{
		if (ShouldBroadcastAbilityTaskDelegates())
		{
			OnAborted.Broadcast(FGameplayTag(), FGameplayEventData());
		}
	}
	else if (Result == EPathFollowingResult::Type::Blocked)
	{
		if (ShouldBroadcastAbilityTaskDelegates())
		{
			OnBlocked.Broadcast(FGameplayTag(), FGameplayEventData());
		}
	}
	else if (Result == EPathFollowingResult::Type::OffPath)
	{
		if (ShouldBroadcastAbilityTaskDelegates())
		{
			OnOffPath.Broadcast(FGameplayTag(), FGameplayEventData());
		}
	}
	else if (Result == EPathFollowingResult::Type::Invalid)
	{
		if (ShouldBroadcastAbilityTaskDelegates())
		{
			OnInvalid.Broadcast(FGameplayTag(), FGameplayEventData());
		}
	}
}

void URPGAbilityTask_MoveToTarget::OnAbilityCancelled()
{
	OnCancelled.Broadcast(FGameplayTag(), FGameplayEventData());
}

void URPGAbilityTask_MoveToTarget::UnbindMoveEndDelegate()
{
	ARPGAIControllerBase* AIController = GetAIController();

	if (!AIController)
	{
		return;
	}

	AIController->OnMoveEnd.Remove(MoveEndHandle);
}

ARPGAIControllerBase* URPGAbilityTask_MoveToTarget::GetAIController() const
{
	const FGameplayAbilityActorInfo* ActorInfo = Ability->GetCurrentActorInfo();

	ARPGCharacterBase* Character = Cast<ARPGCharacterBase>(ActorInfo->OwnerActor.Get());
	if (!Character)
	{
		return NULL;
	}
	
	ARPGAIControllerBase* AIController = Cast<ARPGAIControllerBase>(Character->GetController());
	if (!AIController)
	{
		return NULL;
	}

	return AIController;
}
