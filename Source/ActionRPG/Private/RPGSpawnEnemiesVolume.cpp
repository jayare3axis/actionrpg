// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGSpawnEnemiesVolume.h"

#include "DrawDebugHelpers.h"

#define MAX_FIND_SPAWN_LOCATION_TRIES 100

ARPGSpawnEnemiesVolume::ARPGSpawnEnemiesVolume()
{
	
	MinDistance = 500.f;
	MaxElevation = 10.f;
	AdjustZ = 100.f;
}

void ARPGSpawnEnemiesVolume::BeginPlay()
{
	int32 Index = 0;

	if (Enemies.Num() > 1)
	{
		 Index = FMath::Rand() % Enemies.Num();
	}

	if (HasAuthority() && Enemies.Num() > 0)
	{
		SpawnEnemies(Enemies[Index]);
	}
}

void ARPGSpawnEnemiesVolume::SpawnEnemies(URPGEnemyCrowdDataAsset* Data)
{
	if (!Data || !SpawnEnemyClass)
	{
		return;
	}

	const TArray<URPGEnemyDataAsset*>& Enemies = Data->GetEnemies();

	 TArray<FVector> SpawnedLocations;

	for (int32 i = 0; i < Enemies.Num(); i++)
	{
		FVector SpawnLocation;
		FRotator SpawnRotation;
		URPGEnemyDataAsset* Enemy = Enemies[i];

		if (i == 0 && Data->HasBoss())
		{
			SpawnLocation = GetActorLocation();
			SpawnRotation = GetActorRotation();
		}
		else
		{
			if (!TryFindSpawnLocation(SpawnedLocations, SpawnLocation))
			{
				break;
			}
			
		}

		SpawnRotation = FRotator(0.f, FMath::Rand() % 360, 0.f);

		// Spawn the enemy
		FActorSpawnParameters SpawnInfo;
		SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		ARPGSpawnEnemy* SpawnEnemy = GetWorld()->SpawnActor<ARPGSpawnEnemy>(SpawnEnemyClass, SpawnLocation, SpawnRotation, SpawnInfo);
		if (SpawnEnemy)
		{
			SpawnEnemy->SpawnEnemy(Enemy);
			SpawnedLocations.Add(SpawnLocation);
		}
	}
}

bool ARPGSpawnEnemiesVolume::TryFindSpawnLocation(const TArray<FVector>& UsedLocations, FVector& SpawnLocation)
{
	for (int32 i = 0; i < MAX_FIND_SPAWN_LOCATION_TRIES; i++)
	{
		SpawnLocation = FMath::RandPointInBox(GetBounds().GetBox());
		for (const FVector Location : UsedLocations)
		{
			float Distance = FVector::Dist(SpawnLocation, Location);
			
			if (Distance <= MinDistance)
			{
				continue;
			}
		}

		// Check if geometry at the point is suitable for spawning the enemy
		if (!CanPositionSpawnEnemy(SpawnLocation))
		{
			continue;
		}

		SpawnLocation = FVector(SpawnLocation.X, SpawnLocation.Y, SpawnLocation.Z + AdjustZ);

		return true;
	}

	return false;
}

bool ARPGSpawnEnemiesVolume::CanPositionSpawnEnemy(const FVector& InPosition)
{
	// Trace from above world position to check if we can spawn at this height
	FCollisionQueryParams TraceParams = FCollisionQueryParams(FName("Trace"), true);
	FHitResult Hit;
	FVector Start = InPosition + FVector(0.f, 0.f, 4000.f);
	FVector End = InPosition + FVector(0.f, 0.f, 100.f);

	GetWorld()->LineTraceSingleByChannel(
		Hit,
		Start,
		End,
		ECC_Visibility,
		TraceParams
	);

	// Check if elevation to loot position is in valid range
	float SpawnZ = Hit.ImpactPoint.Z;
	if (Hit.bBlockingHit && FMath::Abs(SpawnZ - GetActorLocation().Z) > MaxElevation)
	{
		return false;
	}

	return true;
}
