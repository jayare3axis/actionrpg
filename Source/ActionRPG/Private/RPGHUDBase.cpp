// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGHUDBase.h"
#include "RPGBlueprintFunctionLibrary.h"
#include "RPGPlayerControllerBase.h"
#include "RPGInitManagerSubsystem.h"
#include "RPGGameInstanceBase.h"

ARPGHUDBase::ARPGHUDBase()
{
	bIsInventoryOpen = false;
	bIsCharacterSheetOpen = false;
	bIsSkillSheetOpen = false;
}

void ARPGHUDBase::BeginPlay()
{
	URPGGameInstanceBase* GameInstance = URPGBlueprintFunctionLibrary::GetGameInstance(this);
	URPGInitManagerSubsystem* InitManager = GameInstance->GetSubsystem<URPGInitManagerSubsystem>();
	check(InitManager);

	InitManager->Bind(this);
}

void ARPGHUDBase::OnCharacterCreated()
{
	BindCharacter();
}

void ARPGHUDBase::OnHUDCreated()
{
}

void ARPGHUDBase::OnReady()
{
}

void ARPGHUDBase::BindCharacter_Implementation()
{
	URPGGameInstanceBase* GameInstance = URPGBlueprintFunctionLibrary::GetGameInstance(this);
	URPGInitManagerSubsystem* InitManager = GameInstance->GetSubsystem<URPGInitManagerSubsystem>();
	check(InitManager);

	InitManager->NextStep();
}

void ARPGHUDBase::ToggleInventory()
{
	ARPGPlayerControllerBase* PC = URPGBlueprintFunctionLibrary::GetLocalPlayerController(this);
	if (PC->IsInteracting())
	{
		return;
	}

	if (!bIsInventoryOpen)
	{
		if (bIsSkillSheetOpen)
		{
			BlueprintCloseSkillSheet(true);
		}
		OpenInventory();
	}
	else
	{
		CloseInventory();
	}

	CloseSkillBindings();

	bIsInventoryOpen = !bIsInventoryOpen;
}

void ARPGHUDBase::ToggleCharacterSheet()
{
	ARPGPlayerControllerBase* PC = URPGBlueprintFunctionLibrary::GetLocalPlayerController(this);
	if (PC->IsInteracting())
	{
		return;
	}

	if (!bIsCharacterSheetOpen)
	{
		if (bIsSkillSheetOpen)
		{
			BlueprintCloseSkillSheet(true);
		}
		OpenCharacterSheet();
	}
	else
	{
		CloseCharacterSheet();
	}

	CloseSkillBindings();

	bIsCharacterSheetOpen = !bIsCharacterSheetOpen;
}

void ARPGHUDBase::ToggleSkillSheet()
{
	ARPGPlayerControllerBase* PC = URPGBlueprintFunctionLibrary::GetLocalPlayerController(this);
	if (PC->IsInteracting())
	{
		return;
	}

	if (!bIsSkillSheetOpen)
	{
		if (bIsInventoryOpen)
		{
			BlueprintCloseInventory(true);
		}
		if (bIsCharacterSheetOpen)
		{
			BlueprintCloseCharacterSheet(true);
		}
		OpenSkillSheet();
	}
	else
	{
		CloseSkillSheet();
	}

	CloseSkillBindings();

	bIsSkillSheetOpen = !bIsSkillSheetOpen;
}

void ARPGHUDBase::BlueprintOpenInventory()
{
	if (!bIsInventoryOpen)
	{
		OpenInventory();
		bIsInventoryOpen = true;
	}
}

void ARPGHUDBase::BlueprintOpenCharacterSheet()
{
	if (!bIsCharacterSheetOpen)
	{
		OpenCharacterSheet();

		bIsCharacterSheetOpen = true;
	}
}

void ARPGHUDBase::BlueprintOpenSkillSheet()
{
	if (!bIsSkillSheetOpen)
	{
		OpenSkillSheet();
		bIsSkillSheetOpen = true;
	}
}

void ARPGHUDBase::BlueprintCloseInventory(bool bDontPlaySound)
{
	if (bIsInventoryOpen)
	{
		CloseInventory(bDontPlaySound);
		bIsInventoryOpen = false;
	}
}

void ARPGHUDBase::BlueprintCloseCharacterSheet(bool bDontPlaySound)
{
	if (bIsCharacterSheetOpen)
	{
		CloseCharacterSheet(bDontPlaySound);
		bIsCharacterSheetOpen = false;
	}
}

void ARPGHUDBase::BlueprintCloseSkillSheet(bool bDontPlaySound)
{
	if (bIsSkillSheetOpen)
	{
		CloseSkillSheet(bDontPlaySound);
		bIsSkillSheetOpen = false;
	}
}

bool ARPGHUDBase::IsAnyWindowOpen() const
{
	return bIsInventoryOpen || bIsCharacterSheetOpen || bIsSkillSheetOpen;
}

void ARPGHUDBase::CloseAllWindows()
{
	// TODO call this method when player character dies
	
	CloseInventory(true);
	CloseCharacterSheet(true);
	CloseSkillSheet(true);
	CloseSkillBindings();

	bIsInventoryOpen = false;
	bIsCharacterSheetOpen = false;
	bIsSkillSheetOpen = false;
}
