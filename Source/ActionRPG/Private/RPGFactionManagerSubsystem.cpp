// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGFactionManagerSubsystem.h"
#include "RPGPlayerControllerBase.h"
#include "RPGCharacterBase.h"
#include "RPGBlueprintFunctionLibrary.h"

EHostilityRanking URPGFactionManagerSubsystem::GetHostilityRankingForLocalPlayer(AActor* Actor)
{
	ARPGPlayerControllerBase* PC = URPGBlueprintFunctionLibrary::GetLocalPlayerController(this);
	if (PC->GetPlayerCharacter() == Actor)
	{
		return EHostilityRanking::HRE_Friendly;
	}

	return GetHostilityRankingForActor(PC->GetPlayerCharacter(), Actor);
}

EHostilityRanking URPGFactionManagerSubsystem::GetHostilityRankingForActor(AActor* TargetActor, AActor* Actor)
{
	// TODO handle relations

	ARPGCharacterBase* Char1 = Cast<ARPGCharacterBase>(TargetActor);
	ARPGCharacterBase* Char2 = Cast<ARPGCharacterBase>(Actor);

	if (Char1 == NULL || Char2 == NULL)
	{
		return EHostilityRanking::HRE_Neutral;
	}

	return (Char1->IsHostile() != Char2->IsHostile()) ? EHostilityRanking::HRE_Enemy : EHostilityRanking::HRE_Neutral;
}
