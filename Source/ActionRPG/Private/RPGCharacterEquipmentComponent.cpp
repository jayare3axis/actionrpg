// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGCharacterEquipmentComponent.h"
#include "RPGBlueprintFunctionLibrary.h"

URPGCharacterEquipmentComponent::URPGCharacterEquipmentComponent()
{
	SetIsReplicated(true);
}

void URPGCharacterEquipmentComponent::EquipItem(const FRPGItemSlot& Slot, const FRPGItemInstance& ItemInstance)
{
	Items.Add(Slot, ItemInstance);
	EquipItemImplementation(Slot, ItemInstance.Item);

	ReceiveItemEquipped.Broadcast(Slot, ItemInstance);
}

FRPGItemInstance URPGCharacterEquipmentComponent::UnequipItem(const FRPGItemSlot& Slot)
{	
	if (!Items.Contains(Slot))
	{
		return FRPGItemInstance();
	}

	FRPGItemInstance ItemInstance = Items[Slot];

	UnequipItemImplementation(Slot, ItemInstance.Item);
	Items.Remove(Slot);

	ReceiveItemUnequipped.Broadcast(Slot);

	return ItemInstance;
}

void URPGCharacterEquipmentComponent::OnRep_Equipment()
{
	TArray<FRPGItemAtSlot> AddedEquipment;
	TArray<FRPGItemAtSlot> RemovedEquipment;

	// Added equipment init
	for (FRPGItemAtSlot ItemAtSlot : Equipment)
	{
		if (!PrevEquipment.Contains(ItemAtSlot))
		{
			AddedEquipment.Add(ItemAtSlot);
		}
	}

	// Removed equipment init
	for (FRPGItemAtSlot ItemAtSlot : PrevEquipment)
	{
		if (!Equipment.Contains(ItemAtSlot))
		{
			RemovedEquipment.Add(ItemAtSlot);
		}
	}

	PrevEquipment.Empty();
	for (FRPGItemAtSlot ItemAtSlot : Equipment)
	{
		PrevEquipment.Add(ItemAtSlot);
	}

	// Added equipment
	for (FRPGItemAtSlot ItemAtSlot : AddedEquipment)
	{
		FRPGItemInstance ItemInstance = URPGBlueprintFunctionLibrary::CreateItemInstance(this, ItemAtSlot.Item, 1, ItemAtSlot.Level); // Currently we allow only 1 item at a slot
		EquipItem(ItemAtSlot.Slot, ItemInstance);
	}

	// Removed equipment
	for (FRPGItemAtSlot ItemAtSlot : RemovedEquipment)
	{
		UnequipItem(ItemAtSlot.Slot);
	}
}

void URPGCharacterEquipmentComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(URPGCharacterEquipmentComponent, Equipment);
}
