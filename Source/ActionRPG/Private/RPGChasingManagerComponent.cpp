// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGChasingManagerComponent.h"
#include "RPGCharacterBase.h"

#include "DrawDebugHelpers.h"

URPGChasingManagerComponent::URPGChasingManagerComponent()
{
	// Distance needed so the aggro chases Player itself instead of chasing target
	ChaseCharInsteadTreshold = 200.f;
}

void URPGChasingManagerComponent::Process()
{
	if (Targets.Num() == 0)
	{
		return;
	}

	// Reset results
	Results.Empty();
	for (ARPGCharacterChasingTarget* Target : Targets)
	{
		Target->ResetAggros();
	}

	for (ARPGCharacterBase* Aggro : Aggros)
	{
		ProcessAggro(Aggro);
	}
}

void URPGChasingManagerComponent::ReceivedByAggro(ARPGCharacterBase* Aggro)
{
	Aggros.Add(Aggro);
}

void URPGChasingManagerComponent::LostByAggro(ARPGCharacterBase* Aggro)
{
	Aggros.Remove(Aggro);
}

void URPGChasingManagerComponent::RegisterChasingTarget(ARPGCharacterChasingTarget* ChasingTarget)
{
	if (!GetOwner()->HasAuthority())
	{
		return;
	}

	check(ChasingTarget);

	Targets.Add(ChasingTarget);
}

AActor* URPGChasingManagerComponent::GetChasingTargetForAggro(ARPGCharacterBase* Aggro)
{
	if (Targets.Num() == 0 || !Results.Contains(Aggro))
	{
		return NULL;
	}

	return Results[Aggro];
}

void URPGChasingManagerComponent::ProcessAggro(ARPGCharacterBase* Aggro)
{
	// If aggro is close enough - target player
	float DistanceToPlayer = FMath::Abs(FVector::Distance(GetOwner()->GetActorLocation(), Aggro->GetActorLocation()));
	if (DistanceToPlayer <= ChaseCharInsteadTreshold)
	{
		Results.Add(Aggro, GetOwner());
		return;
	}

	float ShortestDistance = 10000.f;
	int32 MinimalCount = 10000;
	
	ARPGCharacterChasingTarget* Result = NULL;
	float Distance = 0.f;
	int32 AggroCount = 0;

	for (ARPGCharacterChasingTarget* Target : Targets)
	{
		Distance = FMath::Abs( FVector::Distance(Target->GetActorLocation(), Aggro->GetActorLocation()) );
		AggroCount = Target->GetAggroCount();

		if (AggroCount < MinimalCount)
		{
			Result = Target;
			MinimalCount = AggroCount;
		}
		else if (AggroCount == MinimalCount && Distance < ShortestDistance)
		{
			Result = Target;
			ShortestDistance = Distance;
		}
	}

	Result->AssignAggro(Aggro);
	Results.Add(Aggro, Result);

	//DrawDebugLine(GetWorld(), Aggro->GetActorLocation(), Result->GetActorLocation(), FColor::Red);
}
