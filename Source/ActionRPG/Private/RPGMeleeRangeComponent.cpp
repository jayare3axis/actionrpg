// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGMeleeRangeComponent.h"
#include "Engine/LocalPlayer.h"
#include "ActionRPG.h"
#include "RPGCharacterBase.h"
#include "RPGPlayerControllerBase.h"
#include "RPGFactionManagerSubsystem.h"
#include "RPGBlueprintFunctionLibrary.h"

URPGMeleeRangeComponent::URPGMeleeRangeComponent()
{
}

void URPGMeleeRangeComponent::TargetRecieved(AActor* Actor)
{
	// Check if we are not colliding with self
	if (GetOwner() == Actor)
	{
		return;
	}

	IRPGInteractableInterface* ITarget = Cast<IRPGInteractableInterface>(Actor);

	// Check if target is hostile
	ARPGPlayerControllerBase* PC = URPGBlueprintFunctionLibrary::GetLocalPlayerController(this);
	ULocalPlayer* LocalPlayer = PC->GetLocalPlayer();
	URPGFactionManagerSubsystem* FactionManager = LocalPlayer->GetSubsystem<URPGFactionManagerSubsystem>();
	if (FactionManager->GetHostilityRankingForActor(GetOwner(), Actor) != EHostilityRanking::HRE_Enemy &&
		!(ITarget && ITarget->ShouldAttackOnInteraction(URPGBlueprintFunctionLibrary::GetLocalPlayerController(this))))
	{
		return;
	}

	if (!RecievedTargets.Contains(Actor))
	{
		RecievedTargets.Add(Actor);
	}
}

void URPGMeleeRangeComponent::TargetLost(AActor* Actor)
{
	if (RecievedTargets.Contains(Actor))
	{
		RecievedTargets.Remove(Actor);
	}
}

AActor* URPGMeleeRangeComponent::ChooseValidActorFromRecievedTargets()
{
	AActor* Result = NULL;

	if (RecievedTargets.Num() == 0)
	{
		// No target
		return Result;
	}

	float ClosestYaw = 10000.f;

	// Choose closest target from the player
	for (AActor* Target : RecievedTargets)
	{
		// Check if target is still alive
		ARPGCharacterBase* TargetCharacter = Cast<ARPGCharacterBase>(Target);
		if (TargetCharacter && (TargetCharacter->GetCombatComponent() && !TargetCharacter->GetCombatComponent()->IsAlive()))
		{
			continue;
		}

		float YawToTarget = FMath::Abs(URPGBlueprintFunctionLibrary::GetTurnToTargetYaw(GetWorld(), GetOwner(), Target));

		if (YawToTarget < ClosestYaw)
		{
			Result = Target;
			ClosestYaw = YawToTarget;
		}		
	}

	return Result;
}
