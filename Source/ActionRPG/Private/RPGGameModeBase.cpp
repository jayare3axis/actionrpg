// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGGameModeBase.h"
#include "ActionRPG.h"
#include "RPGPlayerProxyBase.h"
#include "RPGPlayerStateBase.h"
#include "RPGGameStateBase.h"
#include "RPGPlayerControllerBase.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "RPGHUDBase.h"

ARPGGameModeBase::ARPGGameModeBase()
{
	DefaultPawnClass = ARPGPlayerProxyBase::StaticClass();
	PlayerStateClass = ARPGPlayerStateBase::StaticClass();
	GameStateClass = ARPGGameStateBase::StaticClass();
	PlayerControllerClass = ARPGPlayerControllerBase::StaticClass();
	HUDClass = ARPGHUDBase::StaticClass();
}
