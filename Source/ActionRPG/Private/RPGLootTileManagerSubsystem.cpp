// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGLootTileManagerSubsystem.h"

URPGLootTileManagerSubsystem::URPGLootTileManagerSubsystem()
{
	TileSize = 100.f;
	MaxElevation = 50.f;
}

bool URPGLootTileManagerSubsystem::TryGetLootPosition(FVector BasePosition, int32 MaxDistance, FVector& Result)
{
	float CurDistance = 1.f;
	Result = FVector::ZeroVector;

	TTuple<int32, int32> BaseTile;
	WorldPositionToTile(BasePosition, BaseTile);

	while (CurDistance <= MaxDistance)
	{
		TArray<FVector> PossibleLootSpawnLocations;

		for (int32 Y = -CurDistance; Y <= CurDistance; Y++)
		{
			for (int32 X = -CurDistance; X <= CurDistance; X++)
			{
				// Skip center tile
				if (X == 0 && Y == 0)
				{
					continue;
				}

				TTuple<int32, int32> TargetTile = TTuple<int32, int32>(BaseTile.Key + X, BaseTile.Value + Y);
				FVector TileWorldPosition;
				TileToWorldPosition(TargetTile, TileWorldPosition);

				float SpawnZ;
				if (CanWorldPositionSpawnLoot(TileWorldPosition, BasePosition.Z, SpawnZ))
				{
					float DeviationX = FMath::RandRange(-TileSize / 3, TileSize / 3);
					float DeviationY = FMath::RandRange(-TileSize / 3, TileSize / 3);

					PossibleLootSpawnLocations.Add(
						FVector(BasePosition.X + X * TileSize + DeviationX, BasePosition.Y + Y * TileSize + DeviationY, SpawnZ)
					);
				}
			}
		}

		if (PossibleLootSpawnLocations.Num() > 0)
		{
			int32 Index = FMath::Rand() % PossibleLootSpawnLocations.Num();
			Result = PossibleLootSpawnLocations[Index];

			TTuple<int32, int32> ResultTile;
			WorldPositionToTile(Result, ResultTile);
			OccupiedTiles.Add(ResultTile, true);

			return true;
		}

		CurDistance++;
	}

	return false;
}

void URPGLootTileManagerSubsystem::WorldPositionToTile(FVector WorldPosition, TTuple<int32, int32>& Result)
{
	int32 X = WorldPosition.X / TileSize;
	int32 Y = WorldPosition.Y / TileSize;

	Result = TTuple<int32, int32>(X, Y);
}

void URPGLootTileManagerSubsystem::TileToWorldPosition(const TTuple<int32, int32>& Tile, FVector& Result)
{
	float X = Tile.Key * TileSize;
	float Y = Tile.Value * TileSize;

	Result = FVector(X, Y, -2000.f);
}

bool URPGLootTileManagerSubsystem::CanWorldPositionSpawnLoot(const FVector& WorldPosition, float WorldZ, float& SpawnZ)
{
	SpawnZ = 0.f;

	// Check if tile is occupied
	TTuple<int32, int32> WorldTile;
	WorldPositionToTile(WorldPosition, WorldTile);
	if (OccupiedTiles.Contains(WorldTile))
	{
		return false;
	}

	// Trace from above world position to check if we can spawn at this height
	FCollisionQueryParams TraceParams = FCollisionQueryParams(FName("Trace"), true);
	FHitResult Hit;
	FVector Start = WorldPosition + FVector(0.f, 0.f, 4000.f);

	GetWorld()->LineTraceSingleByChannel(
		Hit,
		Start,
		WorldPosition,
		ECC_Visibility,
		TraceParams
	);

	// Check if elevation to loot position is in valid range
	SpawnZ = Hit.ImpactPoint.Z;
	if (FMath::Abs(SpawnZ - WorldZ) > MaxElevation)
	{
		return false;
	}

	return true;
}
