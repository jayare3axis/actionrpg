// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGPartAssetSocketedActor.h"
#include "RPGCharacterBase.h"

void URPGPartAssetSocketedActor::ApplyInternal(USkeletalMeshComponent* _SkeletalMesh)
{
	ARPGCharacterBase* Character = Cast<ARPGCharacterBase>(_SkeletalMesh->GetOwner());
	if (Character)
	{
		Character->SetSocketedActor(ActorClass, Rotation, SocketName);
	}
	else
	{
		UE_LOG(LogActionRPG, Warning, TEXT("Character not present or has invalid type!"));
	}
}
