// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGSkillSetDataAsset.h"
#include "RPGSkillTierDataAsset.h"

const FPrimaryAssetType	URPGSkillSetDataAsset::PrimaryAssetType = TEXT("SkillSet");

FString URPGSkillSetDataAsset::GetIdentifierString() const
{
	return GetPrimaryAssetId().ToString();
}

FPrimaryAssetId URPGSkillSetDataAsset::GetPrimaryAssetId() const
{
	// This is a DataAsset and not a blueprint so we can just use the raw FName
	// For blueprints it's needed to handle stripping the _C suffix
	return FPrimaryAssetId(URPGSkillSetDataAsset::PrimaryAssetType, GetFName());
}
