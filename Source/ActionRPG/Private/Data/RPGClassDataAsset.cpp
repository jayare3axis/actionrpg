// Fill out your copyright notice in the Description page of Project Settings.

#include "Data/RPGClassDataAsset.h"

const FPrimaryAssetType	URPGClassDataAsset::PrimaryAssetType = TEXT("Class");

FString URPGClassDataAsset::GetIdentifierString() const
{
	return GetPrimaryAssetId().ToString();
}

FPrimaryAssetId URPGClassDataAsset::GetPrimaryAssetId() const
{
	// This is a DataAsset and not a blueprint so we can just use the raw FName
	// For blueprints it's needed to handle stripping the _C suffix
	return FPrimaryAssetId(URPGClassDataAsset::PrimaryAssetType, GetFName());
}
