// Fill out your copyright notice in the Description page of Project Settings.

#include "Data/RPGRaceDataAsset.h"

const FPrimaryAssetType	URPGRaceDataAsset::PrimaryAssetType = TEXT("Race");

FString URPGRaceDataAsset::GetIdentifierString() const
{
	return GetPrimaryAssetId().ToString();
}

FPrimaryAssetId URPGRaceDataAsset::GetPrimaryAssetId() const
{
	// This is a DataAsset and not a blueprint so we can just use the raw FName
	// For blueprints it's needed to handle stripping the _C suffix
	return FPrimaryAssetId(URPGRaceDataAsset::PrimaryAssetType, GetFName());
}
