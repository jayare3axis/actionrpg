// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGPartAssetBase.h"

void URPGPartAssetBase::BlueprintApply(USkeletalMeshComponent* SkeletalMesh)
{
	Apply(SkeletalMesh);
}

void URPGPartAssetBase::Apply_Implementation(USkeletalMeshComponent* SkeletalMesh)
{
	ApplyInternal(SkeletalMesh);
}

void URPGPartAssetBase::ApplyInternal(USkeletalMeshComponent* SkeletalMesh)
{
}
