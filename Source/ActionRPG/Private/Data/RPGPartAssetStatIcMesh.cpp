// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGPartAssetStatIcMesh.h"

void URPGPartAssetStatIcMesh::ApplyInternal(USkeletalMeshComponent* _SkeletalMesh)
{
	if (!StaticMeshTag.IsNone())
	{
		const TArray<UActorComponent*>& Components = _SkeletalMesh->GetOwner()->GetComponentsByTag(UPrimitiveComponent::StaticClass(), StaticMeshTag);
		if (Components.Num() != 0)
		{
			UStaticMeshComponent* TargetMesh = Cast<UStaticMeshComponent>(Components[0]);
			TargetMesh->SetStaticMesh(StaticMesh);
		}
		else
		{
			UE_LOG(LogActionRPG, Warning, TEXT("Static mesh part not found!"));
		}
	}
}
