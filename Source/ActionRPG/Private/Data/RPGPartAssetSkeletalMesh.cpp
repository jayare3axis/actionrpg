// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGPartAssetSkeletalMesh.h"

void URPGPartAssetSkeletalMesh::ApplyInternal(USkeletalMeshComponent* _SkeletalMesh)
{
	USkeletalMeshComponent* TargetMesh = _SkeletalMesh;

	if (!SkeletalMeshTag.IsNone())
	{
		const TArray<UActorComponent*>& Components = _SkeletalMesh->GetOwner()->GetComponentsByTag(UPrimitiveComponent::StaticClass(), SkeletalMeshTag);
		if (Components.Num() != 0)
		{
			TargetMesh = Cast<USkeletalMeshComponent>(Components[0]);
			TargetMesh->SetMasterPoseComponent(_SkeletalMesh);
		}
		else
		{
			UE_LOG(LogActionRPG, Warning, TEXT("Skeletal mesh part not found!"));
		}
	}

	TargetMesh->SetSkeletalMesh(SkeletalMesh);
}
