// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGSkillTierDataAsset.h"

const FPrimaryAssetType	URPGSkillTierDataAsset::PrimaryAssetType = TEXT("SkillTier");

FString URPGSkillTierDataAsset::GetIdentifierString() const
{
	return GetPrimaryAssetId().ToString();
}

FPrimaryAssetId URPGSkillTierDataAsset::GetPrimaryAssetId() const
{
	// This is a DataAsset and not a blueprint so we can just use the raw FName
	// For blueprints it's needed to handle stripping the _C suffix
	return FPrimaryAssetId(URPGSkillTierDataAsset::PrimaryAssetType, GetFName());
}
