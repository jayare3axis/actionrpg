// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGPlayerControllerBase.h"
#include "Engine/LocalPlayer.h"
#include "Runtime/AIModule/Classes/Blueprint/AIBlueprintHelperLibrary.h"
#include "RPGPlayerProxyBase.h"
#include "RPGCharacterBase.h"
#include "RPGHUDBase.h"
#include "RPGGameStateBase.h"
#include "RPGBlueprintFunctionLibrary.h"
#include "GameplayTagsModule.h"
#include "RPGTopDownCameraComponent.h"
#include "Engine/LocalPlayer.h"
#include "GameFramework/HUD.h"
#include "RPGInteractableInterface.h"
#include "RPGHighlightableInterface.h"
#include "Components/SkeletalMeshComponent.h"
#include "Items/RPGItem.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "RPGItemContainerComponent.h"
#include "RPGLootBase.h"
#include "RPGSaveGame.h"
#include "RPGPlayerActionComponent.h"
#include "RPGGameInstanceBase.h"
#include "RPGCharacterEquipmentComponent.h"
#include "RPGFactionManagerSubsystem.h"
#include "RPGInitManagerSubsystem.h"
#include "DrawDebugHelpers.h"

ARPGPlayerControllerBase::ARPGPlayerControllerBase()
{
	bShowMouseCursor = true;
	bEnableClickEvents = true;
	bEnableMouseOverEvents = true;
	bAutoManageActiveCameraTarget = false;
	PrimaryActorTick.bCanEverTick = true;

	bIsBusy = false;
	CurrentActionButton = EPlayerActionButton::ABE_None;
	LastActionButton = EPlayerActionButton::ABE_None;
	LastCursorActor = NULL;
	bIsLockAttackOn = false;
	
	HighlightedActor = NULL;
	CurrentInteractable = NULL;

	// Setup components
	BackpackItemContainer = CreateDefaultSubobject<URPGItemContainerComponent>(TEXT("Backpack"));
	ActionComponent = CreateDefaultSubobject<URPGPlayerActionComponent>(TEXT("Action"));
	ActionComponent->MinInteractionDistance = 100.f;
}

void ARPGPlayerControllerBase::BeginPlay()
{
	Super::BeginPlay();

	BackpackItemContainer->SetSize(12, 5);

	// Spawn move dummy for that controller
	if (HasAuthority())
	{
		FActorSpawnParameters SpawnInfo;
		SpawnInfo.Owner = this;
		MoveDummy = GetWorld()->SpawnActor<ARPGPlayerMoveDummy>(MoveDummyClass, FVector::ZeroVector, FRotator::ZeroRotator, SpawnInfo);
	}
}

void ARPGPlayerControllerBase::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAction("PrimaryAction", IE_Pressed, this, &ARPGPlayerControllerBase::SetPrimaryActionOn);
	InputComponent->BindAction("PrimaryAction", IE_Released, this, &ARPGPlayerControllerBase::SetActionOff);

	InputComponent->BindAction("SecondaryAction", IE_Pressed, this, &ARPGPlayerControllerBase::SetSecondaryActionOn);
	InputComponent->BindAction("SecondaryAction", IE_Released, this, &ARPGPlayerControllerBase::SetActionOff);

	InputComponent->BindAction("Key1Action", IE_Pressed, this, &ARPGPlayerControllerBase::SetKey1ActionOn);
	InputComponent->BindAction("Key1Action", IE_Released, this, &ARPGPlayerControllerBase::SetActionOff);

	InputComponent->BindAction("Key2Action", IE_Pressed, this, &ARPGPlayerControllerBase::SetKey2ActionOn);
	InputComponent->BindAction("Key2Action", IE_Released, this, &ARPGPlayerControllerBase::SetActionOff);

	InputComponent->BindAction("Key3Action", IE_Pressed, this, &ARPGPlayerControllerBase::SetKey3ActionOn);
	InputComponent->BindAction("Key3Action", IE_Released, this, &ARPGPlayerControllerBase::SetActionOff);

	InputComponent->BindAction("Key4Action", IE_Pressed, this, &ARPGPlayerControllerBase::SetKey4ActionOn);
	InputComponent->BindAction("Key4Action", IE_Released, this, &ARPGPlayerControllerBase::SetActionOff);

	InputComponent->BindAction("ForceAttack", IE_Pressed, this, &ARPGPlayerControllerBase::SetLockAttackOn);
	InputComponent->BindAction("ForceAttack", IE_Released, this, &ARPGPlayerControllerBase::SetLockAttackOff);

	InputComponent->BindAxis("Zoom", this, &ARPGPlayerControllerBase::SetCameraZoom);

	InputComponent->BindAction("Inventory", IE_Pressed, this, &ARPGPlayerControllerBase::ToggleInventory);
	InputComponent->BindAction("CharacterSheet", IE_Pressed, this, &ARPGPlayerControllerBase::ToggleCharacterSheet);
	InputComponent->BindAction("SkillSheet", IE_Pressed, this, &ARPGPlayerControllerBase::ToggleSkillSheet);

	//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Some debug message!"));
}

void ARPGPlayerControllerBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	// See what is under cursor
	FHitResult HitUnderCursor;
	bool IsHit = GetHitResultUnderCursorByChannelIgnoreSelf(ECC_Interactable, true, HitUnderCursor);
	AActor* CursorActor = HitUnderCursor.Actor.Get();
	IRPGInteractableInterface* CursorActorAsInteractable = Cast<IRPGInteractableInterface>(CursorActor);
	if (!CursorActorAsInteractable)
	{
		CursorActor = NULL;
	}

	// Dynamic transparency
	ProcessDynamicTransparencyQuery();

	// Highlighting of actors under cursor
	IRPGHighlightableInterface* ITarget = Cast<IRPGHighlightableInterface>(HitUnderCursor.Actor);
	if (ITarget != Cast<IRPGHighlightableInterface>(HighlightedActor))
	{
		if (HighlightedActor != NULL)
		{
			SetHighlightOff();
		}
		if (ITarget && ITarget->CanBeHighlighted())
		{
			SetHighlightOn(HitUnderCursor.Actor.Get());
		}
	}

	// Needed for combo evaluation
	if ((CurrentActionButton != LastActionButton) && (CurrentActionButton == EPlayerActionButton::ABE_None))
	{
		bIsBusy = false;
	}

	if (!IsPaused())
	{
		if (!CurrentInteractable && CurrentActionButton == EPlayerActionButton::ABE_Primary && !CursorActor)
		{
			ServerUpdateMoveDummyLocation(HitUnderCursor.Location);
		}

		// Process action
		if (IsHit && CurrentActionButton != EPlayerActionButton::ABE_None)
		{
			ARPGCharacterBase* Char = GetPlayerCharacter();

			// Do not process if we are dead
			if ((Char->GetCombatComponent() && !Char->GetCombatComponent()->IsAlive()))
			{
				return;
			}

			// If we are holding an item - drop it on the ground
			if (GrabbedItem.Item && Char && Char->GetCombatComponent()->IsAlive() && CurrentActionButton == EPlayerActionButton::ABE_Primary)
			{
				DropItemInstance();
			}
			else
			{
				// Lock attack or just use skill
				if (bIsLockAttackOn)
				{
					if (!bIsBusy)
					{
						bIsBusy = true;
						ClientStopInteraction();

						FRPGItemSlot ItemSlot;
						GetItemSlotByCurrentActionButton(ItemSlot);
						ServerTurnToTargetAndExecuteItemSlot(HitUnderCursor.Location, ItemSlot);
					}
				}
				// Execute action on actor
				else if (CursorActor)
				{
					if (!bIsBusy)
					{
						bIsBusy = true;

						IRPGInteractableInterface* I = Cast<IRPGInteractableInterface>(CursorActor);
						if (I->CanBeInteract() && !I->ShouldAttackOnInteraction(this))
						{
							if (CurrentInteractable == NULL)
							{
								ClientStopInteraction();

								FRPGItemSlot ItemSlot;
								GetItemSlotByCurrentActionButton(ItemSlot);
								ServerExecuteActionOnInteractable(CursorActor, CursorActor->GetActorLocation(), CurrentActionButton, ItemSlot);
							}
						}
						else
						{
							ClientStopInteraction();

							FRPGItemSlot ItemSlot;
							GetItemSlotByCurrentActionButton(ItemSlot);
							ServerExecuteActionOnActor(CursorActor, CurrentActionButton, ItemSlot);
						}						
					}
				}
				// Move to target location
				else if (!CurrentInteractable && CurrentActionButton == EPlayerActionButton::ABE_Primary)
				{
					if (!bIsBusy)
					{
						bIsBusy = true;

						ClientStopInteraction();
						ServerMoveToDestination();
					}					
				}
			}
		}
	}

	LastActionButton = CurrentActionButton;
	LastCursorActor = CursorActor;
}

void ARPGPlayerControllerBase::Destroyed()
{
	if (ActionComponent)
	{
		ActionComponent->CleanUp();
	}

	Super::Destroyed();
}

bool ARPGPlayerControllerBase::ServerSpawnPlayerCharacterUsingSaveGameData_Validate(const FString& CharacterName, FName Race, const TArray<int32>& AppearanceOptions, const TArray<FRPGItemAtSlotRaw>& Items)
{
	return true;
}

void ARPGPlayerControllerBase::ServerSpawnPlayerCharacterUsingSaveGameData_Implementation(const FString& CharacterName, FName Race, const TArray<int32>& AppearanceOptions, const TArray<FRPGItemAtSlotRaw>& Items)
{
	URPGGameInstanceBase* GameInstance = URPGBlueprintFunctionLibrary::GetGameInstance(this);
	TArray<FRPGItemAtSlot> NoItems; // We  will add items to spawned character different way
	ARPGPlayerProxyBase* Proxy = Cast<ARPGPlayerProxyBase>(GetPawn());

	ARPGCharacterBase* PlayerCharacter = GameInstance->SpawnPlayerCharacterUsingSaveGameData(CharacterName, Race, NoItems, Proxy->GetActorLocation(), Proxy->GetActorRotation(), Proxy, Proxy);
	Proxy->PlayerCharacter = PlayerCharacter;
	Proxy->CheckSetCameraViewTarget();

	if (URPGBlueprintFunctionLibrary::GetLocalPlayerController(this) == this)
	{
		URPGInitManagerSubsystem* InitManager = GameInstance->GetSubsystem<URPGInitManagerSubsystem>();
		check(InitManager);
		InitManager->NextStep();
	}

	PlayerCharacter->GetCombatComponent()->InitSlottedAbilitiesSourceAsController(this);
	PlayerCharacter->GetPlayerAppearanceComponent()->InitAppearanceOptions(Race, AppearanceOptions);
	
	URPGAssetManager& AssetManager = URPGAssetManager::Get();
	for (const FRPGItemAtSlotRaw& ItemAtSlot : Items)
	{
		URPGItem* LoadedItem = AssetManager.ForceLoadItem(ItemAtSlot.Item);
		FRPGItemInstance ItemInstance = URPGBlueprintFunctionLibrary::CreateItemInstance(this, LoadedItem, 1, ItemAtSlot.Level); // Currently we allow equipping only 1 item at a slot
		ServerAddItem(ItemAtSlot.ItemSlot, ItemInstance, false);
	}

	NotifySlottedAbilitiesLoaded();

	BlueprintOnPlayerCharacterSpawned(PlayerCharacter, CharacterName, Race);
}

void ARPGPlayerControllerBase::ClientInitActionBindingsUsingSaveGameData_Implementation(const TMap<EPlayerActionButton, FRPGItemSlot>& ActionBindings)
{
	for (TPair<EPlayerActionButton, FRPGItemSlot> Pair : ActionBindings)
	{
		AddActionBinding(Pair.Key, Pair.Value);
	}
}

void ARPGPlayerControllerBase::ClientInitBackPackItemsBySaveGameData_Implementation(const TArray<FRPGItemInContainerRaw>& BackPackItems)
{
	BackpackItemContainer->LoadFromData(BackPackItems);
}

bool ARPGPlayerControllerBase::ServerInitAttributesBySaveGameData_Validate(float Level, float Experience, float AvailableSkillPoints, float Health, float Mana)
{
	return true;
}

void ARPGPlayerControllerBase::ServerInitAttributesBySaveGameData_Implementation(float Level, float Experience, float AvailableSkillPoints, float Health, float Mana)
{
	ARPGCharacterBase* Char = GetPlayerCharacter();
	if (!Char)
	{
		UE_LOG(LogActionRPG, Error, TEXT("Player character not present!"));
		return;
	}

	Char->GetCombatComponent()->InitAttributesBySaveGameData(Level, Experience, AvailableSkillPoints, Health, Mana);
}

void ARPGPlayerControllerBase::BlueprintServerAddItem(const FRPGItemSlot& ItemSlot, const FRPGItemInstance& ItemInstance, bool bNotify)
{
	ServerAddItem(ItemSlot, ItemInstance);
}

bool ARPGPlayerControllerBase::ServerAddItem_Validate(const FRPGItemSlot& ItemSlot, const FRPGItemInstance& ItemInstance, bool bNotify)
{
	return true;
}

void ARPGPlayerControllerBase::ServerAddItem_Implementation(const FRPGItemSlot& ItemSlot, const FRPGItemInstance& ItemInstance, bool bNotify)
{
	URPGItem* Item = ItemInstance.Item;
	if (!Item)
	{
		UE_LOG(LogActionRPG, Warning, TEXT("Add Item: Item not present!"));
		return;
	}

	SlottedItems.Add(ItemSlot, FRPGItemAtSlot(ItemSlot, Item, ItemInstance.Level));

	if (bNotify)
	{
		NotifySlottedItemChanged(ItemSlot, Item);		
	}

	RebuildAssignableSkills();

	if (URPGBlueprintFunctionLibrary::IsEquipableSlot(ItemSlot))
	{
		ARPGCharacterBase* PlayerCharacter = GetPlayerCharacter();
		URPGCharacterEquipmentComponent* EquipmentComponent = PlayerCharacter->GetCharacterEquipmentComponent();
		EquipmentComponent->Equipment.Add(FRPGItemAtSlot(ItemSlot, Item, ItemInstance.Level));
		EquipmentComponent->EquipItem(ItemSlot, ItemInstance);
	}

	ClientOnItemAdded(ItemSlot, ItemInstance);
}

void ARPGPlayerControllerBase::ClientOnItemAdded_Implementation(const FRPGItemSlot& ItemSlot, const FRPGItemInstance& ItemInstance)
{
	URPGGameInstanceBase* GameInstance = URPGBlueprintFunctionLibrary::GetGameInstance(this);
	GameInstance->OnPlayerItemAdded(ItemSlot, ItemInstance);

	SlottedItems.Add(ItemSlot, FRPGItemAtSlot(ItemSlot, ItemInstance.Item, ItemInstance.Level));
	ReceiveKnownSkillsChanged.Broadcast();
}

void ARPGPlayerControllerBase::BlueprintServerRemoveItem(const FRPGItemSlot& ItemSlot, bool bNotify)
{
	ServerRemoveItem(ItemSlot, bNotify);
}

bool ARPGPlayerControllerBase::ServerRemoveItem_Validate(const FRPGItemSlot& ItemSlot, bool bNotify)
{
	return true;
}

void ARPGPlayerControllerBase::ServerRemoveItem_Implementation(const FRPGItemSlot& ItemSlot, bool bNotify)
{
	if (!SlottedItems.Contains(ItemSlot))
	{
		return;
	}

	const FRPGItemAtSlot& ItemAtSlot = SlottedItems[ItemSlot];

	SlottedItems.Remove(ItemSlot);

	if (bNotify)
	{
		NotifySlottedItemChanged(ItemSlot, ItemAtSlot.Item);
		RebuildAssignableSkills();
	}

	if (URPGBlueprintFunctionLibrary::IsEquipableSlot(ItemSlot))
	{
		ARPGCharacterBase* PlayerCharacter = GetPlayerCharacter();
		URPGCharacterEquipmentComponent* EquipmentComponent = PlayerCharacter->GetCharacterEquipmentComponent();

		int32 RemoveIndex = -1;
		for (int32 i = 0; i < EquipmentComponent->Equipment.Num(); i++)
		{
			const FRPGItemAtSlot& EquipmentItem = EquipmentComponent->Equipment[i];
			if (EquipmentItem.Slot == ItemSlot)
			{
				RemoveIndex = i;
				break;
			}
		}

		if (RemoveIndex > -1)
		{
			EquipmentComponent->Equipment.RemoveAt(RemoveIndex);
		}		

		EquipmentComponent->UnequipItem(ItemSlot);
	}

	ClientOnItemRemoved(ItemSlot);
}

void ARPGPlayerControllerBase::ClientOnItemRemoved_Implementation(const FRPGItemSlot& ItemSlot)
{
	URPGGameInstanceBase* GameInstance = URPGBlueprintFunctionLibrary::GetGameInstance(this);
	GameInstance->OnPlayerItemRemoved(ItemSlot);

	SlottedItems.Remove(ItemSlot);
	ReceiveKnownSkillsChanged.Broadcast();
}

void ARPGPlayerControllerBase::BlueprintServerAcquireSkill(const FRPGItemInstance& ItemInstance)
{
	ServerAcquireSkill(ItemInstance);
}

bool ARPGPlayerControllerBase::ServerAcquireSkill_Validate(const FRPGItemInstance& ItemInstance)
{
	return true;
}

void ARPGPlayerControllerBase::ServerAcquireSkill_Implementation(const FRPGItemInstance& ItemInstance)
{
	ARPGCharacterBase* PlayerCharacter = GetPlayerCharacter();
	check(PlayerCharacter);

	// Find free slot for new skill
	int32 LearnedSkillsCount = 0;
	for (TPair<FRPGItemSlot, FRPGItemAtSlot> Pair : SlottedItems)
	{
		if (Pair.Key.ItemType == URPGAssetManager::SkillItemType)
		{
			LearnedSkillsCount++;
		}
	}

	// Add skill
	FRPGItemSlot ItemSlot = FRPGItemSlot(URPGAssetManager::SkillItemType, LearnedSkillsCount);
	ServerAddItem(ItemSlot, ItemInstance);

	// Decrease available skill points count
	PlayerCharacter->GetCombatComponent()->OnNewSkillAcquired();
}

bool ARPGPlayerControllerBase::HasItem(URPGItem* _Item)
{
	for (TPair<FRPGItemSlot, FRPGItemAtSlot> Pair : SlottedItems)
	{
		if (Pair.Value.Item == _Item)
		{
			return true;
		}
	}

	return false;
}

ARPGCharacterBase* ARPGPlayerControllerBase::GetPlayerCharacter() const
{
	ARPGPlayerProxyBase* Proxy = Cast<ARPGPlayerProxyBase>(GetPawn());
	if (!Proxy)
	{
		return NULL;
	}

	ARPGCharacterBase* Char = Cast<ARPGCharacterBase>(Proxy->PlayerCharacter);

	return Char;
}

void ARPGPlayerControllerBase::BlueprintServerLevelUp()
{
	ServerLevelUp();
}

bool ARPGPlayerControllerBase::ServerLevelUp_Validate()
{
	return true;
}

void ARPGPlayerControllerBase::ServerLevelUp_Implementation()
{
	ARPGCharacterBase* Char = GetPlayerCharacter();
	check(Char);

	Char->GetCombatComponent()->ForceLevelUp();
}

void ARPGPlayerControllerBase::GetItemSlotByCurrentActionButton(FRPGItemSlot& ItemSlot)
{
	ARPGCharacterBase* Char = GetPlayerCharacter();
	check(Char);

	ItemSlot = FRPGItemSlot(URPGAssetManager::WeaponItemType, 0);

	if (CurrentActionButton != EPlayerActionButton::ABE_None && ActionBindings.Contains(CurrentActionButton))
	{		
		ItemSlot = ActionBindings[CurrentActionButton];
	}
}

bool ARPGPlayerControllerBase::CanCarryItem(FRPGItemInstance ItemInstance)
{
	FItemContainerPosition Position;
	return BackpackItemContainer->FindPositionForItem(ItemInstance, Position);
}

void ARPGPlayerControllerBase::PickUpItem_Implementation(ARPGLootBase* ItemActor)
{
	if (BackpackItemContainer->AddItem(ItemActor->GetItemInstance()))
	{
		ItemActor->Destroy();
	}
	else
	{
		UE_LOG(LogActionRPG, Warning, TEXT("PickUpItem: Pick Up Item failed due to invalid position!"));
	}
}

void ARPGPlayerControllerBase::GrabItemInstance(FRPGItemInstance ItemInstance)
{
	OnGrabItemInstance(ItemInstance);	
}

void ARPGPlayerControllerBase::OnGrabItemInstance_Implementation(FRPGItemInstance ItemInstance)
{
	if (GrabbedItem.Item)
	{
		UE_LOG(LogActionRPG, Warning, TEXT("GrabItemInstance: Attempted to grab item instance when there is another item instance being grabbed!"));
		return;
	}

	ReceiveItemGrabbed.Broadcast(ItemInstance);
	GrabbedItem = ItemInstance;
}

void ARPGPlayerControllerBase::DropItemInstance()
{
	if (!GrabbedItem.Item)
	{
		return;
	}

	OnDropItemInstance(GrabbedItem);
}

void ARPGPlayerControllerBase::OnDropItemInstance_Implementation(FRPGItemInstance ItemInstance)
{
	ItemInstanceDropped();
}

void ARPGPlayerControllerBase::ItemInstanceDropped()
{
	ReceiveItemDropped.Broadcast(GrabbedItem);
	GrabbedItem = FRPGItemInstance();
}

bool ARPGPlayerControllerBase::IsGrabbingItem() const
{
	return GrabbedItem.Item != NULL;
}

void ARPGPlayerControllerBase::ClientShowDamageInfo_Implementation(float DamageAmount, ARPGCharacterBase* Target)
{
	ShowDamageInfo(DamageAmount, Target);
}

void ARPGPlayerControllerBase::ClientStartAbilityCooldown_Implementation(float Cooldown, const FRPGItemSlot& ItemSlot)
{
	StartAbilityCooldown(Cooldown, ItemSlot);
}

bool ARPGPlayerControllerBase::ServerTurnToTargetAndExecuteItemSlot_Validate(const FVector& Target, const FRPGItemSlot& ItemSlot)
{
	return true;
}

void ARPGPlayerControllerBase::ServerTurnToTargetAndExecuteItemSlot_Implementation(const FVector& Target, const FRPGItemSlot& ItemSlot)
{
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("FORCE ATTACK!"));

	AttackStartLocation = Target;
	ActionComponent->TurnToTargetAndExecuteItemSlot(Target, ItemSlot);
}

bool ARPGPlayerControllerBase::ServerExecuteActionOnActor_Validate(AActor* Target, EPlayerActionButton ActionButton, const FRPGItemSlot& ItemSlot)
{
	return true;
}

void ARPGPlayerControllerBase::ServerExecuteActionOnActor_Implementation(AActor* Target, EPlayerActionButton ActionButton, const FRPGItemSlot& ItemSlot)
{
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("ACTOR!"));

	ARPGCharacterBase* Char = GetPlayerCharacter();
	if (!Char)
	{
		return;
	}

	ULocalPlayer* LocalPlayer = URPGBlueprintFunctionLibrary::GetLocalPlayerController(this)->GetLocalPlayer();
	URPGFactionManagerSubsystem* FactionManager = LocalPlayer->GetSubsystem<URPGFactionManagerSubsystem>();

	IRPGInteractableInterface* ITarget = Cast<IRPGInteractableInterface>(Target);
	check(ITarget);

	// Non-hostile interaction
	if (ActionButton == EPlayerActionButton::ABE_Primary &&
		FactionManager->GetHostilityRankingForLocalPlayer(Target) != EHostilityRanking::HRE_Enemy && !ITarget->ShouldAttackOnInteraction(this))
	{
		ActionComponent->MoveToTargetAndInteract(Target, Target->GetActorLocation());
		return;
	}

	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Green, TEXT("ServerExecuteActionOnActor!"));

	// Hostile melee interaction
	FGameplayAbilitySpecHandle* SpecHandle = Char->GetCombatComponent()->SlottedAbilities.Find(ItemSlot);
	FGameplayAbilitySpec* Spec = Char->AbilitySystemComponent->FindAbilitySpecFromHandle(*SpecHandle);
	if (Spec)
	{
		if (Spec->Ability->AbilityTags.HasTag(UGameplayTagsManager::Get().RequestGameplayTag(GT_MeleeAbility)))
		{
			GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Green, TEXT("Melee!"));
			ActionComponent->MoveToTargetAndExecuteItemSlot(Target, ItemSlot);
			return;
		}
	}

	// Fallback
	ServerTurnToTargetAndExecuteItemSlot(Target->GetActorLocation(), ItemSlot);
}

bool ARPGPlayerControllerBase::ServerExecuteActionOnInteractable_Validate(AActor* Target, const FVector& TargetLocation, EPlayerActionButton ActionButton, const FRPGItemSlot& ItemSlot, bool bIsActionTargetSet)
{
	return true;
}

void ARPGPlayerControllerBase::ServerExecuteActionOnInteractable_Implementation(AActor* Target, const FVector& TargetLocation, EPlayerActionButton ActionButton, const FRPGItemSlot& ItemSlot, bool bIsActionTargetSet)
{	
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("INTERACT CLIENT!"));
	
	ULocalPlayer* LocalPlayer = URPGBlueprintFunctionLibrary::GetLocalPlayerController(this)->GetLocalPlayer();
	URPGFactionManagerSubsystem* FactionManager = LocalPlayer->GetSubsystem<URPGFactionManagerSubsystem>();

	// If interactable is hostile - attack
	if (Target && FactionManager->GetHostilityRankingForLocalPlayer(Target) == EHostilityRanking::HRE_Enemy)
	{
		ServerExecuteActionOnActor(Target, ActionButton, ItemSlot);
		return;
	}

	ActionComponent->MoveToTargetAndInteract(Target, TargetLocation, bIsActionTargetSet);
}

bool ARPGPlayerControllerBase::ServerMoveToDestination_Validate()
{
	return true;
}

void ARPGPlayerControllerBase::ServerMoveToDestination_Implementation()
{
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("MOVE!"));

	if (!MoveDummy)
	{
		return;
	}

	ActionComponent->MoveToActor(MoveDummy);
}

void ARPGPlayerControllerBase::ClientPickupLoot_Implementation(AActor* Loot)
{
	ARPGCharacterBase* Char = GetPlayerCharacter();
	if (!Char)
	{
		return;
	}

	ClientStopInteraction();

	// Loot doesn't exist on server (is unique for each client)
	Char->SetActionTarget(FActionTarget(
		Loot->GetActorLocation(),
		Loot
	));

	FRPGItemSlot ItemSlot;
	GetItemSlotByCurrentActionButton(ItemSlot);

	ServerExecuteActionOnInteractable(NULL, Loot->GetActorLocation(), EPlayerActionButton::ABE_None, ItemSlot, HasAuthority());
}

bool ARPGPlayerControllerBase::ServerUpdateMoveDummyLocation_Validate(const FVector& Target)
{
	return MoveDummy != NULL;
}

void ARPGPlayerControllerBase::ServerUpdateMoveDummyLocation_Implementation(const FVector& NewLocation)
{
	check(MoveDummy);

	ARPGCharacterBase* Char = GetPlayerCharacter();
	if (!Char)
	{
		return;
	}

	MoveDummy->SetActorLocation(NewLocation);
}

bool ARPGPlayerControllerBase::ServerCheckCombo_Validate()
{
	return true;
}

void ARPGPlayerControllerBase::ServerCheckCombo_Implementation()
{
	ARPGCharacterBase* Char = GetPlayerCharacter();
	if (!Char)
	{
		return;
	}

	Char->GetCombatComponent()->StartCombo();
}

void ARPGPlayerControllerBase::BlueprintClientNotifyReady()
{
	ClientNotifyReady();
}

void ARPGPlayerControllerBase::BlueprintServerCheckCombo()
{
	ServerCheckCombo();
}

void ARPGPlayerControllerBase::ClientNotifyReady_Implementation()
{
	bIsBusy = false;
}

void ARPGPlayerControllerBase::ClientInteract_Implementation(AActor* Target)
{
	ARPGCharacterBase* Char = GetPlayerCharacter();
	if (!Char)
	{
		return;
	}
	
	if (Target == NULL)
	{
		const FActionTarget& ActionTarget = Char->GetActionTarget();
		Target = ActionTarget.Actor;
	}
	
	IRPGInteractableInterface* ITarget = Cast<IRPGInteractableInterface>(Target);
	if (ITarget && ITarget->CanBeInteract())
	{
		CurrentInteractable = Target;
		ITarget->InteractClient(this);
	}
}

bool ARPGPlayerControllerBase::IsInteracting() const
{
	return CurrentInteractable != NULL;
}

void ARPGPlayerControllerBase::ClientStopInteraction_Implementation()
{
	if (!CurrentInteractable)
	{
		return;
	}
	
	IRPGInteractableInterface* ITarget = Cast<IRPGInteractableInterface>(CurrentInteractable);
	if (ITarget)
	{
		ITarget->StopInteractionClient(this);
	}

	ServerOnInteractionStopped();

	CurrentInteractable = NULL;
}

bool ARPGPlayerControllerBase::ServerOnInteractionStopped_Validate()
{
	return true;
}

void ARPGPlayerControllerBase::ServerOnInteractionStopped_Implementation()
{
	ARPGCharacterBase* Char = GetPlayerCharacter();
	if (!Char)
	{
		return;
	}

	const FActionTarget& AT = Char->GetActionTarget();
	
	IRPGInteractableInterface* ITarget = Cast<IRPGInteractableInterface>(AT.Actor);
	if (ITarget && !ITarget)
	{
		ITarget->StopInteraction(this);
	}
}

bool ARPGPlayerControllerBase::GetHitResultUnderCursorByChannelIgnoreSelf(const ECollisionChannel TraceChannel, bool bTraceComplex, FHitResult& HitResult) const
{
	ULocalPlayer* LocalPlayer = Cast<ULocalPlayer>(Player);
	bool bHit = false;
	if (LocalPlayer && LocalPlayer->ViewportClient)
	{
		FVector2D MousePosition;
		if (LocalPlayer->ViewportClient->GetMousePosition(MousePosition))
		{
			FCollisionQueryParams CollisionQueryParams(SCENE_QUERY_STAT(ClickableTrace), bTraceComplex, GetPlayerCharacter());
			bHit = GetHitResultAtScreenPosition(MousePosition, TraceChannel, CollisionQueryParams, HitResult);
		}
	}

	if (!bHit)	//If there was no hit we reset the results. This is redundant but helps Blueprint users
	{
		HitResult = FHitResult();
	}

	return bHit;
}

void ARPGPlayerControllerBase::BlueprintSetHighlightToComponent(UPrimitiveComponent* Component, bool bOn)
{
	bOn ? BlueprintSetHighlightOn(Component, NeutralHighlightColor) : BlueprintSetHighlightOff(Component);
}

bool ARPGPlayerControllerBase::GetSlottedItem(const FRPGItemSlot& ItemSlot, FRPGItemAtSlot& Result) const
{
	const FRPGItemAtSlot* FoundItem = SlottedItems.Find(ItemSlot);

	if (FoundItem)
	{
		Result = *FoundItem;
		return true;
	}

	return false;
}

void ARPGPlayerControllerBase::NotifySlottedItemChanged(FRPGItemSlot ItemSlot, URPGItem* Item)
{
	// Notify native before blueprint
	OnSlottedItemChangedNative.Broadcast(ItemSlot, Item);
	OnSlottedItemChanged.Broadcast(ItemSlot, Item);

	// Call BP update event
	SlottedItemChanged(ItemSlot, Item);
}

void ARPGPlayerControllerBase::NotifySlottedAbilitiesLoaded()
{
	// Notify native before blueprint
	OnSlottedAbilitiesLoadedNative.Broadcast();
	OnSlottedAbilitiesLoaded.Broadcast();
}

void ARPGPlayerControllerBase::AddActionBinding(EPlayerActionButton Button, const FRPGItemSlot& ItemSlot)
{
	// Unmap if there is already some binding mapped
	EPlayerActionButton FoundButton;
	if (IsBoundToActionKey(ItemSlot, FoundButton))
	{
		RemoveActionBinding(FoundButton);
	}

	ActionBindings.Add(Button, ItemSlot);

	URPGGameInstanceBase* GameInstance = URPGBlueprintFunctionLibrary::GetGameInstance(this);
	check(GameInstance);
	GameInstance->OnActionBindingAdded(Button, ItemSlot);

	ActionBindingAdded.Broadcast(Button, ItemSlot);
}

void ARPGPlayerControllerBase::RemoveActionBinding(EPlayerActionButton Button)
{
	ActionBindings.Remove(Button);

	URPGGameInstanceBase* GameInstance = URPGBlueprintFunctionLibrary::GetGameInstance(this);
	check(GameInstance);
	GameInstance->OnActionBindingRemoved(Button);

	ActionBindingRemoved.Broadcast(Button);
}

void ARPGPlayerControllerBase::RebuildAssignableSkills()
{
	ARPGCharacterBase* Char = GetPlayerCharacter();
	if (!Char)
	{
		return;
	}

	AssignableSkills.Empty();

	for (const TPair<FRPGItemSlot, FRPGItemAtSlot>& Pair : SlottedItems)
	{
		const FRPGItemSlot& ItemSlot = Pair.Key;
		URPGSkillItem* Skill = Cast<URPGSkillItem>(Pair.Value.Item);
		if (ItemSlot.ItemType == URPGAssetManager::SkillItemType && Skill && Skill->GrantedAbility != NULL)
		{
			AssignableSkills.Add(Pair.Value);
		}
	}
}

bool ARPGPlayerControllerBase::IsBoundToActionKey(const FRPGItemSlot& ItemSlot, EPlayerActionButton& Button)
{
	Button = EPlayerActionButton::ABE_None;

	for (const TPair<EPlayerActionButton, FRPGItemSlot> Pair : ActionBindings)
	{
		if (SlottedItems.Contains(Pair.Value)) 
		{
			Button = Pair.Key;
			return true;
		}
	}

	return false;
}

void ARPGPlayerControllerBase::SetPrimaryActionOn()
{
	CurrentActionButton = EPlayerActionButton::ABE_Primary;
}

void ARPGPlayerControllerBase::SetSecondaryActionOn()
{
	CurrentActionButton = EPlayerActionButton::ABE_Secondary;
}

void ARPGPlayerControllerBase::SetKey1ActionOn()
{
	CurrentActionButton = EPlayerActionButton::ABE_Key1;
}

void ARPGPlayerControllerBase::SetKey2ActionOn()
{
	CurrentActionButton = EPlayerActionButton::ABE_Key2;
}

void ARPGPlayerControllerBase::SetKey3ActionOn()
{
	CurrentActionButton = EPlayerActionButton::ABE_Key3;
}

void ARPGPlayerControllerBase::SetKey4ActionOn()
{
	CurrentActionButton = EPlayerActionButton::ABE_Key4;
}

void ARPGPlayerControllerBase::SetActionOff()
{
	CurrentActionButton = EPlayerActionButton::ABE_None;
}

void ARPGPlayerControllerBase::SetLockAttackOn()
{
	bIsLockAttackOn = true;
}

void ARPGPlayerControllerBase::SetLockAttackOff()
{
	bIsLockAttackOn = false;
}

void ARPGPlayerControllerBase::SetCameraZoom(float Amount)
{
	ARPGCharacterBase* PlayerCharacter = GetPlayerCharacter();
	if (!PlayerCharacter)
	{
		return;
	}

	ARPGHUDBase* HUD = URPGBlueprintFunctionLibrary::GetHUD(this);
	if (HUD && HUD->IsAnyWindowOpen())
	{
		return;
	}

	IRPGTopDownCameraActorInterface* ITopDownCamera = Cast<IRPGTopDownCameraActorInterface>(PlayerCharacter);
	URPGTopDownCameraComponent* TopDownCamera = ITopDownCamera->GetTopDownCamera();

	// Player Character implements this to return attached Top Down Camera component
	if (TopDownCamera)
	{
		TopDownCamera->SetCameraZoomAmount(Amount);
	}	
}

void ARPGPlayerControllerBase::ToggleInventory()
{
	ARPGCharacterBase* PlayerCharacter = GetPlayerCharacter();
	if (!PlayerCharacter || !PlayerCharacter->GetCombatComponent()->IsAlive())
	{
		return;
	}

	ARPGHUDBase* HUD = URPGBlueprintFunctionLibrary::GetHUD(this);

	if (HUD)
	{
		HUD->ToggleInventory();
	}
}

void ARPGPlayerControllerBase::ToggleCharacterSheet()
{
	ARPGCharacterBase* PlayerCharacter = GetPlayerCharacter();
	if (!PlayerCharacter || !PlayerCharacter->GetCombatComponent()->IsAlive())
	{
		return;
	}

	ARPGHUDBase* HUD = URPGBlueprintFunctionLibrary::GetHUD(this);

	if (HUD)
	{
		HUD->ToggleCharacterSheet();
	}
}

void ARPGPlayerControllerBase::ToggleSkillSheet()
{
	ARPGCharacterBase* PlayerCharacter = GetPlayerCharacter();
	if (!PlayerCharacter || !PlayerCharacter->GetCombatComponent()->IsAlive())
	{
		return;
	}

	ARPGHUDBase* HUD = URPGBlueprintFunctionLibrary::GetHUD(this);

	if (HUD)
	{
		HUD->ToggleSkillSheet();
	}
}

void ARPGPlayerControllerBase::ClearHighlightForActor(AActor* Target)
{
	if (Target != HighlightedActor)
	{
		return;
	}
	
	URPGBlueprintFunctionLibrary::ClearHighlightForActor(GetWorld(), this, Target);
	
	HighlightedActor = NULL;
}

void ARPGPlayerControllerBase::SetHighlightOn(AActor* Target)
{
	URPGBlueprintFunctionLibrary::SetHighlightToActor(GetWorld(), this, Target);
	HighlightedActor = Target;
}

void ARPGPlayerControllerBase::SetHighlightOff()
{
	ClearHighlightForActor(HighlightedActor);
}


void ARPGPlayerControllerBase::ProcessDynamicTransparencyQuery()
{
	ARPGCharacterBase* PlayerCharacter = GetPlayerCharacter();
	// Make sure player is spawned
	if (!PlayerCharacter)
	{
		return;
	}

	// Do not bother for controllers which aren't me
	if (URPGBlueprintFunctionLibrary::GetLocalPlayerController(this) != this)
	{
		return;
	}

	// Trace from camera towards player
	FVector Start = PlayerCameraManager->GetCameraLocation();
	FVector End = PlayerCharacter->GetActorLocation();

	FHitResult HitResult;
	FCollisionQueryParams Params(true);
	bool IsHit = GetWorld()->LineTraceSingleByChannel(HitResult, Start, End, ECC_DynamicTransparency, Params);

	// Turn transparency on or off
	if (IsHit)
	{
		if (PlayerOverlappingActor == HitResult.Actor.Get())
		{
			return;
		}

		if (PlayerOverlappingActor)
		{
			PlayerOverlappingActor->MakeSemiTransparent(false);
			PlayerOverlappingActor = NULL;
		}

		PlayerOverlappingActor = HitResult.Actor.Get();
		if (PlayerOverlappingActor)
		{
			PlayerOverlappingActor->MakeSemiTransparent(true);
		}
	}
	else
	{
		if (PlayerOverlappingActor)
		{
			PlayerOverlappingActor->MakeSemiTransparent(false);
			PlayerOverlappingActor = NULL;
		}
	}
}

void ARPGPlayerControllerBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ARPGPlayerControllerBase, AssignableSkills);
}
