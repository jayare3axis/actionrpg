// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGAggroComponent.h"
#include "Engine/LocalPlayer.h"
#include "RPGCharacterBase.h"
#include "RPGAIControllerBase.h"
#include "RPGPlayerControllerBase.h"
#include "RPGFactionManagerSubsystem.h"
#include "RPGBlueprintFunctionLibrary.h"

URPGAggroComponent::URPGAggroComponent()
{
}

void URPGAggroComponent::TargetRecieved(AActor* Actor)
{
	// Retrive controlled character
	ARPGCharacterBase* Character = GetControlledCharacter();
	if (!Character)
	{
		UE_LOG(LogActionRPG, Warning, TEXT("URPGAggroComponent: Controlled Character not present or invalid!"));
		return;
	}

	// Check if target is hostile
	ARPGPlayerControllerBase* PC = URPGBlueprintFunctionLibrary::GetLocalPlayerController(this);
	ULocalPlayer* LocalPlayer = PC->GetLocalPlayer();
	URPGFactionManagerSubsystem* FactionManager = LocalPlayer->GetSubsystem<URPGFactionManagerSubsystem>();
	if (FactionManager->GetHostilityRankingForActor(Character, Actor) != EHostilityRanking::HRE_Enemy)
	{
		return;
	}

	if (!RecievedTargets.Contains(Actor))
	{
		RecievedTargets.Add(Actor);
	}
}

void URPGAggroComponent::TargetLost(AActor* Actor)
{
	if (RecievedTargets.Contains(Actor))
	{
		RecievedTargets.Remove(Actor);
	}
}

void URPGAggroComponent::UpdateCurrentAggro()
{
	// Retrive controlled character
	ARPGCharacterBase* Character = GetControlledCharacter();
	if (!Character)
	{
		UE_LOG(LogActionRPG, Warning, TEXT("URPGAggroComponent: Controlled Character not present or invalid!"));
		return;
	}

	AActor* Result = NULL;
	
	// For now - lets just choose the closest target
	float SmallestDistance = 10000.f;
	for (AActor* Target : RecievedTargets)
	{
		// Check if target is still alive
		ARPGCharacterBase* TargetCharacter = Cast<ARPGCharacterBase>(Target);
		if (TargetCharacter && (TargetCharacter->GetCombatComponent() && !TargetCharacter->GetCombatComponent()->IsAlive()))
		{
			continue;
		}

		float Distance = FMath::Abs((Character->GetActorLocation() - Target->GetActorLocation()).Size());
		if (Distance < SmallestDistance)
		{
			Result = Target;
			SmallestDistance = Distance;
		}
	}

	// Notify previous aggro it is not aggroed anymore
	if (CurrentAggro != Result)
	{
		AAIController* AIController = Cast<AAIController>(GetOwner());

		ARPGCharacterBase* PreviousAggroChar = Cast<ARPGCharacterBase>(CurrentAggro);
		if (PreviousAggroChar)
		{
			PreviousAggroChar->GetCombatComponent()->LostByAggro(Cast<ARPGCharacterBase>(AIController->GetCharacter()));
		}

		ARPGCharacterBase* CurrentAggroChar = Cast<ARPGCharacterBase>(Result);
		if (CurrentAggroChar)
		{
			CurrentAggroChar->GetCombatComponent()->RecievedByAggro(Cast<ARPGCharacterBase>(AIController->GetCharacter()));
		}
	}

	CurrentAggro = Result;
}

ARPGCharacterBase* URPGAggroComponent::GetControlledCharacter() const
{
	ARPGAIControllerBase* AI = Cast<ARPGAIControllerBase>(GetOwner());
	if (!AI)
	{
		return NULL;
	}

	return Cast<ARPGCharacterBase>(AI->GetCharacter());
}
