// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGSpawnEnemy.h"
#include "RPGCombatComponent.h"

ARPGSpawnEnemy::ARPGSpawnEnemy()
{
	bReplicates = false;
	bNetLoadOnClient = false;
}

void ARPGSpawnEnemy::BeginPlay()
{
	Super::BeginPlay();
	
	if (!Data) {
		return;
	}

	SpawnEnemyAtMyLocation();

	// We are done, we are not needed anymore.
	Destroy();
}

void ARPGSpawnEnemy::SpawnEnemy(URPGEnemyDataAsset* _Data)
{
	Data = _Data;
	SpawnEnemyAtMyLocation();

	// We are done, we are not needed anymore.
	Destroy();
}

void ARPGSpawnEnemy::SpawnEnemyAtMyLocation_Implementation()
{
	if (!Data->GetCharacterClass())
	{
		return;
	}

	// Spawn the character
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	SpawnedCharacter = GetWorld()->SpawnActor<ARPGCharacterBase>(Data->GetCharacterClass(), GetActorLocation(), GetActorRotation(), SpawnInfo);
	if (!SpawnedCharacter)
	{
		return;
	}
	
	// Handle abilities
	URPGCombatComponent* CombatComponent = SpawnedCharacter->GetCombatComponent();
	if (CombatComponent)
	{
		CombatComponent->InitAbilities(Data->GetGameplayAbilities(), Data->GetDefaultSlottedAbilities(), Data->GetPassiveGameplayEffects());
	}
	else
	{
		UE_LOG(LogActionRPG, Warning, TEXT("Spawn Enemy: Combat Component not found!"));
	}

	// Spawn AI for the enemy, needs to be done after abilities are set
	if (Data->GetAIControllerClass())
	{
		SpawnedCharacter->AIControllerClass = Data->GetAIControllerClass();
		SpawnedCharacter->SpawnDefaultController();
	}
}
