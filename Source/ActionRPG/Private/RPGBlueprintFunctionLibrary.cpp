// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGBlueprintFunctionLibrary.h"
#include "RPGPlayerControllerBase.h"
#include "RPGHUDBase.h"
#include "RPGCharacterBase.h"
#include "RPGInteractableInterface.h"
#include "RPGHighlightableInterface.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Items/RPGItem.h"
#include "ActionRPGLoadingScreen.h"
#include "Abilities/RPGDamageGameplayEffect.h"
#include "GameplayTagsModule.h"
#include "RPGFactionManagerSubsystem.h"
#include "Engine/LocalPlayer.h"
#include "RPGGameInstanceBase.h"

int32 __NEXT_ITEM_ID = 0;

URPGGameInstanceBase* URPGBlueprintFunctionLibrary::GetGameInstance(const UObject* WorldContextObject)
{
	URPGGameInstanceBase* Result = Cast<URPGGameInstanceBase>( UGameplayStatics::GetGameInstance(WorldContextObject) );

	if (!Result)
	{
		UE_LOG(LogActionRPG, Error, TEXT("URPGBlueprintFunctionLibrary: URPGGameInstanceBase not found!"));
		return NULL;
	}

	return Result;
}

bool URPGBlueprintFunctionLibrary::IsSaveGameSession(const UObject* WorldContextObject)
{
	FName CurrentSaveGameId = "";

	URPGGameInstanceBase* GameInstance = URPGBlueprintFunctionLibrary::GetGameInstance(WorldContextObject);
	CurrentSaveGameId = GameInstance->GetCurrentSaveGame();

	return !CurrentSaveGameId.IsNone();
}

ARPGPlayerControllerBase* URPGBlueprintFunctionLibrary::GetLocalPlayerController(const UObject* WorldContextObject)
{
	TArray<AActor*> Actors;		
	ARPGPlayerControllerBase* Result = Cast<ARPGPlayerControllerBase>( UGameplayStatics::GetPlayerController(WorldContextObject, 0) );

	if (!Result)
	{
		UE_LOG(LogActionRPG, Error, TEXT("URPGBlueprintFunctionLibrary: ARPGPlayerController not found!"));
		return NULL;
	}

	return Result;
}

ARPGHUDBase* URPGBlueprintFunctionLibrary::GetHUD(const UObject* WorldContextObject)
{
	TArray<AActor*> Actors;
	UGameplayStatics::GetAllActorsOfClass(WorldContextObject, ARPGHUDBase::StaticClass(), Actors);

	if (Actors.Num() == 0)
	{
		return NULL;
	}

	return Cast<ARPGHUDBase>(Actors[0]);
}

ARPGPlayerControllerBase* URPGBlueprintFunctionLibrary::GetPlayerControlerByCharacter(const UObject* WorldContextObject, const ARPGCharacterBase* Character)
{
	ARPGPlayerControllerBase* Result = Character->GetInstigatorController<ARPGPlayerControllerBase>();
	return Result;
}

bool URPGBlueprintFunctionLibrary::IsLocalPlayer(const UObject* WorldContextObject, ARPGPlayerControllerBase* PC)
{
	return PC->GetLocalPlayer() != NULL;
}

URPGRaceDataAsset* URPGBlueprintFunctionLibrary::GetAvailableRace(const UObject* WorldContextObject, const FName& Key)
{
	URPGAssetManager& AssetManager = URPGAssetManager::Get();
	TArray<FPrimaryAssetId> PrimaryAssetIdList;
	if (AssetManager.GetPrimaryAssetIdList(URPGRaceDataAsset::PrimaryAssetType, PrimaryAssetIdList))
	{
		for (FPrimaryAssetId PrimaryAssetId : PrimaryAssetIdList)
		{
			URPGRaceDataAsset* LoadedRace = AssetManager.ForceLoadRace(PrimaryAssetId);
			if (LoadedRace->Key == Key)
			{
				return LoadedRace;
			}
		}
	}

	return NULL;
}

URPGClassDataAsset* URPGBlueprintFunctionLibrary::GetAvailableClass(const UObject* WorldContextObject, const FName& Key)
{
	URPGAssetManager& AssetManager = URPGAssetManager::Get();
	TArray<FPrimaryAssetId> PrimaryAssetIdList;
	if (AssetManager.GetPrimaryAssetIdList(URPGClassDataAsset::PrimaryAssetType, PrimaryAssetIdList))
	{
		for (FPrimaryAssetId PrimaryAssetId : PrimaryAssetIdList)
		{
			URPGClassDataAsset* LoadedClass = AssetManager.ForceLoadClass(PrimaryAssetId);
			if (LoadedClass->Key == Key)
			{
				return LoadedClass;
			}
		}
	}

	return NULL;
}


URPGSkillSetDataAsset* URPGBlueprintFunctionLibrary::GetAvailableSkillSet(const UObject* WorldContextObject, const FName& Key)
{
	URPGAssetManager& AssetManager = URPGAssetManager::Get();
	TArray<FPrimaryAssetId> PrimaryAssetIdList;
	if (AssetManager.GetPrimaryAssetIdList(URPGSkillSetDataAsset::PrimaryAssetType, PrimaryAssetIdList))
	{
		for (FPrimaryAssetId PrimaryAssetId : PrimaryAssetIdList)
		{
			URPGSkillSetDataAsset* LoadedSkillSet = AssetManager.ForceLoadSkillSet(PrimaryAssetId);
			if (LoadedSkillSet->Key == Key)
			{
				return LoadedSkillSet;
			}
		}
	}

	return NULL;
}

URPGSkillTierDataAsset* URPGBlueprintFunctionLibrary::GetAvailableSkillTier(const UObject* WorldContextObject, const FName& Key)
{
	URPGAssetManager& AssetManager = URPGAssetManager::Get();
	TArray<FPrimaryAssetId> PrimaryAssetIdList;
	if (AssetManager.GetPrimaryAssetIdList(URPGSkillTierDataAsset::PrimaryAssetType, PrimaryAssetIdList))
	{
		for (FPrimaryAssetId PrimaryAssetId : PrimaryAssetIdList)
		{
			URPGSkillTierDataAsset* LoadedSkillTier = AssetManager.ForceLoadSkillTier(PrimaryAssetId);
			if (LoadedSkillTier->Key == Key)
			{
				return LoadedSkillTier;
			}
		}
	}

	return NULL;
}

void URPGBlueprintFunctionLibrary::GetAvailableRacesList(const UObject* WorldContextObject, TArray<FRPGOrderedListItem>& Result)
{
	Result = TArray<FRPGOrderedListItem>();

	URPGAssetManager& AssetManager = URPGAssetManager::Get();
	TArray<FPrimaryAssetId> PrimaryAssetIdList;
	if (AssetManager.GetPrimaryAssetIdList(URPGRaceDataAsset::PrimaryAssetType, PrimaryAssetIdList))
	{
		for (FPrimaryAssetId PrimaryAssetId : PrimaryAssetIdList)
		{
			URPGRaceDataAsset* LoadedRace = AssetManager.ForceLoadRace(PrimaryAssetId);
			Result.Add(FRPGOrderedListItem(LoadedRace->Key, LoadedRace->DisplayName, LoadedRace->Order));
		}
	}

	Result.Sort();
}

void URPGBlueprintFunctionLibrary::GetAvailableClassesList(const UObject* WorldContextObject, TArray<FRPGOrderedListItem>& Result)
{
	Result = TArray<FRPGOrderedListItem>();

	URPGAssetManager& AssetManager = URPGAssetManager::Get();
	TArray<FPrimaryAssetId> PrimaryAssetIdList;
	if (AssetManager.GetPrimaryAssetIdList(URPGClassDataAsset::PrimaryAssetType, PrimaryAssetIdList))
	{
		for (FPrimaryAssetId PrimaryAssetId : PrimaryAssetIdList)
		{
			URPGClassDataAsset* LoadedClass = AssetManager.ForceLoadClass(PrimaryAssetId);
			Result.Add(FRPGOrderedListItem(LoadedClass->Key, LoadedClass->DisplayName, LoadedClass->Order));
		}
	}

	Result.Sort();
}

void URPGBlueprintFunctionLibrary::GetAvailableSkillSetsList(const UObject* WorldContextObject, TArray<FRPGOrderedListItem>& Result)
{
	Result = TArray<FRPGOrderedListItem>();

	URPGAssetManager& AssetManager = URPGAssetManager::Get();
	TArray<FPrimaryAssetId> PrimaryAssetIdList;
	if (AssetManager.GetPrimaryAssetIdList(URPGSkillSetDataAsset::PrimaryAssetType, PrimaryAssetIdList))
	{
		for (FPrimaryAssetId PrimaryAssetId : PrimaryAssetIdList)
		{
			URPGSkillSetDataAsset* LoadedSkillSet = AssetManager.ForceLoadSkillSet(PrimaryAssetId);
			Result.Add(FRPGOrderedListItem(LoadedSkillSet->Key, LoadedSkillSet->DisplayName, LoadedSkillSet->Order));
		}
	}

	Result.Sort();
}

void URPGBlueprintFunctionLibrary::GetSkillTiersBySkillSet(const UObject* WorldContextObject, FName SkillSetName, TArray<FRPGOrderedListItem>& Result)
{
	Result = TArray<FRPGOrderedListItem>();

	URPGSkillSetDataAsset* SkillSet = URPGBlueprintFunctionLibrary::GetAvailableSkillSet(WorldContextObject, SkillSetName);
	check(SkillSet);

	for (URPGSkillTierDataAsset* SkillTier : SkillSet->Tiers)
	{
		Result.Add(FRPGOrderedListItem(SkillTier->Key, SkillTier->DisplayName, SkillTier->Level));
	}

	Result.Sort();
}

bool URPGBlueprintFunctionLibrary::IsSkillTierUnlockedByPlayer(const UObject* WorldContextObject, const URPGSkillTierDataAsset* SkillTier)
{
	ARPGPlayerControllerBase* PC = URPGBlueprintFunctionLibrary::GetLocalPlayerController(WorldContextObject);
	check(PC);

	URPGSkillItem* PrimarySkill = SkillTier->Skills[0];
	if (!PrimarySkill)
	{
		return false;
	}

	// Skill tier is considered unlocked if primary skill is acquired
	if (PC->HasItem(PrimarySkill))
	{
		return true;
	}

	// Skill tier is considered unlocked if some related tier has acquired at least 5 skills
	int32 Count;
	for (URPGSkillTierDataAsset* RelatedTier : SkillTier->RelatedTiers)
	{
		Count = 0;
		for (URPGSkillItem* Skill : RelatedTier->Skills)
		{
			if (PC->HasItem(Skill) && ++Count == 5)
			{
				return true;
			}
		}
	}

	return false;
}

void URPGBlueprintFunctionLibrary::PlayLoadingScreen(float PlayTime, const UTexture2D* LoadingScreenTexture, const FVector2D& BrushSize)
{
	IActionRPGLoadingScreenModule& LoadingScreenModule = IActionRPGLoadingScreenModule::Get();
	FString Path = LoadingScreenTexture->GetPathName();
	LoadingScreenModule.StartInGameLoadingScreen(PlayTime, FName(*Path), BrushSize);
}

bool URPGBlueprintFunctionLibrary::IsEquipableSlot(const FRPGItemSlot& ItemSlot)
{
	return ItemSlot.ItemType == URPGAssetManager::WeaponItemType || ItemSlot.ItemType == URPGAssetManager::ArmorItemType || ItemSlot.ItemType == URPGAssetManager::AccessoryItemType;
}

FRPGItemInstance URPGBlueprintFunctionLibrary::CreateItemInstance(const UObject* WorldContextObject, URPGItem* Item, int32 Count, int32 Level)
{
	return FRPGItemInstance(Item, Count, Level, __NEXT_ITEM_ID++);
}

float URPGBlueprintFunctionLibrary::GetTurnToTargetYaw(const UObject* WorldContextObject, const AActor* Actor, const AActor* Target)
{
	return  URPGBlueprintFunctionLibrary::GetTurnToPointYaw(WorldContextObject, Actor, Target->GetActorLocation());
}

float URPGBlueprintFunctionLibrary::GetTurnToPointYaw(const UObject* WorldContextObject, const AActor* Actor, const FVector& TargetPoint)
{
	FRotator ResultRotation = (UKismetMathLibrary::FindLookAtRotation(Actor->GetActorLocation(), TargetPoint) - Actor->GetActorRotation());

	ResultRotation.Normalize();

	return ResultRotation.Yaw;
}

void URPGBlueprintFunctionLibrary::HostilityRankingToHighlightColor(const UObject* WorldContextObject, const EHostilityRanking& Value, FLinearColor& Result)
{
	ARPGPlayerControllerBase* PC = URPGBlueprintFunctionLibrary::GetLocalPlayerController(WorldContextObject);
	check(PC);

	switch (Value)
	{
		case EHostilityRanking::HRE_Enemy:
			Result = PC->GetHostileHighlightColor();
			break;
		case EHostilityRanking::HRE_Friendly:
			Result = PC->GetFriendlyHighlightColor();
			break;
		default:
			Result = PC->GetNeutralHighlightColor();
	}
}

void URPGBlueprintFunctionLibrary::SetHighlightToActor(const UObject* WorldContextObject, ARPGPlayerControllerBase* PC, AActor* Actor)
{
	// Ignore if character is dead
	ARPGCharacterBase* Character = Cast<ARPGCharacterBase>(Actor);
	if (Character && (Character->GetCombatComponent() && !Character->GetCombatComponent()->IsAlive()))
	{
		return;
	}

	// Retrieve Faction Manager reference
	ULocalPlayer* LocalPlayer = PC->GetLocalPlayer();
	URPGFactionManagerSubsystem* FactionManager = LocalPlayer->GetSubsystem<URPGFactionManagerSubsystem>();

	const TArray<UActorComponent*>& Components = Actor->GetComponentsByTag(UPrimitiveComponent::StaticClass(), FName(TAG_Highlight));
	if (Components.Num() == 0)
	{
		UE_LOG(LogActionRPG, Error, TEXT("ARPGPlayerControllerBase: Highlight component not found!"));
		return;
	}

	// Retrieve highlight color based on hostility ranking
	FLinearColor HighlightColor;
	switch (FactionManager->GetHostilityRankingForLocalPlayer(Actor))
	{
		case EHostilityRanking::HRE_Enemy:
			HighlightColor = PC->GetHostileHighlightColor();
			break;
		case EHostilityRanking::HRE_Friendly:
			HighlightColor = PC->GetFriendlyHighlightColor();
			break;
		default:
			HighlightColor = PC->GetNeutralHighlightColor();
	}

	PC->BlueprintSetHighlightOn(Cast<UPrimitiveComponent>(Components[0]), HighlightColor);

	// Highlight also children components
	USceneComponent* SceneComponent = Cast<USceneComponent>(Components[0]);
	if (SceneComponent)
	{
		TArray<USceneComponent*> ChildComponents;
		SceneComponent->GetChildrenComponents(true, ChildComponents);

		for (USceneComponent* Child : ChildComponents)
		{
			UPrimitiveComponent* ChildAsPrim = Cast<UPrimitiveComponent>(Child);
			if (ChildAsPrim)
			{
				PC->BlueprintSetHighlightOn(ChildAsPrim, HighlightColor);
			}
		}
	}

	IRPGHighlightableInterface* ITarget = Cast<IRPGHighlightableInterface>(Actor);
	ITarget->Highlighted(true);
}

void URPGBlueprintFunctionLibrary::ClearHighlightForActor(const UObject* WorldContextObject, ARPGPlayerControllerBase* PC, AActor* Actor)
{
	const TArray<UActorComponent*>& Components = Actor->GetComponentsByTag(UPrimitiveComponent::StaticClass(), FName(TAG_Highlight));
	if (Components.Num() == 0)
	{
		UE_LOG(LogActionRPG, Warning, TEXT("ARPGPlayerControllerBase: Highlight component not found!"));
		return;
	}

	PC->BlueprintSetHighlightOff(Cast<UPrimitiveComponent>(Components[0]));

	// Clear highlight also for children components
	USceneComponent* SceneComponent = Cast<USceneComponent>(Components[0]);
	if (SceneComponent)
	{
		TArray<USceneComponent*> ChildComponents;
		SceneComponent->GetChildrenComponents(true, ChildComponents);

		for (USceneComponent* Child : ChildComponents)
		{
			UPrimitiveComponent* ChildAsPrim = Cast<UPrimitiveComponent>(Child);
			if (ChildAsPrim)
			{
				PC->BlueprintSetHighlightOff(ChildAsPrim);
			}
		}
	}

	IRPGHighlightableInterface* IHighlightedActor = Cast<IRPGHighlightableInterface>(Actor);
	IHighlightedActor->Highlighted(false);
}

FText URPGBlueprintFunctionLibrary::AttributeToDisplayString(const UObject* WorldContextObject, const FGameplayAttribute& Attribute)
{
	if (Attribute.GetName() == "MaxHealth") return FText::FromString("Health");
	if (Attribute.GetName() == "MaxMana") return FText::FromString("Mana");

	return FText::FromString(Attribute.GetName());
}

 float URPGBlueprintFunctionLibrary::GetAbilityCooldown(const UObject* WorldContextObject, URPGGameplayAbility* Ability)
{
	check(Ability);

	float Result = 0;

	UGameplayEffect* CooldownEffect = Ability->GetCooldownGameplayEffect();
	if (!CooldownEffect)
	{
		return Result;
	}

	CooldownEffect->DurationMagnitude.GetStaticMagnitudeIfPossible(1, Result);

	return Result;
}

void URPGBlueprintFunctionLibrary::GetItemDescription(const UObject* WorldContextObject, const FRPGItemInstance& ItemInstance, TArray<FRPGItemsDescriptionField>& OutFields)
{
	if (ItemInstance.Item->GrantedAbility)
	{
		URPGGameplayAbility* Ability = ItemInstance.Item->GrantedAbility->GetDefaultObject<URPGGameplayAbility>();
		check(Ability);

		// First get all damages so they are displayed at the top
		for (const TPair<FGameplayTag, FRPGGameplayEffectContainer>& Pair : Ability->EffectContainerMap)
		{
			for (TSubclassOf<UGameplayEffect> EffectClass : Pair.Value.TargetGameplayEffectClasses)
			{
				AddEffectDescription(WorldContextObject, EffectClass, ItemInstance.Level, OutFields);
			}
		}
	}

	for (const FEffectAndLevel& GrantedEffect : ItemInstance.Item->GrantedGameplayEffects)
	{
		AddEffectDescription(WorldContextObject, GrantedEffect.Effect, GrantedEffect.Level, OutFields);
	}
}

void URPGBlueprintFunctionLibrary::AddEffectDescription(const UObject* WorldContextObject, const TSubclassOf<UGameplayEffect> EffectClass, float Level, TArray<FRPGItemsDescriptionField>& OutFields)
{
	UGameplayEffect* Effect = EffectClass->GetDefaultObject<UGameplayEffect>();
	check(Effect);

	// Damage
	URPGDamageGameplayEffect* DamageEffect = Cast<URPGDamageGameplayEffect>(Effect);
	if (DamageEffect)
	{
		OutFields.Add(FRPGItemsDescriptionField(
			DamageEffect->EvalMinValue(Level),
			DamageEffect->EvalMaxValue(Level),
			FText::FromString(""),
			EItemDescriptionsFieldType::IDE_Damage
		));
	}
	else
	{
		// Process modifiers
		for (const FGameplayModifierInfo& Modifier : Effect->Modifiers)
		{
			OutFields.Add(FRPGItemsDescriptionField(
				Level,
				0.f,
				URPGBlueprintFunctionLibrary::AttributeToDisplayString(WorldContextObject, Modifier.Attribute),
				EItemDescriptionsFieldType::IDE_Modifier
			));
		}
	}
}

FRPGNamedAppearanceOption URPGBlueprintFunctionLibrary::CreateNamedAppearanceOption(const FName& InKey, int32 InValue)
{
	return FRPGNamedAppearanceOption(InKey, InValue);
}

FText URPGBlueprintFunctionLibrary::RarityRankingToText(ERarityRanking RarityRanking)
{
	switch (RarityRanking) 
	{
		case ERarityRanking::RRE_Common:
			return FText::FromString("Common");
		case ERarityRanking::RRE_Uncommon:
			return FText::FromString("Uncommon");
		case ERarityRanking::RRE_Rare:
			return FText::FromString("Rare");
		case ERarityRanking::RRE_Epic:
			return FText::FromString("Epic");
		case ERarityRanking::RRE_Legendary:
			return FText::FromString("Legendary");
		default:
			return FText::FromString("Common");
	}
}

bool URPGBlueprintFunctionLibrary::DoesEffectContainerSpecHaveEffects(const FRPGGameplayEffectContainerSpec& ContainerSpec)
{
	return ContainerSpec.HasValidEffects();
}

bool URPGBlueprintFunctionLibrary::DoesEffectContainerSpecHaveTargets(const FRPGGameplayEffectContainerSpec& ContainerSpec)
{
	return ContainerSpec.HasValidTargets();
}

FRPGGameplayEffectContainerSpec URPGBlueprintFunctionLibrary::AddTargetsToEffectContainerSpec(const FRPGGameplayEffectContainerSpec& ContainerSpec, const TArray<FHitResult>& HitResults, const TArray<AActor*>& TargetActors)
{
	FRPGGameplayEffectContainerSpec NewSpec = ContainerSpec;
	NewSpec.AddTargets(HitResults, TargetActors);
	return NewSpec;
}

TArray<FActiveGameplayEffectHandle> URPGBlueprintFunctionLibrary::ApplyExternalEffectContainerSpec(const FRPGGameplayEffectContainerSpec& ContainerSpec)
{
	TArray<FActiveGameplayEffectHandle> AllEffects;

	// Iterate list of gameplay effects
	for (const FGameplayEffectSpecHandle& SpecHandle : ContainerSpec.TargetGameplayEffectSpecs)
	{
		if (SpecHandle.IsValid())
		{
			// If effect is valid, iterate list of targets and apply to all
			for (TSharedPtr<FGameplayAbilityTargetData> Data : ContainerSpec.TargetData.Data)
			{
				AllEffects.Append(Data->ApplyGameplayEffectSpec(*SpecHandle.Data.Get()));
			}
		}
	}
	return AllEffects;
}

bool URPGBlueprintFunctionLibrary::IsMeleeAttack(const FGameplayTagContainer& Tags)
{
	FGameplayTagContainer TagsToMatch;
	UGameplayTagsManager::Get().RequestGameplayTagContainer({ GT_DamageTypeSlashing, GT_DamageTypePiercing, GT_DamageTypeCrushing }, TagsToMatch);

	return Tags.HasAny(TagsToMatch);
}
