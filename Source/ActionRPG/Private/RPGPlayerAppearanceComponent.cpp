// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGPlayerAppearanceComponent.h"
#include "RPGCharacterBase.h"

URPGPlayerAppearanceComponent::URPGPlayerAppearanceComponent()
{
	SetIsReplicated(true);
}

void URPGPlayerAppearanceComponent::OnRep_AppearanceOptions()
{
	for (FRPGNamedAppearanceOption Option : AppearanceOptions)
	{		
		HandleAppearanceOption(Option.Key, Option.Value);
	}
}

void URPGPlayerAppearanceComponent::BlueprintHandleAppearanceOption(FName Key, int32 Value)
{	
	HandleAppearanceOption(Key, Value);
}

void URPGPlayerAppearanceComponent::HandleAppearanceOption(FName Key, int32 Value)
{
	ARPGCharacterBase* Char = Cast<ARPGCharacterBase>(GetOwner());
	if (!Char)
	{
		UE_LOG(LogActionRPG, Warning, TEXT("Player Appearance Component: Owner must be Character!"));
		return;
	}

	Char->HandleAppearanceOption(Key, Value);
}

void URPGPlayerAppearanceComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(URPGPlayerAppearanceComponent, AppearanceOptions);
}
