// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGPlayerManagerSubsystem.h"
#include "RPGBlueprintFunctionLibrary.h"
#include "RPGInitManagerSubsystem.h"
#include "RPGGameInstanceBase.h"
#include "RPGCharacterBase.h"

void URPGPlayerManagerSubsystem::OnPlayerCharacterCreated(ARPGCharacterBase* Character)
{
	PlayerCharacters.Add(Character);

	ARPGPlayerControllerBase* PC = URPGBlueprintFunctionLibrary::GetPlayerControlerByCharacter(this, Character);
	if (PC == URPGBlueprintFunctionLibrary::GetLocalPlayerController(this))
	{
		URPGGameInstanceBase* GameInstance = URPGBlueprintFunctionLibrary::GetGameInstance(Character);
		URPGInitManagerSubsystem* InitManager = GameInstance->GetSubsystem<URPGInitManagerSubsystem>();
		check(InitManager);

		InitManager->Bind(GameInstance);
	}
}

void URPGPlayerManagerSubsystem::OnPlayerCharacterDestroyed(ARPGCharacterBase* Character)
{
	PlayerCharacters.Remove(Character);

	// TODO handle unregistering
	/*URPGGameInstanceBase* GameInstance = URPGBlueprintFunctionLibrary::GetGameInstance(Character);
	if (!GameInstance)
	{
		return;
	}
	GameInstance->OnPlayerCharacterDestroyed();*/
}
