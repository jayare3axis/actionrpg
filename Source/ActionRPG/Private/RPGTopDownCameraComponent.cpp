// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGTopDownCameraComponent.h"
#include "ActionRPG.h"
#include "Camera/CameraComponent.h"

#define MIN_SPRING_ARM_LENGTH 400.f
#define MAX_SPRING_ARM_LENGTH 1900.f
#define CAMERA_ZOOM_SPEED 50.f

URPGTopDownCameraComponent::URPGTopDownCameraComponent()
{
	// Spring Arm setup
	bAbsoluteRotation = true; // Don't want arm to rotate when character does
	TargetArmLength = MAX_SPRING_ARM_LENGTH;
	RelativeRotation = FRotator(-60.f, 0.f, 0.f);
	bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(this, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	bEnableCameraLag = true;
}

void URPGTopDownCameraComponent::SetCameraZoomAmount(float Amount)
{
	TargetArmLength -= Amount * CAMERA_ZOOM_SPEED;

	if (TargetArmLength > MAX_SPRING_ARM_LENGTH)
	{
		TargetArmLength = MAX_SPRING_ARM_LENGTH;
	}
	else if (TargetArmLength < MIN_SPRING_ARM_LENGTH)
	{
		TargetArmLength = MIN_SPRING_ARM_LENGTH;
	}
}
