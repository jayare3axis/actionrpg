// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGAssetManager.h"
#include "Data/RPGRaceDataAsset.h"
#include "Data/RPGClassDataAsset.h"
#include "Data/RPGSkillSetDataAsset.h"
#include "Data/RPGSkillTierDataAsset.h"
#include "Items/RPGItem.h"

const FPrimaryAssetType	URPGAssetManager::ConsumableItemType = TEXT("Consumable");
const FPrimaryAssetType	URPGAssetManager::SkillItemType = TEXT("Skill");
const FPrimaryAssetType	URPGAssetManager::TokenItemType = TEXT("Token");
const FPrimaryAssetType	URPGAssetManager::WeaponItemType = TEXT("Weapon");
const FPrimaryAssetType	URPGAssetManager::ArmorItemType = TEXT("Armor");
const FPrimaryAssetType	URPGAssetManager::AccessoryItemType = TEXT("Accessory");

URPGAssetManager& URPGAssetManager::Get()
{
	URPGAssetManager* This = Cast<URPGAssetManager>(GEngine->AssetManager);

	if (This)
	{
		return *This;
	}
	else
	{
		UE_LOG(LogActionRPG, Fatal, TEXT("Invalid AssetManager in DefaultEngine.ini, must be RPGAssetManager!"));
		return *NewObject<URPGAssetManager>();
	}
}

URPGItem* URPGAssetManager::ForceLoadItem(const FPrimaryAssetId& PrimaryAssetId, bool bLogWarning)
{
	FSoftObjectPath ItemPath = GetPrimaryAssetPath(PrimaryAssetId);

	// This does a synchronous load and may hitch
	URPGItem* LoadedItem = Cast<URPGItem>(ItemPath.TryLoad());

	if (bLogWarning && LoadedItem == nullptr)
	{
		UE_LOG(LogActionRPG, Warning, TEXT("Failed to load item for identifier %s!"), *PrimaryAssetId.ToString());
	}

	return LoadedItem;
}

URPGRaceDataAsset* URPGAssetManager::ForceLoadRace(const FPrimaryAssetId& PrimaryAssetId, bool bLogWarning)
{
	FSoftObjectPath RacePath = GetPrimaryAssetPath(PrimaryAssetId);

	// This does a synchronous load and may hitch
	URPGRaceDataAsset* LoadedRace = Cast<URPGRaceDataAsset>(RacePath.TryLoad());

	if (bLogWarning && LoadedRace == nullptr)
	{
		UE_LOG(LogActionRPG, Warning, TEXT("Failed to load race for identifier %s!"), *PrimaryAssetId.ToString());
	}

	return LoadedRace;
}

URPGClassDataAsset* URPGAssetManager::ForceLoadClass(const FPrimaryAssetId& PrimaryAssetId, bool bLogWarning)
{
	FSoftObjectPath RacePath = GetPrimaryAssetPath(PrimaryAssetId);

	// This does a synchronous load and may hitch
	URPGClassDataAsset* LoadedClass = Cast<URPGClassDataAsset>(RacePath.TryLoad());

	if (bLogWarning && LoadedClass == nullptr)
	{
		UE_LOG(LogActionRPG, Warning, TEXT("Failed to load class for identifier %s!"), *PrimaryAssetId.ToString());
	}

	return LoadedClass;
}

URPGSkillSetDataAsset* URPGAssetManager::ForceLoadSkillSet(const FPrimaryAssetId& PrimaryAssetId, bool bLogWarning)
{
	FSoftObjectPath SkillSetPath = GetPrimaryAssetPath(PrimaryAssetId);

	// This does a synchronous load and may hitch
	URPGSkillSetDataAsset* LoadedSkillSet = Cast<URPGSkillSetDataAsset>(SkillSetPath.TryLoad());

	if (bLogWarning && LoadedSkillSet == nullptr)
	{
		UE_LOG(LogActionRPG, Warning, TEXT("Failed to load skill set for identifier %s!"), *PrimaryAssetId.ToString());
	}

	return LoadedSkillSet;
}

URPGSkillTierDataAsset* URPGAssetManager::ForceLoadSkillTier(const FPrimaryAssetId& PrimaryAssetId, bool bLogWarning)
{
	FSoftObjectPath SkillTierPath = GetPrimaryAssetPath(PrimaryAssetId);

	// This does a synchronous load and may hitch
	URPGSkillTierDataAsset* LoadedSkillTier = Cast<URPGSkillTierDataAsset>(SkillTierPath.TryLoad());

	if (bLogWarning && LoadedSkillTier == nullptr)
	{
		UE_LOG(LogActionRPG, Warning, TEXT("Failed to load skill tier for identifier %s!"), *PrimaryAssetId.ToString());
	}

	return LoadedSkillTier;
}
