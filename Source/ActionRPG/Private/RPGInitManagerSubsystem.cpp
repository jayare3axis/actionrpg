// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGInitManagerSubsystem.h"

void URPGInitManagerSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	InitializationPhaze = EInitializationPhase::IPE_Pre;
}

void URPGInitManagerSubsystem::Bind(TScriptInterface<IRPGInitializationInterface> Target)
{
	if (!Target || Listeners.Contains(Target))
	{
		return;
	}

	if (InitializationPhaze >= EInitializationPhase::IPE_Character)
	{
		Target->OnCharacterCreated();
	}
	if (InitializationPhaze >= EInitializationPhase::IPE_UI)
	{
		Target->OnHUDCreated();
	}
	if (InitializationPhaze >= EInitializationPhase::IPE_Done)
	{
		Target->OnReady();
	}

	Listeners.Add(Target);
}

void URPGInitManagerSubsystem::Unbind(TScriptInterface<IRPGInitializationInterface> Target)
{
	if (!Target || !Listeners.Contains(Target))
	{
		return;
	}

	Listeners.Remove(Target);
}

void URPGInitManagerSubsystem::NextStep()
{
	if (InitializationPhaze == EInitializationPhase::IPE_Done)
	{
		return;
	}

	if (InitializationPhaze == EInitializationPhase::IPE_Pre)
	{
		InitializationPhaze = EInitializationPhase::IPE_Character;
		for (TScriptInterface<IRPGInitializationInterface> Listener : Listeners)
		{
			// This fails in production without if for some reason
			if (Listener != NULL)
			{
				Listener->OnCharacterCreated();
			}			
		}		
	}
	else if (InitializationPhaze == EInitializationPhase::IPE_Character)
	{
		InitializationPhaze = EInitializationPhase::IPE_UI;
		for (TScriptInterface<IRPGInitializationInterface> Listener : Listeners)
		{
			// This fails in production without if for some reason
			if (Listener != NULL)
			{
				Listener->OnHUDCreated();
			}
		}
	}
	else if (InitializationPhaze == EInitializationPhase::IPE_UI)
	{
		InitializationPhaze = EInitializationPhase::IPE_Done;
		for (TScriptInterface<IRPGInitializationInterface> Listener : Listeners)
		{
			// This fails in production without if for some reason
			if (Listener != NULL)
			{
				Listener->OnReady();
			}
		}
	}
}
