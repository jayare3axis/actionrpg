// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGCombatComponent.h"
#include "GameplayTagsModule.h"
#include "RPGBlueprintFunctionLibrary.h"
#include "RPGPlayerControllerBase.h"
#include "RPGGameInstanceBase.h"
#include "RPGDamageInfo.h"
#include "RPGCharacterBase.h"
#include "RPGChasingManagerComponent.h"

#define COMBO_CHECK_PERIOD_S .1f

URPGCombatComponent::URPGCombatComponent()
{
	bAbilitiesInitialized = false;
	bEnableComboPeriod = false;
	CurrentComboSectionName = "";
	ComboSectionIndex = 0;
}

void URPGCombatComponent::GrantAbilities()
{
	ARPGCharacterBase* Char = GetOwnerCharacter();
	check(Char);

	// For some reason AbilitySystemComponent needs to be owned by a character, not here to work
	UAbilitySystemComponent* AbilitySystemComponent = Char->GetAbilitySystemComponent();

	AbilitySystemComponent->InitAbilityActorInfo(GetOwnerCharacter(), GetOwnerCharacter());
	AddStartupGameplayAbilities();
}

void URPGCombatComponent::ResetSlottedAbilitiesSource()
{
	// Unmap from inventory source
	if (SlottedAbilitiesSource && SlottedAbilitiesLoadedHandle.IsValid())
	{
		SlottedAbilitiesSource->GetSlottedItemChangedDelegate().Remove(SlottedAbilitesUpdateHandle);
		SlottedAbilitesUpdateHandle.Reset();

		SlottedAbilitiesSource->GetSlottedAbilitiesLoadedDelegate().Remove(SlottedAbilitiesLoadedHandle);
		SlottedAbilitiesLoadedHandle.Reset();
	}

	SlottedAbilitiesSource = NULL;
}

void URPGCombatComponent::InitSlottedAbilitiesSourceAsController(AController* NewController)
{
	if (SlottedAbilitiesSource == NewController)
	{
		return;
	}

	SlottedAbilitiesSource = NewController;

	if (SlottedAbilitiesSource)
	{
		SlottedAbilitesUpdateHandle = SlottedAbilitiesSource->GetSlottedItemChangedDelegate().AddUObject(this, &URPGCombatComponent::OnItemSlotChanged);
		SlottedAbilitiesLoadedHandle = SlottedAbilitiesSource->GetSlottedAbilitiesLoadedDelegate().AddUObject(this, &URPGCombatComponent::RefreshSlottedGameplayAbilities);
	}
}

void URPGCombatComponent::InitAbilities(const TArray<FAbilityAndLevel>& _GameplayAbilities, const TMap<FRPGItemSlot, FAbilityAndLevel>& _DefaultSlottedAbilities, const TArray<FEffectAndLevel>& _PassiveGameplayEffects)
{
	GameplayAbilities = _GameplayAbilities;
	DefaultSlottedAbilities = _DefaultSlottedAbilities;
	PassiveGameplayEffects = _PassiveGameplayEffects;
}

void URPGCombatComponent::CancelAnyAttack()
{
	ARPGCharacterBase* Char = GetOwnerCharacter();
	check(Char);

	// For some reason AbilitySystemComponent needs to be owned by a character, not here to work
	UAbilitySystemComponent* AbilitySystemComponent = Char->GetAbilitySystemComponent();

	FGameplayTagContainer Tags;
	UGameplayTagsManager::Get().RequestGameplayTagContainer({ GT_MeleeAbility, GT_SkillAbility }, Tags);
	AbilitySystemComponent->CancelAbilities(&Tags);
}

void URPGCombatComponent::OnNewSkillAcquired_Implementation()
{
	ARPGCharacterBase* Char = GetOwnerCharacter();
	check(Char);

	// For some reason AttributeSet needs to be owned by a character, not here to work
	URPGAttributeSet* AttributeSet = Char->AttributeSet;

	AttributeSet->SetAvailableSkillPoints(GetAttributeValue(EAttributeName::ANE_AvailableSkillPoints) - 1.f);

	FGameplayTagContainer Tags;
	HandleAttributeChanged(EAttributeName::ANE_AvailableSkillPoints, -1, Tags);
}

void URPGCombatComponent::ForceLevelUp()
{
	ARPGCharacterBase* Char = GetOwnerCharacter();
	check(Char);

	// For some reason AttributeSet needs to be owned by a character, not here to work
	URPGAttributeSet* AttributeSet = Char->AttributeSet;

	URPGGameInstanceBase* GameInstance = URPGBlueprintFunctionLibrary::GetGameInstance(this);
	float NeededExperience = GameInstance->GetNeededExperienceForNextLevel(GetAttributeValue(EAttributeName::ANE_Level));

	if (NeededExperience - GetAttributeValue(EAttributeName::ANE_Experience) == 0.f)
	{
		return;
	}

	AttributeSet->SetExperience(NeededExperience);
	AttributeSet->SetLevel(GetAttributeValue(EAttributeName::ANE_Level) + 1);

	FGameplayTagContainer Tags;
	HandleAttributeChanged(EAttributeName::ANE_Experience, GetAttributeValue(EAttributeName::ANE_Experience) - NeededExperience, Tags);
	HandleAttributeChanged(EAttributeName::ANE_Level, 1, Tags);
}

void URPGCombatComponent::NotifyAttributesChanged()
{
	MulticastBroadcastAttributeChanged(EAttributeName::ANE_Health, GetAttributeValue(EAttributeName::ANE_Health));
	MulticastBroadcastAttributeChanged(EAttributeName::ANE_MaxHealth, GetAttributeValue(EAttributeName::ANE_MaxHealth));
	MulticastBroadcastAttributeChanged(EAttributeName::ANE_Mana, GetAttributeValue(EAttributeName::ANE_Mana));
	MulticastBroadcastAttributeChanged(EAttributeName::ANE_MaxMana, GetAttributeValue(EAttributeName::ANE_MaxMana));
	MulticastBroadcastAttributeChanged(EAttributeName::ANE_Strength, GetAttributeValue(EAttributeName::ANE_Strength));
	MulticastBroadcastAttributeChanged(EAttributeName::ANE_Dexterity, GetAttributeValue(EAttributeName::ANE_Dexterity));
	MulticastBroadcastAttributeChanged(EAttributeName::ANE_Endurance, GetAttributeValue(EAttributeName::ANE_Endurance));
	MulticastBroadcastAttributeChanged(EAttributeName::ANE_Intelligence, GetAttributeValue(EAttributeName::ANE_Intelligence));
	MulticastBroadcastAttributeChanged(EAttributeName::ANE_AttackRatingBonus, GetAttributeValue(EAttributeName::ANE_AttackRatingBonus));
	MulticastBroadcastAttributeChanged(EAttributeName::ANE_DefenceRatingBonus, GetAttributeValue(EAttributeName::ANE_DefenceRatingBonus));
}

bool URPGCombatComponent::TryAbilityByItemSlot(FRPGItemSlot ItemSlot)
{
	if (!CanUseAnyAbility())
	{
		return false;
	}

	ARPGCharacterBase* Char = GetOwnerCharacter();
	check(Char);

	// Handle combo
	if (ItemSlot == FRPGItemSlot(URPGAssetManager::WeaponItemType, 0.f) && bEnableComboPeriod && (ComboSection.Num() > 1))
	{
		if (++ComboSectionIndex >= ComboSection.Num())
		{
			ComboSectionIndex = 0;
		}
		CurrentComboSectionName = ComboSection[ComboSectionIndex];
	}

	// For some reason AbilitySystemComponent needs to be owned by a character, not here to work
	UAbilitySystemComponent* AbilitySystemComponent = Char->GetAbilitySystemComponent();
	FGameplayAbilitySpecHandle* FoundHandle = SlottedAbilities.Find(ItemSlot);
	FGameplayAbilitySpec* FoundSpec = AbilitySystemComponent->FindAbilitySpecFromHandle(*FoundHandle);

	bool Success = ActivateAbilitiesWithItemSlot(ItemSlot);
	
	if (Success)
	{
		// Handle cooldown UI
		if (AbilitySystemComponent && FoundHandle && FoundSpec && FoundSpec->Ability)
		{
			float Cooldown = URPGBlueprintFunctionLibrary::GetAbilityCooldown(Char, Cast<URPGGameplayAbility>(FoundSpec->Ability));
			if (Cooldown)
			{
				ARPGPlayerControllerBase* PC = URPGBlueprintFunctionLibrary::GetPlayerControlerByCharacter(Char, Char);
				check(PC);

				PC->ClientStartAbilityCooldown(Cooldown, ItemSlot);
			}
		}
	}
	else
	{
		// Handle failure
		if (AbilitySystemComponent && FoundHandle && ItemSlot.ItemType == URPGAssetManager::SkillItemType)
		{
			FGameplayAbilitySpec* FoundSpec = AbilitySystemComponent->FindAbilitySpecFromHandle(*FoundHandle);			
			if (FoundSpec && FoundSpec->Ability)
			{
				// Failed melee skill fallback to melee weapon
				if (FoundSpec->Ability->AbilityTags.HasTag(UGameplayTagsManager::Get().RequestGameplayTag(GT_MeleeAbility)))
				{
					TryAbilityByItemSlot(FRPGItemSlot(URPGAssetManager::WeaponItemType, 0));
				}
			}
		}		
	}

	return Success;
}

void URPGCombatComponent::StartCombo()
{
	ARPGCharacterBase* Char = GetOwnerCharacter();
	check(Char);

	bEnableComboPeriod = true;

	Char->GetWorldTimerManager().SetTimer(CheckComboTimerHandle, this, &URPGCombatComponent::CancelCombo, COMBO_CHECK_PERIOD_S, false);
}

void URPGCombatComponent::CancelCombo()
{
	ARPGCharacterBase* Char = GetOwnerCharacter();
	check(Char);

	bEnableComboPeriod = false;

	CurrentComboSectionName = "";
	ComboSectionIndex = 0;
}

FName URPGCombatComponent::GetCurrentComboSectionName() const 
{
	return CurrentComboSectionName;
}

void URPGCombatComponent::RecievedByAggro(ARPGCharacterBase* AggroChar)
{
	ARPGCharacterBase* Char = GetOwnerCharacter();
	check(Char);

	Char->ChasingManagerComponent->ReceivedByAggro(AggroChar);
}

void URPGCombatComponent::LostByAggro(ARPGCharacterBase* AggroChar)
{
	ARPGCharacterBase* Char = GetOwnerCharacter();
	check(Char);

	Char->ChasingManagerComponent->LostByAggro(AggroChar);
}

bool URPGCombatComponent::IsAlive()
{
	return GetAttributeValue(EAttributeName::ANE_Health) > 0.f;
}

bool URPGCombatComponent::IsUsingMelee()
{
	TArray<URPGGameplayAbility*> ActiveAbilities;

	FGameplayTagContainer MeleeAttackTag;
	UGameplayTagsManager::Get().RequestGameplayTagContainer({ GT_MeleeAbility }, MeleeAttackTag);
	GetActiveAbilitiesWithTags(MeleeAttackTag, ActiveAbilities);

	return ActiveAbilities.Num() > 0;

	return false;
}

bool URPGCombatComponent::IsUsingSkill()
{
	TArray<URPGGameplayAbility*> ActiveAbilities;

	FGameplayTagContainer SkillTag;
	UGameplayTagsManager::Get().RequestGameplayTagContainer({ GT_SkillAbility }, SkillTag);
	GetActiveAbilitiesWithTags(SkillTag, ActiveAbilities);

	return ActiveAbilities.Num() > 0;

	return false;
}

void URPGCombatComponent::InitAttributesBySaveGameData(float Level, float Experience, float AvailableSkillPoints, float Health, float Mana)
{
	ARPGCharacterBase* Char = GetOwnerCharacter();
	check(Char);

	// For some reason AttributeSet needs to be owned by a character, not here to work
	URPGAttributeSet* AttributeSet = Char->AttributeSet;

	// What health does char have when respawned?
	if (Health == 0)
	{
		Health = 1;
	}
	// Handle initial values
	if (Health == -1)
	{
		Health = GetAttributeValue(EAttributeName::ANE_MaxHealth);
	}
	if (Mana == -1)
	{
		Mana = GetAttributeValue(EAttributeName::ANE_MaxMana);
	}

	AttributeSet->SetLevel(Level);
	AttributeSet->SetExperience(Experience);
	AttributeSet->SetAvailableSkillPoints(AvailableSkillPoints);
	AttributeSet->SetHealth(Health);
	AttributeSet->SetMana(Mana);

	// Raise changes for any attributes affected by save game, this is needed for UI (blueprints) to get notified about those changes
	MulticastBroadcastAttributeChanged(EAttributeName::ANE_Experience, GetAttributeValue(EAttributeName::ANE_Experience));
	MulticastBroadcastAttributeChanged(EAttributeName::ANE_AvailableSkillPoints, GetAttributeValue(EAttributeName::ANE_AvailableSkillPoints));
	MulticastBroadcastAttributeChanged(EAttributeName::ANE_Health, GetAttributeValue(EAttributeName::ANE_Health));
	MulticastBroadcastAttributeChanged(EAttributeName::ANE_Mana, GetAttributeValue(EAttributeName::ANE_Mana));
}

void URPGCombatComponent::AddStartupGameplayAbilities()
{
	ARPGCharacterBase* Char = GetOwnerCharacter();
	check(Char);

	// For some reason AbilitySystemComponent needs to be owned by a character, not here to work
	UAbilitySystemComponent* AbilitySystemComponent = Char->GetAbilitySystemComponent();

	if (Char->Role != ROLE_Authority || bAbilitiesInitialized)
	{
		return;
	}

	// Grant abilities, but only on the server
	for (FAbilityAndLevel& StartupAbility : CommonGameplayAbilities)
	{
		AbilitySystemComponent->GiveAbility(FGameplayAbilitySpec(StartupAbility.Ability, StartupAbility.Level, INDEX_NONE, Char));
	}
	for (FAbilityAndLevel& StartupAbility : GameplayAbilities)
	{
		AbilitySystemComponent->GiveAbility(FGameplayAbilitySpec(StartupAbility.Ability, StartupAbility.Level, INDEX_NONE, Char));
	}


	AddStartupGameplayEffects();
	AddSlottedGameplayAbilities();

	bAbilitiesInitialized = true;
}

void URPGCombatComponent::RemoveStartupGameplayAbilities()
{
	ARPGCharacterBase* Char = GetOwnerCharacter();
	check(Char);

	// For some reason AbilitySystemComponent needs to be owned by a character, not here to work
	UAbilitySystemComponent* AbilitySystemComponent = Char->GetAbilitySystemComponent();

	check(AbilitySystemComponent);

	if (Char->Role != ROLE_Authority && !bAbilitiesInitialized)
	{
		return;
	}

	// Remove any abilities added from a previous call
	TArray<FGameplayAbilitySpecHandle> AbilitiesToRemove;
	for (const FGameplayAbilitySpec& Spec : AbilitySystemComponent->GetActivatableAbilities())
	{
		if ((Spec.SourceObject == this) && CommonGameplayAbilities.Contains(FAbilityAndLevel(Spec.Ability->GetClass(), 1)))
		{
			AbilitiesToRemove.Add(Spec.Handle);
		}
	}
	for (const FGameplayAbilitySpec& Spec : AbilitySystemComponent->GetActivatableAbilities())
	{
		if ((Spec.SourceObject == this) && GameplayAbilities.Contains(FAbilityAndLevel(Spec.Ability->GetClass(), 1)))
		{
			AbilitiesToRemove.Add(Spec.Handle);
		}
	}

	// Do in two passes so the removal happens after we have the full list
	for (int32 i = 0; i < AbilitiesToRemove.Num(); i++)
	{
		AbilitySystemComponent->ClearAbility(AbilitiesToRemove[i]);
	}

	RemoveAllGameplayEffects();
	RemoveSlottedGameplayAbilities(true);

	bAbilitiesInitialized = false;
}

void URPGCombatComponent::AddStartupGameplayEffects()
{
	ARPGCharacterBase* Char = GetOwnerCharacter();
	check(Char);

	// For some reason AbilitySystemComponent needs to be owned by a character, not here to work
	UAbilitySystemComponent* AbilitySystemComponent = Char->GetAbilitySystemComponent();

	// Apply passives
	for (FEffectAndLevel& GameplayEffect :PassiveGameplayEffects)
	{
		FGameplayEffectContextHandle EffectContext = AbilitySystemComponent->MakeEffectContext();
		EffectContext.AddSourceObject(this);

		FGameplayEffectSpecHandle NewHandle = AbilitySystemComponent->MakeOutgoingSpec(GameplayEffect.Effect, GameplayEffect.Level, EffectContext);
		if (NewHandle.IsValid())
		{
			FActiveGameplayEffectHandle ActiveGEHandle = AbilitySystemComponent->ApplyGameplayEffectSpecToTarget(*NewHandle.Data.Get(), AbilitySystemComponent);
		}
	}
}

void URPGCombatComponent::AddSlottedGameplayAbilities()
{
	ARPGCharacterBase* Char = GetOwnerCharacter();
	check(Char);

	// For some reason AbilitySystemComponent needs to be owned by a character, not here to work
	UAbilitySystemComponent* AbilitySystemComponent = Char->GetAbilitySystemComponent();

	TMap<FRPGItemSlot, FGameplayAbilitySpec> SlottedAbilitySpecs;
	FillSlottedAbilitySpecs(SlottedAbilitySpecs);

	// Now add abilities if needed
	for (const TPair<FRPGItemSlot, FGameplayAbilitySpec>& SpecPair : SlottedAbilitySpecs)
	{
		FGameplayAbilitySpecHandle& SpecHandle = SlottedAbilities.FindOrAdd(SpecPair.Key);

		if (!SpecHandle.IsValid())
		{
			SpecHandle = AbilitySystemComponent->GiveAbility(SpecPair.Value);
		}
	}
}

void URPGCombatComponent::AddSlottedGameplayEffects()
{
	ARPGCharacterBase* Char = GetOwnerCharacter();
	check(Char);

	// For some reason AbilitySystemComponent needs to be owned by a character, not here to work
	UAbilitySystemComponent* AbilitySystemComponent = Char->GetAbilitySystemComponent();

	if (SlottedAbilitiesSource)
	{
		const TMap<FRPGItemSlot, FRPGItemAtSlot>& SlottedItemMap = SlottedAbilitiesSource->GetSlottedItemMap();

		for (const TPair<FRPGItemSlot, FRPGItemAtSlot>& ItemPair : SlottedItemMap)
		{
			FRPGItemAtSlot ItemAtSlot = ItemPair.Value;
			URPGItem* Item = ItemAtSlot.Item;

			if (Item && Item->GrantedGameplayEffects.Num() > 0)
			{
				for (const FEffectAndLevel GrantedGameplayEffect : Item->GrantedGameplayEffects)
				{
					FGameplayEffectContextHandle EffectContext = AbilitySystemComponent->MakeEffectContext();
					EffectContext.AddSourceObject(this);

					FGameplayEffectSpecHandle NewHandle = AbilitySystemComponent->MakeOutgoingSpec(GrantedGameplayEffect.Effect, GrantedGameplayEffect.Level, EffectContext);
					if (NewHandle.IsValid())
					{
						FActiveGameplayEffectHandle ActiveGEHandle = AbilitySystemComponent->ApplyGameplayEffectSpecToTarget(*NewHandle.Data.Get(), AbilitySystemComponent);
						SlottedEffects.Add(FRPGActiveEffectAtSlot(ItemPair.Key, ActiveGEHandle));
					}
				}
			}
		}
	}
}

void URPGCombatComponent::RemoveAllGameplayEffects()
{
	ARPGCharacterBase* Char = GetOwnerCharacter();
	check(Char);

	// For some reason AbilitySystemComponent needs to be owned by a character, not here to work
	UAbilitySystemComponent* AbilitySystemComponent = Char->GetAbilitySystemComponent();

	// Remove all of the passive gameplay effects that were applied by this character
	FGameplayEffectQuery Query;
	Query.EffectSource = this;
	AbilitySystemComponent->RemoveActiveEffects(Query);
}

void URPGCombatComponent::FillSlottedAbilitySpecs(TMap<FRPGItemSlot, FGameplayAbilitySpec>& SlottedAbilitySpecs)
{
	// First add default ones
	for (const TPair<FRPGItemSlot, FAbilityAndLevel>& DefaultPair : DefaultSlottedAbilities)
	{
		if (DefaultPair.Value.Ability.Get())
		{
			SlottedAbilitySpecs.Add(DefaultPair.Key, FGameplayAbilitySpec(DefaultPair.Value.Ability, DefaultPair.Value.Level, INDEX_NONE, this));
		}
	}

	// Now potentially override with slotted abilities
	if (SlottedAbilitiesSource)
	{
		const TMap<FRPGItemSlot, FRPGItemAtSlot>& SlottedItemMap = SlottedAbilitiesSource->GetSlottedItemMap();

		for (const TPair<FRPGItemSlot, FRPGItemAtSlot>& ItemPair : SlottedItemMap)
		{
			FRPGItemAtSlot ItemAtSlot = ItemPair.Value;
			URPGItem* Item = ItemAtSlot.Item;

			if (Item && Item->GrantedAbility)
			{
				// This will override anything from default
				SlottedAbilitySpecs.Add(ItemPair.Key, FGameplayAbilitySpec(Item->GrantedAbility, ItemAtSlot.Level, INDEX_NONE, Item));
			}
		}
	}
}

void URPGCombatComponent::OnItemSlotChanged(FRPGItemSlot ItemSlot, URPGItem* Item)
{
	RefreshSlottedGameplayAbilities();
}

void URPGCombatComponent::RefreshSlottedGameplayAbilities()
{
	if (!bAbilitiesInitialized)
	{
		return;
	}

	// Refresh any invalid abilities and adds new ones
	RemoveSlottedGameplayAbilities(false);
	AddSlottedGameplayAbilities();

	RefreshSlottedGameplayEffects();
}

void URPGCombatComponent::RefreshSlottedGameplayEffects()
{
	RemoveSlottedGameplayEffects();
	AddSlottedGameplayEffects();

	// Raise changes for any attributes affected by slotted effects, this is needed for UI (blueprints) to get notified about those changes, because those
	// don't trigger post gameplay effect execution (see RPGAttributeSet)
	NotifyAttributesChanged();
}

void URPGCombatComponent::RemoveSlottedGameplayAbilities(bool bRemoveAll)
{
	ARPGCharacterBase* Char = GetOwnerCharacter();
	check(Char);

	// For some reason AbilitySystemComponent needs to be owned by a character, not here to work
	UAbilitySystemComponent* AbilitySystemComponent = Char->GetAbilitySystemComponent();

	TMap<FRPGItemSlot, FGameplayAbilitySpec> SlottedAbilitySpecs;

	if (!bRemoveAll)
	{
		// Fill in map so we can compare
		FillSlottedAbilitySpecs(SlottedAbilitySpecs);
	}

	for (TPair<FRPGItemSlot, FGameplayAbilitySpecHandle>& ExistingPair : SlottedAbilities)
	{
		FGameplayAbilitySpec* FoundSpec = AbilitySystemComponent->FindAbilitySpecFromHandle(ExistingPair.Value);
		bool bShouldRemove = bRemoveAll || !FoundSpec;

		if (!bShouldRemove)
		{
			// Need to check desired ability specs, if we got here FoundSpec is valid
			FGameplayAbilitySpec* DesiredSpec = SlottedAbilitySpecs.Find(ExistingPair.Key);

			if (!DesiredSpec || DesiredSpec->Ability != FoundSpec->Ability || DesiredSpec->SourceObject != FoundSpec->SourceObject)
			{
				bShouldRemove = true;
			}
		}

		if (bShouldRemove)
		{
			if (FoundSpec)
			{
				// Need to remove registered ability
				AbilitySystemComponent->ClearAbility(ExistingPair.Value);
			}

			// Make sure handle is cleared even if ability wasn't found
			ExistingPair.Value = FGameplayAbilitySpecHandle();
		}
	}
}

void URPGCombatComponent::RemoveSlottedGameplayEffects()
{
	ARPGCharacterBase* Char = GetOwnerCharacter();
	check(Char);

	// For some reason AbilitySystemComponent needs to be owned by a character, not here to work
	UAbilitySystemComponent* AbilitySystemComponent = Char->GetAbilitySystemComponent();

	for (FRPGActiveEffectAtSlot& EffectAtSlot : SlottedEffects)
	{
		if (EffectAtSlot.Effect.IsValid())
		{
			AbilitySystemComponent->RemoveActiveGameplayEffect(EffectAtSlot.Effect);
		}
	}

	SlottedEffects.Empty();
}

bool URPGCombatComponent::ActivateAbilitiesWithItemSlot(FRPGItemSlot ItemSlot, bool bAllowRemoteActivation)
{
	ARPGCharacterBase* Char = GetOwnerCharacter();
	check(Char);

	// For some reason AbilitySystemComponent needs to be owned by a character, not here to work
	UAbilitySystemComponent* AbilitySystemComponent = Char->GetAbilitySystemComponent();

	FGameplayAbilitySpecHandle* FoundHandle = SlottedAbilities.Find(ItemSlot);

	if (FoundHandle && AbilitySystemComponent)
	{
		return AbilitySystemComponent->TryActivateAbility(*FoundHandle, bAllowRemoteActivation);
	}

	return false;
}

void URPGCombatComponent::GetActiveAbilitiesWithItemSlot(FRPGItemSlot ItemSlot, TArray<URPGGameplayAbility*>& ActiveAbilities)
{
	ARPGCharacterBase* Char = GetOwnerCharacter();
	check(Char);

	// For some reason AbilitySystemComponent needs to be owned by a character, not here to work
	UAbilitySystemComponent* AbilitySystemComponent = Char->GetAbilitySystemComponent();

	FGameplayAbilitySpecHandle* FoundHandle = SlottedAbilities.Find(ItemSlot);

	if (FoundHandle && AbilitySystemComponent)
	{
		FGameplayAbilitySpec* FoundSpec = AbilitySystemComponent->FindAbilitySpecFromHandle(*FoundHandle);

		if (FoundSpec)
		{
			TArray<UGameplayAbility*> AbilityInstances = FoundSpec->GetAbilityInstances();

			// Find all ability instances executed from this slot
			for (UGameplayAbility* ActiveAbility : AbilityInstances)
			{
				ActiveAbilities.Add(Cast<URPGGameplayAbility>(ActiveAbility));
			}
		}
	}
}

bool URPGCombatComponent::ActivateAbilitiesWithTags(FGameplayTagContainer AbilityTags, bool bAllowRemoteActivation)
{
	ARPGCharacterBase* Char = GetOwnerCharacter();
	check(Char);

	// For some reason AbilitySystemComponent needs to be owned by a character, not here to work
	UAbilitySystemComponent* AbilitySystemComponent = Char->GetAbilitySystemComponent();

	if (AbilitySystemComponent)
	{
		return AbilitySystemComponent->TryActivateAbilitiesByTag(AbilityTags, bAllowRemoteActivation);
	}

	return false;
}

void URPGCombatComponent::GetActiveAbilitiesWithTags(FGameplayTagContainer AbilityTags, TArray<URPGGameplayAbility*>& ActiveAbilities)
{
	ARPGCharacterBase* Char = GetOwnerCharacter();
	check(Char);

	// For some reason AbilitySystemComponent needs to be owned by a character, not here to work
	URPGAbilitySystemComponent* AbilitySystemComponent = Cast<URPGAbilitySystemComponent>( Char->GetAbilitySystemComponent() );

	if (AbilitySystemComponent)
	{
		AbilitySystemComponent->GetActiveAbilitiesWithTags(AbilityTags, ActiveAbilities);
	}
}

float URPGCombatComponent::GetAttributeValue(EAttributeName AttributeName) const
{	
	ARPGCharacterBase* Char = GetOwnerCharacter();
	check(Char);

	// For some reason AttributeSet needs to be owned by a character, not here to work
	URPGAttributeSet* AttributeSet = Char->AttributeSet;

	switch (AttributeName)
	{
		case EAttributeName::ANE_Health :
			return AttributeSet->GetHealth();
		case EAttributeName::ANE_MaxHealth:
			return AttributeSet->GetMaxHealth();
		case EAttributeName::ANE_Mana :
			return AttributeSet->GetMana();
		case EAttributeName::ANE_MaxMana :
			return AttributeSet->GetMaxMana();
		case EAttributeName::ANE_MoveSpeed:
			return AttributeSet->GetMoveSpeed();
		case EAttributeName::ANE_Strength :
			return AttributeSet->GetStrength();
		case EAttributeName::ANE_Dexterity :
			return AttributeSet->GetDexterity();
		case EAttributeName::ANE_Endurance :
			return AttributeSet->GetEndurance();
		case EAttributeName::ANE_Intelligence :
			return AttributeSet->GetIntelligence();
		case EAttributeName::ANE_AttackRatingBonus:
			return AttributeSet->GetAttackRatingBonus();
		case EAttributeName::ANE_DefenceRatingBonus:
			return AttributeSet->GetDefenceRatingBonus();
		case EAttributeName::ANE_Level:
			return AttributeSet->GetLevel();
		case EAttributeName::ANE_Experience:
			return AttributeSet->GetExperience();
		case EAttributeName::ANE_AvailableSkillPoints:
			return AttributeSet->GetAvailableSkillPoints();
		default:
			UE_LOG(LogActionRPG, Error, TEXT("Not implemented!"));
			return 0.f;
	}
}

ARPGCharacterBase* URPGCombatComponent::GetOwnerCharacter() const
{
	ARPGCharacterBase* Char = Cast<ARPGCharacterBase>(GetOwner());
	if (!Char)
	{
		UE_LOG(LogActionRPG, Warning, TEXT("Combat Component: Owner must be Character!"));
	}
	return Char;
}

void URPGCombatComponent::MulticastBroadcastAttributeChanged_Implementation(EAttributeName AttributeName, float Value)
{
	ReceiveAttributeChanged.Broadcast(AttributeName, Value);
}

void URPGCombatComponent::MulticastBroadcastOnDamaged_Implementation(float DamageAmount, const FHitResult& HitInfo, const struct FGameplayTagContainer& DamageTags, ARPGCharacterBase* InstigatorCharacter, AActor* DamageCauser)
{
	ReceiveOnDamaged.Broadcast(
		FRPGDamageInfo(DamageAmount, HitInfo, DamageTags, InstigatorCharacter, DamageCauser)
	);
}

void URPGCombatComponent::MulticastBroadcastOnDeath_Implementation()
{
	ARPGCharacterBase* Char = GetOwnerCharacter();
	check(Char);

	Char->GetCapsuleComponent()->SetCollisionProfileName("Spectator");

	ReceiveOnDeath.Broadcast();
}

void URPGCombatComponent::HandleDamage(float DamageAmount, const FHitResult& HitInfo, const struct FGameplayTagContainer& DamageTags, ARPGCharacterBase* InstigatorCharacter, AActor* DamageCauser)
{
	ARPGCharacterBase* Char = GetOwnerCharacter();
	check(Char);

	MulticastBroadcastOnDamaged(DamageAmount, HitInfo, DamageTags, InstigatorCharacter, DamageCauser);

	// Check if damage comes from a player - if so show the damage info
	ARPGPlayerControllerBase* PC = URPGBlueprintFunctionLibrary::GetPlayerControlerByCharacter(this, InstigatorCharacter);
	if (PC != NULL)
	{
		PC->ClientShowDamageInfo(DamageAmount, Char);
	}
}

void URPGCombatComponent::HandleAttributeChanged(EAttributeName AttributeName, float DeltaValue, const struct FGameplayTagContainer& EventTags)
{
	ARPGCharacterBase* Char = GetOwnerCharacter();
	check(Char);

	// For some reason AbilitySystemComponent needs to be owned by a character, not here to work
	URPGAbilitySystemComponent* AbilitySystemComponent = Cast<URPGAbilitySystemComponent>(Char->GetAbilitySystemComponent());

	// For some reason AttributeSet needs to be owned by a character, not here to work
	URPGAttributeSet* AttributeSet = Char->AttributeSet;

	// Level
	if (AttributeName == EAttributeName::ANE_Level)
	{
		// Add available Skill Point
		if (DeltaValue > 0.f)
		{
			AttributeSet->SetAvailableSkillPoints(GetAttributeValue(EAttributeName::ANE_AvailableSkillPoints) + 1);

			FGameplayTagContainer Tags;
			HandleAttributeChanged(EAttributeName::ANE_AvailableSkillPoints, 1, Tags);
		}
	}
	// Health
	else if (AttributeName == EAttributeName::ANE_Health && bAbilitiesInitialized)
	{
		if (!IsAlive())
		{
			// Gain XP (if available)
			FGameplayTagContainer GainXPTag;
			UGameplayTagsManager::Get().RequestGameplayTagContainer({ GT_GainExperience }, GainXPTag);
			AbilitySystemComponent->TryActivateAbilitiesByTag(GainXPTag);

			// Tell everyone we died
			MulticastBroadcastOnDeath();
		}
	}

	// We only call the BP callback if this is not the initial ability setup
	if (bAbilitiesInitialized)
	{
		OnAttributeChanged(AttributeName, DeltaValue, EventTags);
		MulticastBroadcastAttributeChanged(AttributeName, GetAttributeValue(AttributeName));
	}
}

bool URPGCombatComponent::CanUseAnyAbility_Implementation()
{
	return IsAlive() && !IsUsingSkill() && !IsUsingMelee();
}
