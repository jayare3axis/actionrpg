// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGCharacterChasingTarget.h"
#include "RPGCharacterBase.h"

ARPGCharacterChasingTarget::ARPGCharacterChasingTarget()
{
}

void ARPGCharacterChasingTarget::AssignAggro(ARPGCharacterBase* Aggro)
{
	Aggros.Add(Aggro);
}

int32 ARPGCharacterChasingTarget::GetAggroCount() const
{
	return Aggros.Num();
}

void ARPGCharacterChasingTarget::ResetAggros()
{
	Aggros.Empty();
}
