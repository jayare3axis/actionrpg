// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGInteractableActor.h"
#include "ActionRPG.h"
#include "RPGPlayerControllerBase.h"

ARPGInteractableActor::ARPGInteractableActor()
{
}

void ARPGInteractableActor::Interact(ARPGPlayerControllerBase* PC)
{
	BlueprintInteract(PC);
}

void ARPGInteractableActor::InteractClient(ARPGPlayerControllerBase* PC)
{
	BlueprintInteractClient(PC);
}

void ARPGInteractableActor::StopInteraction(ARPGPlayerControllerBase* PC)
{
	BlueprintStopInteraction(PC);
}

bool ARPGInteractableActor::CanBeInteract()
{
	return BlueprintCanBeInteract();
}

bool ARPGInteractableActor::CanBeHighlighted()
{
	return BlueprintCanBeHighlighted();
}

void ARPGInteractableActor::Highlighted(bool Value)
{
	BlueprintHighlighted(Value);
}
