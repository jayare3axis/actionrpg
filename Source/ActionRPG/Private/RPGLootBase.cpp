// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGLootBase.h"
#include "RPGPlayerControllerBase.h"

ARPGLootBase::ARPGLootBase()
{
	bIsLootEnabled = false;
}

void ARPGLootBase::InteractClient(ARPGPlayerControllerBase* PC)
{
	// Do not proceed if player is currently grabbing the item
	if (PC->IsGrabbingItem())
	{
		PC->ClientStopInteraction();
		return;
	}

	if (PC->CanCarryItem(ItemInstance))
	{
		PC->PickUpItem(this);
	}
	else
	{
		bIsLootEnabled = false;		
		HidePickUpWidget();
		CannotPickUpAnimation();
	}

	// This is one time interaction
	PC->ClientStopInteraction();
}

void ARPGLootBase::StopInteraction(ARPGPlayerControllerBase* PC)
{
}

bool ARPGLootBase::CanBeInteract()
{
	return bIsLootEnabled;
}

void ARPGLootBase::Highlighted(bool Value)
{
	BlueprintHighlighted(Value);
}

bool ARPGLootBase::CanBeHighlighted()
{
	return bIsLootEnabled;
}

void ARPGLootBase::OnItemDropped(FRPGItemInstance ItemToPickUp, float DropHeight)
{
	LootSpawnAnimation(ItemToPickUp, DropHeight);
}

void ARPGLootBase::LootSpawnAnimation_Implementation(FRPGItemInstance ItemToPickUp, float DropHeight)
{
	bIsLootEnabled = true;
	ItemInstance = ItemToPickUp;
	ShowPickUpWidget(ItemToPickUp);
}

void ARPGLootBase::CannotPickUpAnimation_Implementation()
{
	bIsLootEnabled = true;
	ShowPickUpWidget(ItemInstance);
}
