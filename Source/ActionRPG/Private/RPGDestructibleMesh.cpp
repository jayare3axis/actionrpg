// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGDestructibleMesh.h"

ARPGDestructibleMesh::ARPGDestructibleMesh()
{
	bDestroyed = false;
}

bool ARPGDestructibleMesh::CanBeInteract()
{
	return !bDestroyed;
}


bool ARPGDestructibleMesh::ShouldAttackOnInteraction(ARPGPlayerControllerBase* PC)
{
	return true;
}

void ARPGDestructibleMesh::Highlighted(bool Value)
{
	BlueprintHighlighted(Value);
}

bool ARPGDestructibleMesh::CanBeHighlighted()
{
	return !bDestroyed;
}

void ARPGDestructibleMesh::TakeDamage_Implementation()
{
	bDestroyed = true;
}

void ARPGDestructibleMesh::BlueprintTakeDamage()
{
	TakeDamage();
}
